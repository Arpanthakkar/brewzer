var express = require('express');
var router = express.Router();

router.get('/vendor/login', function (req, res){
    res.render('vendor/login');
});

router.get('/vendor', function (req, res){
    res.render('vendor/signup');
});



router.get('/', function (req, res) {
    res.render('admin/login');
});


module.exports = router;