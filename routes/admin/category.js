var express = require('express');
var router = express.Router();
var path = require('path');
var multer = require('multer');
var mkdirp = require('mkdirp');
var bcrypt = require('bcryptjs');
var http = require('http');

var storage = multer.memoryStorage();

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads/category/')
    },
    filename: function(req, file, cb) {
        var getFileExt = function(fileName) {
            var fileExt = fileName.split(".");
            if (fileExt.length === 1 || (fileExt[0] === "" && fileExt.length === 2)) {
                return "";
            }
            return fileExt.pop();
        }
        var microsecond = Math.round(new Date().getTime() / 1000 * Math.floor(Math.random() * 1000000000)); //new Date().getTime();
        cb(null, microsecond + path.extname(file.originalname)); // Date.now() + '.' + getFileExt(file.originalname))
    }
});

var multerUpload = multer({storage: storage});

module.exports = function (app, passport) {
    app.get('/admin/category/categoryindex', isLoggedIn, function (req, res) {
        req.getConnection(function(err, connection) {
            if(err) throw err;
            var currentPage = 1;
            var pageCount = 0;
            var totalCount = 0;
            if(typeof (req.query.page) != "undefined" && req.query.page > 0){
                var currentPage = req.query.page;
            }
            connection.query("SELECT count(*) AS cnt FROM categories WHERE status != '2'", function(err, rowsCount) {
                if(err) throw err;
                totalCount = rowsCount[0].cnt;
                
                var items_per_page = 5;
                var offset = (currentPage - 1) * items_per_page;
                pageCount = Math.ceil(totalCount / items_per_page);

                connection.query("SELECT * FROM categories WHERE status != '2' ORDER BY id DESC LIMIT "+offset+", "+items_per_page+" ", function(err, rows) {
                    if (err) {
                        req.flash('categoryMessage', 'Oops! Something went wrong');
                        res.render('admin/dashboard', {
                            message: req.flash('categoryMessage')
                        });
                    } else {
                        res.render('admin/categoryindex', {
                            Categories: rows,
                            currentPage: currentPage,
                            pageCount: pageCount,
                            message: req.flash('categoryMessage')
                        });
                    }
                });
            });
        });
    });

    //category create
    app.get('/admin/category/categorycreate', isLoggedIn, function (req, res) {
        res.render('admin/categorycreate', {
            message: req.flash('categoryMessage')
        });
    });

    //create form action
    app.post('/admin/category/categorycreate', isLoggedIn, multerUpload.single('photo'), function (req, res) {
        var post = req.body;
        req.getConnection(function(err, connection) {
            if (err) {
                req.flash('categoryMessage', err);
                res.render('admin/categorycreate', {
                    message: req.flash('categoryMessage')
                });
            } else {
                var photo = "";

                if (typeof(req.file) != 'undefined' && req.file.filename) {
                    photo = req.file.filename;
                }
                
                connection.query("INSERT INTO categories (name, photo, status, created_by, created_date) VALUES (?, ?, 1, 1, NOW())", [post.name, photo], function(err, categories) {
                    if (err) {
                        console.log(err);
                        req.flash('categoryMessage', err);
                        res.render('admin/categorycreate', {
                            message: req.flash('categoryMessage')
                        });
                    } else {
                        //req.flash('categoryMessage', 'created Successfully');
                        res.redirect('/admin/category/categoryindex');
                    }
                });
            }
        });
    });

    //update navigation
    app.get('/admin/category/categoryedit', isLoggedIn, function (req, res) {
        req.getConnection(function(err, connection) {
            if (err) {
                throw err
            }else{
                connection.query("SELECT * FROM categories WHERE id = '"+req.query.id+"' AND status = '1'", function(err, rows) {
                    res.render('admin/categoryedit', {
                        Categories: rows,
                        message: req.flash('categoryMessage'),
                    });
                });
            }
        });
    });

    //update category form post
    app.post('/admin/category/categoryedit', isLoggedIn, multerUpload.single('photo'), function(req, res, next) {
        var post = req.body;
        req.getConnection(function(err, connection) {
            if (err) {
                throw err
            } else {
                var t_date = new Date();
                var set_condition = "modified_by= '" + post.id + "', modified_date= '" + t_date + "'";
                var set_cond_arr = [];
                if (typeof(post.name) != "undefined" && post.name) {
                    set_condition += ", name=?";
                    set_cond_arr.push(post.name);
                }
                if (typeof(req.file) != "undefined" && req.file.filename) {
                    set_condition += ", photo=?";
                    set_cond_arr.push(req.file.filename);
                }
                connection.query("UPDATE categories SET " + set_condition + " WHERE id = '" + post.id + "'", set_cond_arr, function(err, data) {
                    if (err) {
                        throw err;
                    } else {
                        res.redirect('/admin/category/categoryindex');
                    }
                });
            }
        });
    });

    //Active inactive category 
    app.post('/admin/category/activedeactive' , function(req, res, next){
        req.getConnection(function(err , connection){
            if(err){
                throw err;
            }else{
                connection.query("UPDATE categories SET status = '"+req.body.status+"' WHERE id = "+req.body.id+" " , function(err, data){
                    if(err){
                        throw err;
                    }
                    else{
                        res.send({
                            "status":1
                        });   
                    }
                });
            }
        });
    });

    //Delete category 
    app.post('/admin/category/categorydelete' , function(req, res, next){
        req.getConnection(function(err , connection){
            if(err){
                throw err;
            }else{
                connection.query("UPDATE categories SET status = '2' WHERE id = "+req.body.id+" " , function(err, data){
                    if(err){
                        throw err;
                    }
                    else{
                        res.send({
                            "status":1
                        });   
                    }
                });
            }
        });
    });
}

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}

//Fetch master function
function fetchMaster(master_name, req, done) {
    req.getConnection(function(err, connection) {
        connection.query("SELECT * FROM "+master_name+" WHERE status = 1", function(err, rows) {
            if (err) {
               done(false);
            } else {
                if (rows.length > 0) {
                    done(rows);
                } else {
                    done(rows);
                }
            }
        });
    });
}
