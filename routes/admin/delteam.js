var express = require('express');
var router = express.Router();
var path = require('path');
var multer = require('multer');
var mkdirp = require('mkdirp');
var bcrypt = require('bcryptjs');
var http = require('http');

var storage = multer.memoryStorage();

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads/delteam/')
    },
    filename: function(req, file, cb) {
        var getFileExt = function(fileName) {
            var fileExt = fileName.split(".");
            if (fileExt.length === 1 || (fileExt[0] === "" && fileExt.length === 2)) {
                return "";
            }
            return fileExt.pop();
        }
        var microsecond = Math.round(new Date().getTime() / 1000 * Math.floor(Math.random() * 1000000000)); //new Date().getTime();
        cb(null, microsecond + path.extname(file.originalname)); // Date.now() + '.' + getFileExt(file.originalname))
    }
});

var multerUpload = multer({storage: storage});

module.exports = function (app, passport) {
    app.get('/admin/delteam/delteamindex', isLoggedIn, function (req, res) {
        var currentPage = 1;
        var pageCount = 0;
        var totalCount = 0;
        if(typeof (req.query.page) != "undefined" && req.query.page > 0){
            var currentPage = req.query.page;
        }
        req.getConnection(function(err, connection) {
            connection.query("SELECT count(*) AS cnt FROM delivery_team WHERE status != '2' ORDER BY id DESC", function(err, rowsCount) {
                if(err) throw err;
                totalCount = rowsCount[0].cnt;
                
                var items_per_page = 5;
                var offset = (currentPage - 1) * items_per_page;
                pageCount = Math.ceil(totalCount / items_per_page);
                
                connection.query("SELECT * FROM delivery_team WHERE status != '2' ORDER BY id DESC LIMIT "+offset+", "+items_per_page+" ", function(err, rows) {
                    if (err) {
                        req.flash('delteamMessage', 'Oops! Something went wrong');
                        res.render('admin/dashboard', {
                            message: req.flash('delteamMessage')
                        });
                    } else {
                        res.render('admin/delteamindex', {
                            Delteams: rows,
                            currentPage: currentPage,
                            pageCount: pageCount,
                            message: req.flash('delteamMessage'),
                        });
                    }
                });
            });
        });
    });

    //delteam create
    app.get('/admin/delteam/delteamcreate', isLoggedIn, function (req, res) {
        res.render('admin/delteamcreate', {
            message: req.flash('delteamMessage')
        });
    });

    //create form action
    app.post('/admin/delteam/delteamcreate', isLoggedIn, multerUpload.single('profile_photo'), function (req, res) {
        var post = req.body;
        req.getConnection(function(err, connection) {
            if (err) {
                req.flash('delteamMessage', err);
                res.render('admin/delteamcreate', {
                    message: req.flash('delteamMessage')
                });
            } else {
                var profile_photo = "";

                if (typeof(req.file) != 'undefined' && req.file.filename) {
                    profile_photo = req.file.filename;
                }
                
                connection.query("INSERT INTO delivery_team (first_name, last_name, email, phone, profile_photo, status, created_date, created_by) VALUES (?, ?, ?, ?, ?, 1, NOW(), 1)", [post.first_name, post.last_name, post.email, post.phone, profile_photo], function(err, teamdata) {
                    console.log("teamdata" + teamdata);
                    if (err) {
                        console.log(err);
                        req.flash('delteamMessage', err);
                        res.render('admin/delteamcreate', {
                            message: req.flash('delteamMessage')
                        });
                    } else {
                        req.flash('delteamMessage', 'Created Successfully');
                        res.redirect('/admin/delteam/delteamindex');
                    }
                });
            }
        });
    });

    //update navigation
    app.get('/admin/delteam/delteamedit', isLoggedIn, function (req, res) {
        req.getConnection(function(err, connection) {
            if (err) {
                throw err;
            } else {
                connection.query("SELECT u.* FROM delivery_team AS u WHERE u.id = '"+req.query.id+"' AND u.status = '1'", function(err, rows) {
                    res.render('admin/delteamedit', {
                        Delteams: rows,
                        message: req.flash('delteamMessage')
                    });
                });
            }
        });
    });

    //update delteam form post
    app.post('/admin/delteam/delteamedit', isLoggedIn, multerUpload.single('profile_photo'), function(req, res, next) {
        var post = req.body;
        req.getConnection(function(err, connection) {
            if (err) {
                throw err
            } else {
                var t_date = new Date();
                var set_condition = "modified_by= '1', modified_date= '" + t_date + "'";
                var set_cond_arr = [];
                if (typeof(post.first_name) != "undefined" && post.first_name) {
                    set_condition += ", first_name=?";
                    set_cond_arr.push(post.first_name);
                }
                if (typeof(post.last_name) != "undefined" && post.last_name) {
                    set_condition += ", last_name=?";
                    set_cond_arr.push(post.last_name);
                }
                if (typeof(req.file) != "undefined" && req.file.filename) {
                    set_condition += ", profile_photo=?";
                    set_cond_arr.push(req.file.filename);
                }
                if (typeof(post.phone) != "undefined" && post.phone) {
                    set_condition += ", phone=?";
                    set_cond_arr.push(post.phone);
                }
                if (typeof(post.email) != "undefined" && post.email) {
                    set_condition += ", email=?";
                    set_cond_arr.push(post.email);
                }
                connection.query("UPDATE delivery_team SET " + set_condition + " WHERE id = '" + post.id + "'", set_cond_arr, function(err, data) {
                    if (err) {
                        throw err;
                    } else {
                        req.flash('delteamMessage', 'Data has been updated successfully');
                        res.redirect('/admin/delteam/delteamindex');
                    }
                });
            }
        });
    });

    //Active inactive delteam 
    app.post('/admin/delteam/activedeactive' , function(req, res, next){
        req.getConnection(function(err , connection){
            if(err){
                throw err;
            }else{
                connection.query("UPDATE delivery_team SET status = '"+req.body.status+"' WHERE id = "+req.body.id+" " , function(err, data){
                    if(err){
                        throw err;
                    }
                    else{
                        res.send({
                            "status":1
                        });   
                    }
                });
            }
        });
    });

    //Delete delteam 
    app.post('/admin/delteam/delteamdelete' , function(req, res, next){
        req.getConnection(function(err , connection){
            if(err){
                throw err;
            }else{
                connection.query("UPDATE delivery_team SET status = '2' WHERE id = "+req.body.id+" " , function(err, data){
                    if(err){
                        throw err;
                    }
                    else{
                        res.send({
                            "status":1
                        });   
                    }
                });
            }
        });
    });
}

// route middleware to ensure delteam is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}
