var express = require('express');
var router = express.Router();
var path = require('path');
var multer = require('multer');
var mkdirp = require('mkdirp');
var bcrypt = require('bcryptjs');
var http = require('http');

var storage = multer.memoryStorage();

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads/product/')
    },
    filename: function(req, file, cb) {
        var getFileExt = function(fileName) {
            var fileExt = fileName.split(".");
            if (fileExt.length === 1 || (fileExt[0] === "" && fileExt.length === 2)) {
                return "";
            }
            return fileExt.pop();
        }
        var microsecond = Math.round(new Date().getTime() / 1000 * Math.floor(Math.random() * 1000000000)); //new Date().getTime();
        cb(null, microsecond + path.extname(file.originalname)); // Date.now() + '.' + getFileExt(file.originalname))
    }
});

var multerUpload = multer({storage: storage});

module.exports = function (app, passport) {
    app.get('/admin/product/productindex', isLoggedIn, function (req, res) {
        req.getConnection(function(err, connection) {
            if(err) throw err;
            var currentPage = 1;
            var pageCount = 0;
            var totalCount = 0;
            if(typeof (req.query.page) != "undefined" && req.query.page > 0){
                var currentPage = req.query.page;
            }
            connection.query("SELECT count(*) AS cnt FROM product AS p LEFT JOIN sub_categories AS s ON s.id = p.subcategory_id LEFT JOIN categories AS c ON c.id = s.category_id WHERE p.status != '2' ORDER BY p.id DESC", function(err, rowsCount) {
                if(err) throw err;
                totalCount = rowsCount[0].cnt;
                
                var items_per_page = 5;
                var offset = (currentPage - 1) * items_per_page;
                pageCount = Math.ceil(totalCount / items_per_page);

                connection.query("SELECT p.*, c.id AS category_id, c.name AS category_name, s.id AS subcategory_id, s.name AS subcategory_name FROM product AS p LEFT JOIN sub_categories AS s ON s.id = p.subcategory_id LEFT JOIN categories AS c ON c.id = s.category_id WHERE p.status != '2' ORDER BY p.id DESC LIMIT "+offset+", "+items_per_page+" ", function(err, rows) {
                    if (err) {
                        req.flash('productMessage', 'Oops! Something went wrong');
                        res.render('admin/dashboard', {
                            message: req.flash('productMessage')
                        });
                    } else {
                        res.render('admin/productindex', {
                            Products: rows,
                            currentPage: currentPage,
                            pageCount: pageCount,
                            message: req.flash('productMessage')
                        });
                    }
                });
            });
        });
    });

    //product create
    app.get('/admin/product/productcreate', isLoggedIn, function (req, res) {
        fetchMaster("categories", req, function(category_data){
            res.render('admin/productcreate', {
                Categories: category_data,
                message: req.flash('productMessage')
            });
        });
    });

    //create form action
    app.post('/admin/product/productcreate', isLoggedIn, multerUpload.single('image'), function (req, res) {
        var post = req.body;
        req.getConnection(function(err, connection) {
            if (err) {
                req.flash('productMessage', err);
                fetchMaster("categories", req, function(category_data){
                    res.render('admin/productcreate', {
                        Categories: category_data,
                        message: req.flash('productMessage')
                    });
                });
            } else {
                var image = "";
                var sku = "";
                if (typeof(req.file) != 'undefined' && req.file.filename) {
                    image = req.file.filename;
                }
                if (typeof(post.flavours_id) != "undefined" && post.flavours_id) {
                    var flavours_id = post.flavours_id;
                    var fla_ids = (typeof(flavours_id)!="object") ? flavours_id : flavours_id.join(",");
                }
                connection.query("INSERT INTO product (name, description, price, image, category_id, subcategory_id, brand_id, flavours_id, zipcode, sku, status, created_by, created_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1, 1, NOW())", [post.name, post.description, post.price, image, post.category_id, post.subcategory_id, post.brand_id, fla_ids, post.zipcode, sku], function(err, categories) {
                    if (err) {
                        console.log(err);
                        req.flash('productMessage', err);
                        fetchMaster("categories", req, function(category_data){
                            res.render('admin/productcreate', {
                                Categories: category_data,
                                message: req.flash('productMessage')
                            });
                        });
                    } else {
                        //req.flash('productMessage', 'created Successfully');
                        res.redirect('/admin/product/productindex');
                    }
                });
            }
        });
    });

    //update navigation
    app.get('/admin/product/productedit', isLoggedIn, function (req, res) {
        req.getConnection(function(err, connection) {
            if (err) {
                throw err
            }else{
                connection.query("SELECT p.*, c.id AS category_id, c.name AS category_name, s.id AS subcategory_id, b.id AS brand_id, b.name AS brand_name, s.name AS subcategory_name FROM product AS p LEFT JOIN sub_categories AS s ON s.id = p.subcategory_id LEFT JOIN brands AS b ON b.id = p.brand_id LEFT JOIN categories AS c ON c.id = s.category_id WHERE p.id = '"+req.query.id+"' AND p.status != '2' ORDER BY p.id DESC", function(err, rows) {
                    if (err) {
                        throw err;
                    }else{
                        fetchMaster("categories", req, function(category_data){
                            res.render('admin/productedit', {
                                Products: rows,
                                Categories: category_data,
                                message: req.flash('productMessage')
                            });
                        });
                    }
                });
            }
        });
    });

    //update product form post
    app.post('/admin/product/productedit', isLoggedIn, multerUpload.single('image'), function(req, res, next) {
        var post = req.body;
        req.getConnection(function(err, connection) {
            if (err) {
                throw err
            } else {
                var t_date = new Date();
                var set_condition = "modified_by= '" + post.id + "', modified_date= '" + t_date + "'";
                var set_cond_arr = [];
                if (typeof(post.name) != "undefined" && post.name) {
                    set_condition += ", name=?";
                    set_cond_arr.push(post.name);
                }
                if (typeof(post.description) != "undefined" && post.description) {
                    set_condition += ", description=?";
                    set_cond_arr.push(post.description);
                }
                if (typeof(post.price) != "undefined" && post.price) {
                    set_condition += ", price=?";
                    set_cond_arr.push(post.price);
                }
                if (typeof(post.zipcode) != "undefined" && post.zipcode) {
                    set_condition += ", zipcode=?";
                    set_cond_arr.push(post.zipcode);
                }
                if (typeof(post.category_id) != "undefined" && post.category_id) {
                    set_condition += ", category_id=?";
                    set_cond_arr.push(post.category_id);
                }
                if (typeof(post.subcategory_id) != "undefined" && post.subcategory_id) {
                    set_condition += ", subcategory_id=?";
                    set_cond_arr.push(post.subcategory_id);
                }
                if (typeof(post.brand_id) != "undefined" && post.brand_id) {
                    set_condition += ", brand_id=?";
                    set_cond_arr.push(post.brand_id);
                }
                if (typeof(req.file) != "undefined" && req.file.filename) {
                    set_condition += ", image=?";
                    set_cond_arr.push(req.file.filename);
                }
                if (typeof(post.flavours_id) != "undefined" && post.flavours_id) {
                    var flavours_id = post.flavours_id;
                    console.log("flavours ID", typeof(flavours_id));
                    var fla_ids = (typeof(flavours_id)!="object") ? flavours_id : flavours_id.join(",");
                    set_condition += ", flavours_id=?";
                    set_cond_arr.push(fla_ids);
                }
                connection.query("UPDATE product SET " + set_condition + " WHERE id = '" + post.id + "'", set_cond_arr, function(err, data) {
                    if (err) {
                        throw err;
                    } else {
                        res.redirect('/admin/product/productindex');
                    }
                });
            }
        });
    });

    //Active inactive product
    app.post('/admin/product/activedeactive' , function(req, res, next){
        req.getConnection(function(err , connection){
            if(err){
                throw err;
            }else{
                connection.query("UPDATE product SET status = '"+req.body.status+"' WHERE id = "+req.body.id+" " , function(err, data){
                    if(err){
                        throw err;
                    }
                    else{
                        res.send({
                            "status":1
                        });   
                    }
                });
            }
        });
    });

    //Delete product 
    app.post('/admin/product/productdelete' , function(req, res, next){
        req.getConnection(function(err , connection){
            if(err){
                throw err;
            }else{
                connection.query("UPDATE product SET status = '2' WHERE id = "+req.body.id+" " , function(err, data){
                    if(err){
                        throw err;
                    }
                    else{
                        res.send({
                            "status":1
                        });   
                    }
                });
            }
        });
    });
}

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}

//Fetch master function
function fetchMaster(master_name, req, done) {
    req.getConnection(function(err, connection) {
        connection.query("SELECT * FROM "+master_name+" WHERE status = 1", function(err, rows) {
            if (err) {
               done(false);
            } else {
                if (rows.length > 0) {
                    done(rows);
                } else {
                    done(rows);
                }
            }
        });
    });
}