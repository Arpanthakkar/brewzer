var express = require('express');
var router = express.Router();
var multer = require('multer');
var path    = require('path');
var http = require('http');
var async = require('async');
var pushfunc = require('../../config/pushnotification');
var push_function = pushfunc.notification();
var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads/ad_photo/')
    },
    filename: function(req, file, cb) {
        var getFileExt = function(fileName) {
            var fileExt = fileName.split(".");
            if (fileExt.length === 1 || (fileExt[0] === "" && fileExt.length === 2)) {
                return "";
            }
            return fileExt.pop();
        }
        var microsecond = Math.round(new Date().getTime() / 1000 * Math.floor(Math.random() * 1000000000)); //new Date().getTime();
        cb(null, microsecond + path.extname(file.originalname)); // Date.now() + '.' + getFileExt(file.originalname))
    }
});
var multerUpload = multer({storage: storage});

var upload = new multer();

module.exports = function (app, passport) {
    app.get('/admin/notification/send', isLoggedIn, function (req, res) {
        res.render('admin/notification', {
            session: req.session,
            status: 1,
            message: req.flash('notificationMessage')
        });
    });
    
    app.post('/admin/notification/send', isLoggedIn, multerUpload.single('ad_photo'), function(req, res, next) {
        var post = req.body;
        // get token of all users
        req.getConnection(function(err, connection) {
            var message = post.message;
            var body = {};
            var image = "";
            if (typeof(req.file) != 'undefined' && req.file.filename) {
                ad_photo = req.file.filename;
                body.image = "http://" + req.headers.host + "/ad_photo/" + req.file.filename;
            }
            body.message = message;
            body.type = 1; //1=Global, 2=Place, 3=Event

            connection.query("SELECT * from user WHERE push_notification = '1' AND role = '3' AND status != '2'", function(err, rows) {
                if(err) throw err;
                async.forEachOf(rows, function(value, key, callback) {
                    if(typeof(value.device_token)!="undefined" && value.device_token!="" && typeof(value.device_type)!="undefined" && value.device_type!=""){
                        body.user_id = value.id;
                        
                        var device_token = value.device_token;
                        
                        // push notification code   
                        push_function.sendNotification(device_token, message, body, req);

                        //save notification for users
                        push_function.saveNotification(body, message, req);
                    }
                    callback();
                },function(err) {
                    req.flash('notificationMessage', 'Notification has been sent Successfully');
                    res.render('admin/notification', {
                        session: req.session,
                        status:1,
                        message: req.flash('notificationMessage')
                    });
                });
            });
        });        
    });
}

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}