var express = require('express');
var router = express.Router();
var path = require('path');
var multer = require('multer');
var mkdirp = require('mkdirp');
var bcrypt = require('bcryptjs');
var http = require('http');

var storage = multer.memoryStorage();

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads/user/')
    },
    filename: function(req, file, cb) {
        var getFileExt = function(fileName) {
            var fileExt = fileName.split(".");
            if (fileExt.length === 1 || (fileExt[0] === "" && fileExt.length === 2)) {
                return "";
            }
            return fileExt.pop();
        }
        var microsecond = Math.round(new Date().getTime() / 1000 * Math.floor(Math.random() * 1000000000)); //new Date().getTime();
        cb(null, microsecond + path.extname(file.originalname)); // Date.now() + '.' + getFileExt(file.originalname))
    }
});

var multerUpload = multer({storage: storage});

module.exports = function (app, passport) {
    app.get('/admin/user/userindex', isLoggedIn, function (req, res) {
        var currentPage = 1;
        var pageCount = 0;
        var totalCount = 0;
        if(typeof (req.query.page) != "undefined" && req.query.page > 0){
            var currentPage = req.query.page;
        }
        req.getConnection(function(err, connection) {
            connection.query("SELECT count(*) AS cnt FROM user WHERE role = '3' AND status != '2' ORDER BY id DESC", function(err, rowsCount) {
                if(err) throw err;
                totalCount = rowsCount[0].cnt;
                
                var items_per_page = 5;
                var offset = (currentPage - 1) * items_per_page;
                pageCount = Math.ceil(totalCount / items_per_page);
                
                connection.query("SELECT * FROM user WHERE role = '3' AND status != '2' ORDER BY id DESC LIMIT "+offset+", "+items_per_page+" ", function(err, rows) {
                    if (err) {
                        req.flash('userMessage', 'Oops! Something went wrong');
                        res.render('admin/dashboard', {
                            message: req.flash('userMessage')
                        });
                    } else {
                        res.render('admin/userindex', {
                            Users: rows,
                            currentPage: currentPage,
                            pageCount: pageCount,
                            message: req.flash('userMessage'),
                        });
                    }
                });
            });
        });
    });

    //user create
    app.get('/admin/user/usercreate', isLoggedIn, function (req, res) {
        fetchMaster("countries", req, function(country_data){
            res.render('admin/usercreate', {
                Countries: country_data,
                message: req.flash('userMessage')
            });
        });
    });

    //create form action
    app.post('/admin/user/usercreate', isLoggedIn, multerUpload.single('profile_photo'), function (req, res) {
        var post = req.body;
        bcrypt.genSalt(10, function(err, salt) {
            console.log("salt for hash generated");
            if (err) {
                //return next(err);
                req.flash('userMessage', err);
                fetchMaster("countries", req, function(country_data){
                    res.render('admin/usercreate', {
                        Countries: country_data,
                        message: req.flash('userMessage')
                    });
                });
            } else {
                bcrypt.hash(req.body.password, salt, function(err, hash) {
                    console.log("hash generated");
                    if (err) {
                        //return next(err);
                        req.flash('userMessage', err);
                        fetchMaster("countries", req, function(country_data){
                            res.render('admin/usercreate', {
                                Countries: country_data,
                                message: req.flash('userMessage')
                            });
                        });
                    } else {
                        var password_hash = hash;
                        req.getConnection(function(err, connection) {
                            if (err) {
                                req.flash('userMessage', err);
                                fetchMaster("countries", req, function(country_data){
                                    res.render('admin/usercreate', {
                                        Countries: country_data,
                                        message: req.flash('userMessage')
                                    });
                                });
                            } else {
                                var profile_photo = "";
                                var birthdate = "";

                                if (typeof(req.file) != 'undefined' && req.file.filename) {
                                    profile_photo = req.file.filename;
                                }
                                if (typeof(post.birthdate) != "undefined" && post.birthdate) {
                                    birthdate = post.birthdate;
                                }
                                
                                connection.query("SELECT  COUNT(*) as cnt FROM user WHERE email = '" + post.email + "' AND status!='2'", function(err, checkemail) {
                                    console.log("count" + checkemail[0].cnt);
                                    if (checkemail[0].cnt == 0) {
                                        validateNumber(post.phone, function(phone_data){
                                            var phone_international = phone_data.international_phone;
                                            var phone_national = phone_data.national_phone;
                                            var phone_e164 = phone_data.E164_phone;
                                            var address2 = (typeof(post.address2) != "undefined" && post.address2!="") ? post.address2 : "";

                                            connection.query("INSERT INTO user (first_name , email, birthdate, address1, address2, country_id, state_id, city_id, phone, phone_international, phone_national, phone_e164, photo_id, profile_photo , status, created_date , chat_notification, created_by, role, auth_key , password_hash) VALUES ('" + post.first_name + "', '" + post.email + "', '"+post.birthdate+"', '"+post.address1+"', '"+address2+"', '"+post.country_id+"', '"+post.state_id+"', '"+post.city_id+"', '" + post.phone + "', '" + phone_international + "', '" + phone_national + "', '" + phone_e164 + "', '" + post.photo_id + "', '" + profile_photo + "' , 1 , NOW() , 1 , 1 , 3, '" + password_hash + "' , '" + password_hash + "')", function(err, signup) {
                                                console.log("signup" + signup);
                                                if (err) {
                                                    console.log(err);
                                                    req.flash('userMessage', err);
                                                    fetchMaster("countries", req, function(country_data){
                                                        res.render('admin/usercreate', {
                                                            Countries: country_data,
                                                            message: req.flash('userMessage')
                                                        });
                                                    });
                                                } else {
                                                    //req.flash('userMessage', 'created Successfully');
                                                    res.redirect('/admin/user/userindex');
                                                }
                                            });
                                        });                                        
                                    } else {
                                        req.flash('userMessage', 'Email Already exist');
                                        fetchMaster("countries", req, function(country_data){
                                            res.render('admin/usercreate', {
                                                Countries: country_data,
                                                message: req.flash('userMessage')
                                            });
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    });

    //update navigation
    app.get('/admin/user/useredit', isLoggedIn, function (req, res) {
        req.getConnection(function(err, connection) {
            if (err) {
                throw err;
            } else {
                connection.query("SELECT u.*, c.name AS country_name, s.name AS state_name, ci.name AS city_name FROM user AS u LEFT JOIN countries AS c ON c.id = u.country_id LEFT JOIN states AS s ON s.id = u.state_id LEFT JOIN cities AS ci ON ci.id = u.city_id WHERE u.id = '"+req.query.id+"' AND u.status = '1'", function(err, rows) {
                    fetchMaster("countries", req, function(country_data){
                        res.render('admin/useredit', {
                            Countries: country_data,
                            Users: rows,
                            message: req.flash('userMessage')
                        });
                    });
                });
            }
        });
    });

    //update user form post
    app.post('/admin/user/useredit', isLoggedIn, multerUpload.single('profile_photo'), function(req, res, next) {
        var post = req.body;
        req.getConnection(function(err, connection) {
            if (err) {
                throw err
            } else {
                var t_date = new Date();
                var set_condition = "modified_by= '" + post.id + "', modified_date= '" + t_date + "'";

                if (typeof(post.first_name) != "undefined" && post.first_name) {
                    set_condition += ", first_name='" + post.first_name + "'";
                }
                if (typeof(post.birthdate) != "undefined" && post.birthdate) {
                    set_condition += ", birthdate='" + post.birthdate + "'";
                }
                if (typeof(post.address1) != "undefined" && post.address1) {
                    set_condition += ", address1='" + post.address1 + "'";
                }
                if (typeof(post.address2) != "undefined") {
                    set_condition += ", address2='" + post.address2 + "'";
                }
                if (typeof(post.country_id) != "undefined" && post.country_id) {
                    set_condition += ", country_id='" + post.country_id + "'";
                }
                if (typeof(post.state_id) != "undefined" && post.state_id) {
                    set_condition += ", state_id='" + post.state_id + "'";
                }
                if (typeof(post.city_id) != "undefined" && post.city_id) {
                    set_condition += ", city_id='" + post.city_id + "'";
                }
                if (typeof(post.photo_id) != "undefined" && post.photo_id) {
                    set_condition += ", photo_id='" + post.photo_id + "'";
                }
                if (typeof(post.first_name) != "undefined" && post.first_name) {
                    set_condition += ", first_name='" + post.first_name + "'";
                }
                if (typeof(req.file) != "undefined" && req.file.filename) {
                    set_condition += ", profile_photo='" + req.file.filename + "'";
                }
                if (typeof(post.phone) != "undefined" && post.phone) {
                    connection.query("SELECT * from user WHERE phone = '" + post.phone + "' AND id != '" + post.id + "' AND status = 1", function(err, data) {
                        if (err) {
                            response.status = 0;
                            response.message = err;
                            res.send(response);
                        } else if (data.length == 0) {
                            validateNumber(post.phone, function(phone_data){
                                var phone_international = phone_data.international_phone;
                                var phone_national = phone_data.national_phone;
                                var phone_e164 = phone_data.E164_phone;

                                set_condition += ", country_code='', phone='" + post.phone + "', phone_international='" + phone_international + "', phone_national='" + phone_national + "', phone_e164='" + phone_e164 + "'";

                                connection.query("UPDATE user SET " + set_condition + " WHERE id = '" + post.id + "'", function(err, data) {
                                    if (err) {
                                        throw err;
                                    } else {
                                        res.redirect('/admin/user/userindex');
                                    }
                                });
                            });
                        } else {
                            req.flash('userMessage', 'Phone number is already exist');
                            connection.query("SELECT u.*, c.name AS country_name, s.name AS state_name, ci.name AS city_name FROM user AS u LEFT JOIN countries AS c ON c.id = u.country_id LEFT JOIN states AS s ON s.id = u.state_id LEFT JOIN cities AS ci ON ci.id = u.city_id WHERE u.id = '"+req.post.id+"' AND u.status = '1'", function(err, rows) {
                                fetchMaster("countries", req, function(country_data){
                                    res.render('admin/useredit', {
                                        Countries: country_data,
                                        Users: rows,
                                        message: req.flash('userMessage')
                                    });
                                });
                            });
                        }
                    });
                } else {
                    connection.query("UPDATE user SET " + set_condition + " WHERE id = '" + post.id + "'", function(err, data) {
                        if (err) {
                            throw err;
                        } else {
                            res.redirect('/admin/user/userindex');
                        }
                    });
                }
            }
        });
    });

    //Active inactive user 
    app.post('/admin/user/activedeactive' , function(req, res, next){
        req.getConnection(function(err , connection){
            if(err){
                throw err;
            }else{
                connection.query("UPDATE user SET status = '"+req.body.status+"' WHERE id = "+req.body.id+" " , function(err, data){
                    if(err){
                        throw err;
                    }
                    else{
                        res.send({
                            "status":1
                        });   
                    }
                });
            }
        });
    });

    //Delete user 
    app.post('/admin/user/userdelete' , function(req, res, next){
        req.getConnection(function(err , connection){
            if(err){
                throw err;
            }else{
                connection.query("UPDATE user SET status = '2' WHERE id = "+req.body.id+" " , function(err, data){
                    if(err){
                        throw err;
                    }
                    else{
                        res.send({
                            "status":1
                        });   
                    }
                });
            }
        });
    });

    //Get dynamic dropdown
    app.post('/admin/user/getdynamicdropdown' , function(req, res, next){
        req.getConnection(function(err , connection){
            if(err){
                throw err;
            }else{
                var WHERE = "status = 1";
                var lab_str = "";
                if(req.body.table == "states"){
                    lab_str = "State";
                    WHERE += " AND country_id = "+req.body.id+" ";
                }else if(req.body.table == "cities"){
                    lab_str = "City";
                    WHERE += " AND state_id = "+req.body.id+" ";
                }else if(req.body.table == "sub_categories"){
                    lab_str = "Sub Category";
                    WHERE += " AND category_id = "+req.body.id+" ";
                }else if(req.body.table == "brands"){
                    lab_str = "Brand";
                    WHERE += " AND category_id = "+req.body.id+" ";
                }
                connection.query("SELECT * FROM "+req.body.table+" WHERE "+WHERE+" " , function(err, data){
                    if(err){
                        throw err;
                    }
                    else{
                        var dropdown = "<option value=''>Select "+lab_str+"</option>";
                        data.forEach(function(newdata){
                            dropdown += "<option value='"+newdata.id+"'>"+newdata.name+"</option>";
                        });
                        res.send({
                            "status":1,
                            "data":dropdown,
                        });   
                    }
                });
            }
        });
    });
}

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}

//Fetch master function
function fetchMaster(master_name, req, done) {
    req.getConnection(function(err, connection) {
        connection.query("SELECT * FROM "+master_name+" WHERE status = 1", function(err, rows) {
            if (err) {
               done(false);
            } else {
                if (rows.length > 0) {
                    done(rows);
                } else {
                    done(rows);
                }
            }
        });
    });
}

function validateNumber(phone, done) {
    // Require `PhoneNumberFormat`.
    var PNF = require('google-libphonenumber').PhoneNumberFormat;

    // Get an instance of `PhoneNumberUtil`.
    var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

    // Parse number with country code.
    var phoneNumber = phoneUtil.parse(phone, 'US');

    var national_phone = phoneUtil.format(phoneNumber, PNF.NATIONAL);
    var international_phone = phoneUtil.format(phoneNumber, PNF.INTERNATIONAL);
    var E164_phone = phoneUtil.format(phoneNumber, PNF.E164);
    var phone_data = {};
    phone_data = {
        national_phone: national_phone,
        international_phone: international_phone,
        E164_phone: E164_phone
    }
    return done(phone_data);
}
