var express = require('express');
var router = express.Router();
var path = require('path');
var multer = require('multer');
var mkdirp = require('mkdirp');
var bcrypt = require('bcryptjs');
var http = require('http');

var storage = multer.memoryStorage();

var upload = multer();

module.exports = function (app, passport) {
    app.get('/admin/cms/cmsindex', isLoggedIn, function (req, res) {
        req.getConnection(function(err, connection) {
            if(err) throw err;
            var currentPage = 1;
            var pageCount = 0;
            var totalCount = 0;
            var keyword = "";
            var searchwhere = "";
            var ordercond = "ORDER BY id DESC";
            if(typeof (req.query.page) != "undefined" && req.query.page > 0){
                var currentPage = req.query.page;
            }
            if(typeof (req.query.keyword) != "undefined" && req.query.keyword){
                keyword = req.query.keyword;
                searchwhere = "AND (`content` LIKE '%"+keyword+"%'";
                searchwhere += "OR `title` LIKE '%"+keyword+"%' )";
            }
            if(typeof (req.query.sort) != "undefined" && typeof (req.query.sort_by) != "undefined" && req.query.sort && req.query.sort_by){
                ordercond = "ORDER BY `"+req.query.sort+"` "+req.query.sort_by+"";
            }
            connection.query("SELECT count(*) AS cnt FROM cms_management WHERE status != '2' "+searchwhere+" ", function(err, rowsCount) {
                if(err) throw err;
                totalCount = rowsCount[0].cnt;
                
                var items_per_page = 5;
                var offset = (currentPage - 1) * items_per_page;
                pageCount = Math.ceil(totalCount / items_per_page);

                connection.query("SELECT * FROM cms_management WHERE status != '2' "+searchwhere+" "+ordercond+" LIMIT "+offset+", "+items_per_page+" ", function(err, rows) {
                    if (err) {
                        req.flash('cmsMessage', 'Oops! Something went wrong');
                        res.render('admin/dashboard', {
                            message: req.flash('cmsMessage')
                        });
                    } else {
                        res.render('admin/cmsindex', {
                            CMS: rows,
                            queryParams: req.query,
                            currentPage: currentPage,
                            pageCount: pageCount,
                            message: req.flash('cmsMessage')
                        });
                    }
                });
            });
        });
    });

    //update navigation
    app.get('/admin/cms/cmsedit', isLoggedIn, function (req, res) {
        req.getConnection(function(err, connection) {
            if (err) {
                throw err
            }else{
                connection.query("SELECT * FROM cms_management WHERE id = '"+req.query.id+"' AND status != '2'", function(err, rows) {
                    console.log('rows >> ',rows[0].title);
                    res.render('admin/cmsedit', {
                        cms: rows[0],
                        message: req.flash('cmsMessage'),
                    });
                });
            }
        });
    });

    //update cms form post
    app.post('/admin/cms/cmsedit', isLoggedIn, upload.array(), function(req, res, next) {
        var post = req.body;
        req.getConnection(function(err, connection) {
            if (err) {
                throw err
            } else {
                var t_date = new Date();
                var set_condition = "modified_date= '" + t_date + "'";
                var whr_arr = [];

                if (typeof(post.title) != "undefined" && post.title) {
                    set_condition += ", title=?";
                    whr_arr[0] = post.title;
                }

                if (typeof(post.content) != "undefined" && post.content) {
                    set_condition += ", content=?";
                    whr_arr[1] = post.content;
                }
                
                connection.query("UPDATE cms_management SET " + set_condition + " WHERE id = '" + post.id + "'", whr_arr, function(err, data) {
                    if (err) {
                        throw err;
                    } else {
                        res.redirect('/admin/cms/cmsindex');
                    }
                });
            }
        });
    });
}

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}
