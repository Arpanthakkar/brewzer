var express = require('express');
var router = express.Router();
var multer = require('multer');
var http = require('http');

var storage = multer.memoryStorage();

var upload = multer({
    storage: storage
});

module.exports = function (app, passport) {
    app.get('/admin/dashboard', isLoggedIn, function (req, res) {
        res.render('admin/dashboard', {
            message: req.flash('loginMessage')
        });
    });
}

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}