var express = require('express');
var router = express.Router();
var path = require('path');
var multer = require('multer');
var mkdirp = require('mkdirp');
var bcrypt = require('bcryptjs');
var http = require('http');
var async = require('async');

var upload = multer();

module.exports = function (app, passport) {
    app.get('/admin/report/order', isLoggedIn, function (req, res) {
        req.getConnection(function(err, connection) {
            if(err) throw err;
            var currentPage = 1;
            var pageCount = 0;
            var totalCount = 0;
            var searchwhere = "";
            if(typeof (req.query.page) != "undefined" && req.query.page > 0){
                var currentPage = req.query.page;
            }
            if (typeof (req.query.keyword) != "undefined" && req.query.keyword) {
                var keyword = req.query.keyword;
                searchwhere += "AND (u.first_name LIKE '%" + keyword + "%' ";
                searchwhere += "OR dt.first_name LIKE '%" + keyword + "%' ";
                searchwhere += "OR dt.last_name LIKE '%" + keyword + "%') ";
            }
            if (typeof (req.query.from_price) != "undefined" && req.query.from_price) {
                var from_price = req.query.from_price;
                searchwhere += "AND o.total >= '"+from_price+"' ";
            }
            if (typeof (req.query.to_price) != "undefined" && req.query.to_price) {
                var to_price = req.query.to_price;
                searchwhere += " AND o.total <= '" + to_price + "' ";
            }
            if (typeof (req.query.from_date) != "undefined" && req.query.from_date) {
                var from_date = req.query.from_date;
                searchwhere += "AND DATE(o.created_date) >= '"+from_date+"' ";
            }
            if (typeof (req.query.to_date) != "undefined" && req.query.to_date) {
                var to_date = req.query.to_date;
                searchwhere += " AND DATE(o.created_date) <= '" + to_date + "' ";
            }
            if (typeof (req.query.order_status) != "undefined" && req.query.order_status) {
                var order_status = req.query.order_status;
                searchwhere += " AND o.order_status <= '" + order_status + "' ";
            }
            if (typeof (req.query.sort) != "undefined" && typeof (req.query.sort_by) != "undefined" && req.query.sort && req.query.sort_by) {
                ordercond = "ORDER BY " + req.query.sort + " " + req.query.sort_by + "";
            }
            console.log("SELECT COUNT(*) AS cnt FROM orders AS o LEFT JOIN user AS u ON u.id = o.user_id LEFT JOIN delivery_team AS dt ON dt.id = o.delivery_by LEFT JOIN order_survey AS os ON os.order_id = o.id LEFT JOIN coupons AS c ON c.id = o.coupon_id WHERE o.status != '2' AND u.status != '2' "+searchwhere+" ");
            connection.query("SELECT COUNT(*) AS cnt FROM orders AS o LEFT JOIN user AS u ON u.id = o.user_id LEFT JOIN delivery_team AS dt ON dt.id = o.delivery_by LEFT JOIN order_survey AS os ON os.order_id = o.id LEFT JOIN coupons AS c ON c.id = o.coupon_id WHERE o.status != '2' AND u.status != '2' "+searchwhere+" ", function(err, rowsCount) {
                if(err) throw err;
                totalCount = rowsCount[0].cnt;
                
                var items_per_page = 5;
                var offset = (currentPage - 1) * items_per_page;
                pageCount = Math.ceil(totalCount / items_per_page);

                connection.query("SELECT o.*, os.rating, dt.first_name AS dt_first_name, dt.last_name AS dt_last_name, u.first_name, c.code AS order_code FROM orders AS o LEFT JOIN user AS u ON u.id = o.user_id LEFT JOIN delivery_team AS dt ON dt.id = o.delivery_by LEFT JOIN order_survey AS os ON os.order_id = o.id LEFT JOIN coupons AS c ON c.id = o.coupon_id WHERE o.status != '2' AND u.status != '2' "+searchwhere+" ORDER BY o.id DESC LIMIT "+offset+", "+items_per_page+" ", function(err, rows) {
                    if (err) {
                        throw err;
                    } else {
                        res.render('admin/orderreport', {
                            Orders: rows,
                            currentPage: currentPage,
                            pageCount: pageCount,
                            queryParams: req.query,
                            message: req.flash('reportMessage')
                        });
                    }
                });
            });
        });
    });
}

//To format date in Y-m-d 
Date.prototype.formatDateTime = function() {
    var date = new Date(this.valueOf()),
        month = '' + (date.getMonth() + 1),
        day = '' + date.getDate(),
        year = date.getFullYear(),
        hours = date.getHours(),
        minutes = date.getMinutes(),
        seconds = date.getSeconds();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    var dateformat = [year, month, day].join('-');
    var timeformat = [hours, minutes, seconds].join(':');
    return dateformat+ ' ' +timeformat ;
}

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}