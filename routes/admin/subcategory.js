var express = require('express');
var router = express.Router();
var path = require('path');
var multer = require('multer');
var mkdirp = require('mkdirp');
var bcrypt = require('bcryptjs');
var http = require('http');

var storage = multer.memoryStorage();

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function(req, file, cb) {
        var getFileExt = function(fileName) {
            var fileExt = fileName.split(".");
            if (fileExt.length === 1 || (fileExt[0] === "" && fileExt.length === 2)) {
                return "";
            }
            return fileExt.pop();
        }
        var microsecond = Math.round(new Date().getTime() / 1000 * Math.floor(Math.random() * 1000000000)); //new Date().getTime();
        cb(null, microsecond + path.extname(file.originalname)); // Date.now() + '.' + getFileExt(file.originalname))
    }
});

var multerUpload = multer({storage: storage});

var upload = multer();

module.exports = function (app, passport) {
    app.get('/admin/subcategory/subcategoryindex', isLoggedIn, function (req, res) {
        req.getConnection(function(err, connection) {
            if(err) throw err;
            var currentPage = 1;
            var pageCount = 0;
            var totalCount = 0;
            if(typeof (req.query.page) != "undefined" && req.query.page > 0){
                var currentPage = req.query.page;
            }
            connection.query("SELECT count(*) AS cnt FROM sub_categories AS s LEFT JOIN categories AS c ON c.id = s.category_id WHERE s.status != '2'", function(err, rowsCount) {
                if(err) throw err;
                totalCount = rowsCount[0].cnt;
                
                var items_per_page = 5;
                var offset = (currentPage - 1) * items_per_page;
                pageCount = Math.ceil(totalCount / items_per_page);

                connection.query("SELECT s.*, c.id AS category_id, c.name AS category_name FROM sub_categories AS s LEFT JOIN categories AS c ON c.id = s.category_id WHERE s.status != '2' ORDER BY s.id DESC LIMIT "+offset+", "+items_per_page+" ", function(err, rows) {
                    if (err) {
                        req.flash('subcategoryMessage', 'Oops! Something went wrong');
                        res.render('admin/dashboard', {
                            message: req.flash('subcategoryMessage')
                        });
                    } else {
                        res.render('admin/subcategoryindex', {
                            Subcategories: rows,
                            currentPage: currentPage,
                            pageCount: pageCount,
                            message: req.flash('subcategoryMessage')
                        });
                    }
                });
            });
        });
    });

    //subcategory create
    app.get('/admin/subcategory/subcategorycreate', isLoggedIn, function (req, res) {
        fetchMaster("categories", req, function(category_data){
            res.render('admin/subcategorycreate', {
                Categories: category_data,
                message: req.flash('subcategoryMessage')
            });
        });
    });

    //create form action
    app.post('/admin/subcategory/subcategorycreate', isLoggedIn, upload.array(), function (req, res) {
        var post = req.body;
        req.getConnection(function(err, connection) {
            if (err) {
                req.flash('subcategoryMessage', err);
                fetchMaster("categories", req, function(category_data){
                    res.render('admin/subcategorycreate', {
                        Categories: category_data,
                        message: req.flash('subcategoryMessage')
                    });
                });
            } else {
                
                connection.query("INSERT INTO sub_categories (name, category_id, status, created_by, created_date) VALUES (?, ?, 1, 1, NOW())", [post.name, post.category_id], function(err, categories) {
                    if (err) {
                        console.log(err);
                        req.flash('subcategoryMessage', err);
                        fetchMaster("categories", req, function(category_data){
                            res.render('admin/subcategorycreate', {
                                Categories: category_data,
                                message: req.flash('subcategoryMessage')
                            });
                        });
                    } else {
                        //req.flash('subcategoryMessage', 'created Successfully');
                        res.redirect('/admin/subcategory/subcategoryindex');
                    }
                });
            }
        });
    });

    //update navigation
    app.get('/admin/subcategory/subcategoryedit', isLoggedIn, function (req, res) {
        req.getConnection(function(err, connection) {
            if (err) {
                throw err
            }else{
                connection.query("SELECT s.*, c.id AS category_id, c.name AS category_name FROM sub_categories AS s LEFT JOIN categories AS c ON c.id = s.category_id WHERE s.id='"+req.query.id+"' AND s.status != '2' ORDER BY s.id DESC", function(err, rows) {
                    if (err) {
                        throw err;
                    }else{
                        fetchMaster("categories", req, function(category_data){
                            res.render('admin/subcategoryedit', {
                                Subcategories: rows,
                                Categories: category_data,
                                message: req.flash('subcategoryMessage')
                            });
                        });
                    }
                });
            }
        });
    });

    //update subcategory form post
    app.post('/admin/subcategory/subcategoryedit', isLoggedIn, upload.array(), function(req, res, next) {
        var post = req.body;
        req.getConnection(function(err, connection) {
            if (err) {
                throw err
            } else {
                var t_date = new Date();
                var set_condition = "modified_by= '" + post.id + "', modified_date= '" + t_date + "'";
                var set_cond_arr = [];
                if (typeof(post.name) != "undefined" && post.name) {
                    set_condition += ", name=?";
                    set_cond_arr.push(post.name);
                }
                if (typeof(post.category_id) != "undefined" && post.category_id) {
                    set_condition += ", category_id=?";
                    set_cond_arr.push(post.category_id);
                }
                connection.query("UPDATE sub_categories SET " + set_condition + " WHERE id = '" + post.id + "'", set_cond_arr, function(err, data) {
                    if (err) {
                        throw err;
                    } else {
                        res.redirect('/admin/subcategory/subcategoryindex');
                    }
                });
            }
        });
    });

    //Active inactive sub-category 
    app.post('/admin/subcategory/activedeactive' , function(req, res, next){
        req.getConnection(function(err , connection){
            if(err){
                throw err;
            }else{
                connection.query("UPDATE sub_categories SET status = '"+req.body.status+"' WHERE id = "+req.body.id+" " , function(err, data){
                    if(err){
                        throw err;
                    }
                    else{
                        res.send({
                            "status":1
                        });   
                    }
                });
            }
        });
    });

    //Delete sub-category 
    app.post('/admin/subcategory/subcategorydelete' , function(req, res, next){
        req.getConnection(function(err , connection){
            if(err){
                throw err;
            }else{
                connection.query("UPDATE sub_categories SET status = '2' WHERE id = "+req.body.id+" " , function(err, data){
                    if(err){
                        throw err;
                    }
                    else{
                        res.send({
                            "status":1
                        });   
                    }
                });
            }
        });
    });
}

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}

//Fetch master function
function fetchMaster(master_name, req, done) {
    req.getConnection(function(err, connection) {
        connection.query("SELECT * FROM "+master_name+" WHERE status = 1", function(err, rows) {
            if (err) {
               done(false);
            } else {
                if (rows.length > 0) {
                    done(rows);
                } else {
                    done(rows);
                }
            }
        });
    });
}