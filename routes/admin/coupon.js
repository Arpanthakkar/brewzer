var express = require('express');
var router = express.Router();
var path = require('path');
var multer = require('multer');
var mkdirp = require('mkdirp');
var bcrypt = require('bcryptjs');
var http = require('http');

var storage = multer.memoryStorage();

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function(req, file, cb) {
        var getFileExt = function(fileName) {
            var fileExt = fileName.split(".");
            if (fileExt.length === 1 || (fileExt[0] === "" && fileExt.length === 2)) {
                return "";
            }
            return fileExt.pop();
        }
        var microsecond = Math.round(new Date().getTime() / 1000 * Math.floor(Math.random() * 1000000000)); //new Date().getTime();
        cb(null, microsecond + path.extname(file.originalname)); // Date.now() + '.' + getFileExt(file.originalname))
    }
});

var multerUpload = multer({storage: storage});
var upload = multer();

module.exports = function (app, passport) {
    app.get('/admin/coupon/couponindex', isLoggedIn, function (req, res) {
        req.getConnection(function(err, connection) {
            if(err) throw err;
            var currentPage = 1;
            var pageCount = 0;
            var totalCount = 0;
            if(typeof (req.query.page) != "undefined" && req.query.page > 0){
                var currentPage = req.query.page;
            }
            connection.query("SELECT count(*) AS cnt FROM coupons WHERE status != '2'", function(err, rowsCount) {
                if(err) throw err;
                totalCount = rowsCount[0].cnt;
                
                var items_per_page = 5;
                var offset = (currentPage - 1) * items_per_page;
                pageCount = Math.ceil(totalCount / items_per_page);

                connection.query("SELECT * FROM coupons WHERE status != '2' ORDER BY id DESC LIMIT "+offset+", "+items_per_page+" ", function(err, rows) {
                    if (err) {
                        req.flash('couponMessage', 'Oops! Something went wrong');
                        res.render('admin/dashboard', {
                            message: req.flash('couponMessage')
                        });
                    } else {
                        res.render('admin/couponindex', {
                            Coupons: rows,
                            currentPage: currentPage,
                            pageCount: pageCount,
                            message: req.flash('couponMessage')
                        });
                    }
                });
            });
        });
    });

    //coupon create
    app.get('/admin/coupon/couponcreate', isLoggedIn, function (req, res) {
        res.render('admin/couponcreate', {
            message: req.flash('couponMessage')
        });
    });

    //create form action
    app.post('/admin/coupon/couponcreate', isLoggedIn, upload.array(), function (req, res) {
        var post = req.body;
        req.getConnection(function(err, connection) {
            if (err) {
                req.flash('couponMessage', err);
                res.render('admin/couponcreate', {
                    message: req.flash('couponMessage')
                });
            } else {
                connection.query("INSERT INTO coupons (code, description, type, percentage, discount, min_amount, min_product, start_date, expiry_date, status, created_by, created_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 1, 1, NOW())", [post.code, post.description, post.type, post.percentage, post.discount, post.min_amount, post.min_product, post.start_date, post.expiry_date], function(err, coupons) {
                    if (err) {
                        console.log(err);
                        req.flash('couponMessage', err);
                        res.render('admin/couponcreate', {
                            message: req.flash('couponMessage')
                        });
                    } else {
                        //req.flash('couponMessage', 'created Successfully');
                        res.redirect('/admin/coupon/couponindex');
                    }
                });
            }
        });
    });

    //Active inactive coupon 
    app.post('/admin/coupon/activedeactive' , function(req, res, next){
        req.getConnection(function(err , connection){
            if(err){
                throw err;
            }else{
                connection.query("UPDATE coupons SET status = '"+req.body.status+"' WHERE id = "+req.body.id+" " , function(err, data){
                    if(err){
                        throw err;
                    }
                    else{
                        res.send({
                            "status":1
                        });   
                    }
                });
            }
        });
    });

    //Delete coupon 
    app.post('/admin/coupon/coupondelete' , function(req, res, next){
        req.getConnection(function(err , connection){
            if(err){
                throw err;
            }else{
                connection.query("UPDATE coupons SET status = '2' WHERE id = "+req.body.id+" " , function(err, data){
                    if(err){
                        throw err;
                    }else{
                        res.send({
                            "status":1
                        });
                    }
                });
            }
        });
    });
}

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}