var express = require('express');
var router = express.Router();
var passport = require('passport');
var bcrypt = require('bcryptjs');
var nodemailer = require('nodemailer');
var multer = require('multer');
var randomBytes = require('random-bytes');
var upload = multer();

// Logout
router.get('/admin/logout', function (req, res) {
    req.logout();
    res.redirect('/admin/login');
});

router.get('/admin/login', function (req, res) {
    res.render('admin/login', {
        message: req.flash('loginMessage')
    });
});

// login form
router.post('/admin/login', passport.authenticate('local-login', {
    successRedirect: '/admin/dashboard', 
    failureRedirect: '/admin/login', 
    failureFlash: true 
}));

// signup form
router.get('/admin/signup', function (req, res) {
    res.render('admin/signup', {
        message: req.flash('signupMessage')
    });
});

// signup form processing
router.post('/admin/signup', passport.authenticate('local-signup', {
    successRedirect: '/admin/dashboard', 
    failureRedirect: '/admin/signup', 
    failureFlash: true 
}));

router.get('/admin/forgotpassword', function (req, res) {
    res.render('admin/forgotpassword', {
        message: req.flash('forgotPasswordMessage')
    });
});

// forgot-password form
router.post('/admin/forgotpassword1', passport.authenticate('local-forgot-password', {
    successRedirect: '/admin/login', 
    failureRedirect: '/admin/forgotpassword', 
    failureFlash: true 
}));

// forgot-password form
router.post('/admin/forgotpassword', upload.array(), function(req, res){
    var post = req.body;
    console.log("forgot password"+JSON.stringify(post));
    if(typeof (post.email) != "undefined" && post.email){
        console.log("INSIDE forgot password");
        var email = post.email.toLowerCase();
        req.getConnection(function(err, connection) {
            connection.query("SELECT * FROM user WHERE email='"+email+"' AND status = 1 AND role = 1", function(err, rows) {
                if (err) {
                    req.flash('forgotPasswordMessage', err);
                    res.render('admin/forgotpassword', {
                        status: 0,
                        message: req.flash('forgotPasswordMessage')
                    });
                } else {
                    console.log("ROW LENGTH IS ::: " + rows.length);
                    if (rows.length > 0) {
                        var transporter = nodemailer.createTransport({
                            debug: true,
                            host: smtp.SMTP_HOST,
                            secureConnection: false, // true for 465, false for other ports
                            port: smtp.SMTP_PORT,
                            tls: {cipher:'SSLv3'},
                            auth: {
                                user: smtp.SMTP_EMAIL,
                                pass: smtp.SMTP_PASSWORD,
                            }
                        });

                        randomBytes(48, function (err, buffer) {
                            var passwordResetToken = buffer.toString('hex');

                            connection.query("UPDATE user SET password_reset_token = '" + passwordResetToken + "' WHERE email = '" + email + "' ", function (err, updateResetToken) {
                                if (err) console.log(err);
                                var mailOptions = {
                                    from: 'testprojdev@gmail.com',
                                    to: email,
                                    subject: 'Reset Password',
                                    html: '<p>Hello ' + data[0].first_name + '</p><p>Click on this link to reset your password : <a href="' + req.headers.host + '/admin/resetpassword?token=' + passwordResetToken + '">Reset Password</a></p><br /> Regards,<br />Team Brewzer',
                                };
                                transporter.sendMail(mailOptions, function (error, info) {
                                    if (error) {
                                        console.log(error);
                                        req.flash('forgotPasswordMessage', "Oops! Something went wrong while sending a mail");
                                        res.render('admin/forgotpassword', {
                                            status: 0,
                                            message: req.flash('forgotPasswordMessage')
                                        });
                                        //return done(null, false, req.flash('forgotPasswordMessage', 'Oops! Something went wrong while sending a mail'));
                                    } else {
                                        console.log('Email sent: ' + info.response);
                                        req.flash('forgotPasswordMessage', "Mail has been sent to registered email-id");
                                        res.render('admin/forgotpassword', {
                                            status: 1,
                                            message: req.flash('forgotPasswordMessage')
                                        });
                                        //return done(null, true, req.flash('forgotPasswordMessage', 'Mail has been sent to registered email-id'));
                                    }
                                });
                            });
                        });
                    } else {
                        console.log("no user found");
                        //return done(null, false, req.flash('loginMessage', 'No user found.'));
                        req.flash('forgotPasswordMessage', "Invalid User");
                        res.render('admin/forgotpassword', {
                            status: 0,
                            message: req.flash('forgotPasswordMessage')
                        });
                    }
                }
            });
        });
    } else {
        console.log("OUT forgot password");
        req.flash('forgotPasswordMessage', "Field cannot be blank");
        res.render('admin/forgotpassword', {
            status: 0,
            message: req.flash('forgotPasswordMessage')
        });
    }
});

router.get('/admin/resetpassword', upload.array(), function (req, res, next) {
    if(typeof (req.query.token) != "undefined" && req.query.token){
        var resetToken = req.query.token;
        // check that token is valid or not
        req.getConnection(function (err, connection) {
            console.log("RESET TOKEN:"+resetToken);
            connection.query("SELECT COUNT(*) as cnt FROM user WHERE password_reset_token = '" + resetToken + "' AND status = 1", function (err, data) {
                if (data[0].cnt > 0) {
                    req.session.resetToken = resetToken;
                    req.flash('resetPasswordMessage', "Valid Token");
                    res.render('admin/resetpassword', {
                        status: 1,
                        message: req.flash('resetPasswordMessage')
                    });
                }
                else {
                    res.send("TOKEN MISSMATCH, INVALID REQUEST");
                }
            });
        });
    }else{
        res.send("TOKEN MISSMATCH, INVALID REQUEST");
    }
});

router.post('/admin/resetpassword', upload.array(), function (req, res, next) {
    var post = req.body;
    if(typeof (req.body.newpassword) != "undefined" && req.body.newpassword && typeof (req.body.confirmpassword) != "undefined" && req.body.confirmpassword){
        var newpassword = req.body.newpassword;
        var confirmpassword = req.body.confirmpassword;
        if(newpassword != confirmpassword){
            req.flash('resetPasswordMessage', "password and confirm-password missmatch");
            res.render('admin/resetpassword', {
                status: 0,
                message: req.flash('resetPasswordMessage')
            });
        }else{
            req.getConnection(function (err, connection) {
                if (err){
                    req.flash('resetPasswordMessage', "Connection Error");
                    res.render('admin/resetpassword', {
                        status: 0,
                        message: req.flash('resetPasswordMessage')
                    });
                }else{
                    bcrypt.genSalt(10, function(err, salt) {
                        console.log("salt for hash generated");
                        if (err) {
                            //return next(err);
                            console.log(err);
                            req.flash('resetPasswordMessage', err);
                            res.render('admin/resetpassword', {
                                status: 0,
                                message: req.flash('resetPasswordMessage')
                            });
                        } else {
                            bcrypt.hash(post.newpassword, salt, function(err, hash) {
                                if(err) throw err;
                                connection.query("UPDATE user SET password_hash = '" + hash + "', password_reset_token='' WHERE password_reset_token = '" + req.session.resetToken + "'", function (err, data) {
                                    if (err) throw err;
                                    req.logout();
                                    req.flash('resetPasswordMessage', "Succesfully Reset");
                                    res.render('admin/resetpassword', {
                                        status: 1,
                                        message: req.flash('resetPasswordMessage')
                                    });
                                });
                            });
                        }
                    });
                }
            });
        }
    }else{
        req.flash('resetPasswordMessage', "Fields cannot be blank");
        res.render('admin/resetpassword', {
            status: 0,
            message: req.flash('resetPasswordMessage')
        });
    }
});

// user is logged in check
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}

module.exports = router;
