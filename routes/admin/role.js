var express = require('express');
var router = express.Router();

router.get('/admin/addrole' , function(req , res , next){
    res.render('admin/addRole');
});
router.post('/admin/addrole' , function(req , res , next){
    req.getConnection(function(err , connection){
        if(err){
            console.log(err);
        }
        else{
            connection.query("INSERT INTO roles (role_name, role_description, status, created_by, created_date) VALUES('"+req.body.role+"' , '"+req.body.description+"' , 1, 1 , NOW())" , function(err , addRole){
                if(err){
                    console.log(err);
                    res.send({
                        "status":0
                    });
                }
                else{
                    res.send({
                        "status":1
                    });
                }
            });
        }
    });
});

module.exports = router;