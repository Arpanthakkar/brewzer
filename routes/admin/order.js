var express = require('express');
var router = express.Router();
var path = require('path');
var multer = require('multer');
var mkdirp = require('mkdirp');
var bcrypt = require('bcryptjs');
var http = require('http');
var async = require('async');

var pushfunc = require('../../config/pushnotification');
var push_function = pushfunc.notification();

var storage = multer.memoryStorage();

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function(req, file, cb) {
        var getFileExt = function(fileName) {
            var fileExt = fileName.split(".");
            if (fileExt.length === 1 || (fileExt[0] === "" && fileExt.length === 2)) {
                return "";
            }
            return fileExt.pop();
        }
        var microsecond = Math.round(new Date().getTime() / 1000 * Math.floor(Math.random() * 1000000000)); //new Date().getTime();
        cb(null, microsecond + path.extname(file.originalname)); // Date.now() + '.' + getFileExt(file.originalname))
    }
});

var multerUpload = multer({storage: storage});
var upload = multer();

module.exports = function (app, passport) {
    app.get('/admin/order/orderindex', isLoggedIn, function (req, res) {
        req.getConnection(function(err, connection) {
            if(err) throw err;
            var currentPage = 1;
            var pageCount = 0;
            var totalCount = 0;
            if(typeof (req.query.page) != "undefined" && req.query.page > 0){
                var currentPage = req.query.page;
            }
            connection.query("SELECT count(*) AS cnt FROM orders WHERE status != '2'", function(err, rowsCount) {
                if(err) throw err;
                totalCount = rowsCount[0].cnt;
                
                var items_per_page = 5;
                var offset = (currentPage - 1) * items_per_page;
                pageCount = Math.ceil(totalCount / items_per_page);

                connection.query("SELECT o.*, u.username, TIMESTAMPDIFF(YEAR, u.birthdate, CURDATE()) AS age, u.first_name, u.phone AS contactno, u.country_code, a.country_id ,a.address1, a.address2, a.state_id, a.city_id, cities.name AS city_name, countries.name AS country_name, states.name AS state_name , c.code AS order_code FROM orders AS o LEFT JOIN user AS u ON u.id = o.user_id LEFT JOIN address AS a ON o.user_id = a.user_id  INNER JOIN cities ON a.city_id = cities.id INNER JOIN states ON a.state_id = states.id INNER JOIN countries ON a.country_id = countries.id  LEFT JOIN coupons AS c ON c.id = o.coupon_id WHERE o.status != '2' AND u.status != '2' ORDER BY o.id DESC LIMIT "+offset+", "+items_per_page+" ", function(err, rows) {   
                    // console.log("row::::::",rows);
                    if (err) {
                        throw err;
                    } else {
			
                        res.render('admin/orderindex', {
                            Orders: rows,
                            currentPage: currentPage,
                            pageCount: pageCount,
                            message: req.flash('orderMessage')
                        });
                    }
                });
            });
        });
    });

    //Active inactive order 
    app.post('/admin/order/activedeactive' , function(req, res, next){
        req.getConnection(function(err , connection){
            if(err){
                throw err;
            }else{
                connection.query("UPDATE orders SET status = '"+req.body.status+"' WHERE id = "+req.body.id+" " , function(err, data){
                    if(err){
                        throw err;
                    }
                    else{
                        res.send({
                            "status":1
                        });   
                    }
                });
            }
        });
    });

    //Delete order 
    app.post('/admin/order/orderdelete' , function(req, res, next){
        req.getConnection(function(err , connection){
            if(err){
                throw err;
            }else{
                connection.query("UPDATE orders SET status = '2' WHERE id = "+req.body.id+" " , function(err, data){
                    if(err){
                        throw err;
                    }else{
                        res.send({
                            "status":1
                        });
                    }
                });
            }
        });
    });

    //Change Order status
    app.get('/admin/order/orderstatus', isLoggedIn, function (req, res, next) {
        req.getConnection(function(err, connection) {
            if(err) throw err;
            if(typeof (req.query.id) != "undefined" && req.query.id > 0){
                var order_id = req.query.id;
                var orders = [];
                var del_teams = [];
                async.parallel([
                    function(callback1){
                        connection.query("SELECT o.*, u.first_name, c.code AS order_code FROM orders AS o LEFT JOIN user AS u ON u.id = o.user_id LEFT JOIN coupons AS c ON c.id = o.coupon_id WHERE o.id = '"+order_id+"' AND u.status != '2'", function(err, rows) {
                            if (err) {
                                console.log("ERROR 1 "+err);
                                throw err;
                            } else {
                                if(rows.length > 0){
                                    orders = rows;
                                }
                                callback1();
                            }
                        });
                    },
                    function(callback2){
                        connection.query("SELECT * FROM delivery_team AS dt WHERE dt.status != '2'", function(err, rows) {
                            if (err) {
                                console.log("ERROR 2 "+err);
                                throw err;
                            } else {
                                if(rows.length > 0){
                                    del_teams = rows;
                                }
                                callback2();
                            }
                        });
                    },
                ], function(err){
                    if(err){
                        console.log("ERROR Final "+err);
                        throw err;
                    }
                    res.render('admin/orderstatus', {
                        Orders: orders,
                        Delteams: del_teams,
                        message: req.flash('orderMessage')
                    });
                });
            }else{
                res.redirect('/admin/order/orderindex');
            }
        });
    });

    //Change Order status on submit
    app.post('/admin/order/change-orderstatus', isLoggedIn, function (req, res, next) {
        var post = req.body;
        var response = {};
        req.getConnection(function(err, connection) {
            if(err) throw err;
            if(typeof (post.order_id) != "undefined" && post.order_id > 0 && typeof (post.order_status) != "undefined" && post.order_status > 0){
                var order_id = post.order_id;
                var order_status = post.order_status;
                var other_condtn = "";
                if(typeof (post.delivery_by) != "undefined" && post.delivery_by > 0){
                    other_condtn += ", delivery_by = '"+post.delivery_by+"'";
                }
                if(typeof (post.estimated_time) != "undefined" && post.estimated_time != null && post.estimated_time!=""){
                    other_condtn += ", estimated_time = '"+post.estimated_time+"'";
                }
                if(order_status == 4){
                    var today_date = new Date();
                    var del_date = today_date.formatDateTime();
                    other_condtn += ", delivery_date = '"+del_date+"'";
                }
                connection.query("UPDATE orders SET order_status = ?, modified_date = ?, modified_by='1'"+other_condtn+" WHERE id = '"+post.order_id+"'", [post.order_status, new Date().getDate()], function(err, rows) {
                    if (err) {
                        throw err;
                    } else {
                        console.log("Rows--------->", rows);
                        connection.query("SELECT u.id AS send_to, u.device_token, u.push_notification, u.status FROM user AS u LEFT JOIN orders AS o ON o.user_id = u.id WHERE o.id = '"+post.order_id+"'", function(err, userdata) {
                            if(userdata.length > 0){
                                var message = "Your Order status has been changed to "+params.ORDER_STATUS[order_status];
                                var body = {};
                                body.message = message;
                                body.type = 2;
                                body.primary_id = post.order_id;
                                body.user_id = userdata[0].send_to;
                                if(userdata[0].device_token!="" && userdata[0].device_token!=null && userdata[0].push_notification == 1 && userdata[0].status == 1){
                                    var device_token = userdata[0].device_token;
                                    // push notification code   
                                    push_function.sendNotification(device_token, message, body, req);
                                }
                                //save notification for users
                                push_function.saveNotification(body, message, req);
                            }
                            
                            response.status = 1;
                            response.message = 'Order status has been changed';
                            res.send(response);
                        });
                    }
                });
            } else {
                response.status = 0;
                response.message = 'Oops! Something went wrong';
                res.send(response);
            }
        });
    });
}

//To format date in Y-m-d 
Date.prototype.formatDateTime = function() {
    var date = new Date(this.valueOf()),
        month = '' + (date.getMonth() + 1),
        day = '' + date.getDate(),
        year = date.getFullYear(),
        hours = date.getHours(),
        minutes = date.getMinutes(),
        seconds = date.getSeconds();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    var dateformat = [year, month, day].join('-');
    var timeformat = [hours, minutes, seconds].join(':');
    return dateformat+ ' ' +timeformat ;
}

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}
