var express = require('express');
var router = express.Router();
var path = require('path');
var multer = require('multer');
var mkdirp = require('mkdirp');
var bcrypt = require('bcryptjs');
var http = require('http');

var upload = multer();

module.exports = function (app, passport) {
    app.get('/admin/deliveryprice/deliverypriceindex', isLoggedIn, function (req, res) {
        req.getConnection(function(err, connection) {
            if(err) throw err;
            var currentPage = 1;
            var pageCount = 0;
            var totalCount = 0;
            if(typeof (req.query.page) != "undefined" && req.query.page > 0){
                var currentPage = req.query.page;
            }
            connection.query("SELECT count(*) AS cnt FROM delivery_prices WHERE status != '2'", function(err, rowsCount) {
                if(err) throw err;
                totalCount = rowsCount[0].cnt;
                
                var items_per_page = 5;
                var offset = (currentPage - 1) * items_per_page;
                pageCount = Math.ceil(totalCount / items_per_page);

                connection.query("SELECT * FROM delivery_prices WHERE status != '2' ORDER BY id DESC LIMIT "+offset+", "+items_per_page+" ", function(err, rows) {
                    if (err) {
                        req.flash('deliverypriceMessage', 'Oops! Something went wrong');
                        res.render('admin/dashboard', {
                            message: req.flash('deliverypriceMessage')
                        });
                    } else {
                        res.render('admin/deliverypriceindex', {
                            deliveryPrices: rows,
                            currentPage: currentPage,
                            pageCount: pageCount,
                            message: req.flash('deliverypriceMessage')
                        });
                    }
                });
            });
        });
    });

    //deliveryprice create
    app.get('/admin/deliveryprice/deliverypricecreate', isLoggedIn, function (req, res) {
        res.render('admin/deliverypricecreate', {
            message: req.flash('deliverypriceMessage')
        });
    });

    //create form action
    app.post('/admin/deliveryprice/deliverypricecreate', isLoggedIn, upload.array(), function (req, res) {
        var post = req.body;
        req.getConnection(function(err, connection) {
            if (err) {
                req.flash('deliverypriceMessage', err);
                res.render('admin/deliverypricecreate', {
                    message: req.flash('deliverypriceMessage')
                });
            } else {
                connection.query("INSERT INTO delivery_prices (no_of_bottles, min_amount, tax, miles, status, created_by, created_date) VALUES ('"+post.no_of_bottles+"', '"+post.min_amount+"', '"+post.tax+"', '"+post.miles+"', 1, 1, NOW())", function(err, delivery_prices) {
                    if (err) {
                        console.log(err);
                        req.flash('deliverypriceMessage', err);
                        res.render('admin/deliverypricecreate', {
                            message: req.flash('deliverypriceMessage')
                        });
                    } else {
                        //req.flash('deliverypriceMessage', 'created Successfully');
                        res.redirect('/admin/deliveryprice/deliverypriceindex');
                    }
                });
            }
        });
    });

    //update navigation
    app.get('/admin/deliveryprice/deliverypriceupdate', isLoggedIn, function (req, res) {
        req.getConnection(function(err, connection) {
            if (err) {
                throw err
            }else{
                connection.query("SELECT * FROM delivery_prices WHERE id = '"+req.query.id+"' AND status = '1'", function(err, rows) {
                    res.render('admin/deliverypriceupdate', {
                        deliveryPrices: rows,
                        message: req.flash('deliverypriceMessage'),
                    });
                });
            }
        });
    });

    //update deliveryprice form post
    app.post('/admin/deliveryprice/deliverypriceupdate', isLoggedIn, upload.array(), function(req, res, next) {
        var post = req.body;
        req.getConnection(function(err, connection) {
            if (err) {
                throw err
            } else {
                var t_date = new Date();
                var set_condition = "modified_by= '1', modified_date= '" + t_date + "'";

                //connection.query("UPDATE delivery_prices SET " + set_condition + ", no_of_bottles = ?, min_amount = ?, tax = ?, miles = ? WHERE id = '" + post.id + "'", [post.no_of_bottles, post.min_amount, post.tax, post.miles], function(err, data) {
		connection.query("UPDATE delivery_prices SET " + set_condition + ", no_of_bottles = '"+post.no_of_bottles+"', min_amount = '"+post.min_amount+"', tax = '"+post.tax+"', miles = '"+post.miles+"' WHERE id = '" + post.id + "'", function(err, data) {
                    if (err) {
                        throw err;
                    } else {
                        res.redirect('/admin/deliveryprice/deliverypriceindex');
                    }
                });
            }
        });
    });

    //Active inactive deliveryprice 
    app.post('/admin/deliveryprice/activedeactive' , function(req, res, next){
        req.getConnection(function(err , connection){
            if(err){
                throw err;
            }else{
                connection.query("UPDATE delivery_prices SET status = '"+req.body.status+"' WHERE id = "+req.body.id+" " , function(err, data){
                    if(err){
                        throw err;
                    }else{
                        res.send({
                            "status":1
                        });   
                    }
                });
            }
        });
    });

    //Delete deliveryprice 
    app.post('/admin/deliveryprice/deliverypricedelete' , function(req, res, next){
        req.getConnection(function(err , connection){
            if(err){
                throw err;
            }else{
                connection.query("UPDATE delivery_prices SET status = '2' WHERE id = "+req.body.id+" " , function(err, data){
                    if(err){
                        throw err;
                    }else{
                        res.send({
                            "status":1
                        });
                    }
                });
            }
        });
    });
}

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}
