var express = require('express');
var router = express.Router();
var hash = require('../../pass').hash;
var path = require('path');
var multer = require('multer');
var mkdirp = require('mkdirp');
var bcrypt = require('bcryptjs');
var passport = require('passport');
var async = require("async");
var stripe = require('stripe');

var request = require('request');

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function(req, file, cb) {
        var getFileExt = function(fileName) {
            var fileExt = fileName.split(".");
            if (fileExt.length === 1 || (fileExt[0] === "" && fileExt.length === 2)) {
                return "";
            }
            return fileExt.pop();
        }
        var microsecond = Math.round(new Date().getTime() / 1000 * Math.floor(Math.random() * 1000000000)); //new Date().getTime();
        cb(null, microsecond + path.extname(file.originalname)); // Date.now() + '.' + getFileExt(file.originalname))
    }
});
var multerUpload = multer({storage: storage});



var response = {};
var upload = multer();

//Get Order details of user
router.post('/api/master/get-filter-options', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    var data = {};
    if(typeof(post.category_id) != "undefined" && post.category_id > 0){
        req.getConnection(function(err, connection) {
            if (err)
                throw err;
            async.parallel([
                function(callback1){
                    var brands = [];
                    connection.query("SELECT b.* FROM brands AS b WHERE b.status = '1' AND b.category_id='"+post.category_id+"'", function(err, rows1){
                        if(err){
                            response.status = 0;
                            response.message = err;
                            res.send(response);
                        } else {
                            if(rows1.length > 0){
                                async.forEachOf(rows1, function(value, key, callback11){
                                    var result1 = {};
                                    var flavours = (typeof(value.flavours_id) != "undefined" && value.flavours_id != null && value.flavours_id != "") ? value.flavours_id : "";
                                    result1.brand_id = parseInt(value.id);
                                    result1.brand_name = value.name;
                                    if(flavours != ""){
                                        connection.query("SELECT id AS flavour_id, name AS flavour_name FROM flavours WHERE id IN ("+flavours+") AND status = '1'", function(err, rows2){
                                            if(err){
                                                response.status = 0;
                                                response.message = err;
                                                res.send(response);
                                            } else {
                                                result1.flavours = rows2;
                                            }
                                            brands.push(result1);
                                            callback11();
                                        })
                                    } else {
                                        result1.flavours = [];
                                        brands.push(result1);
                                        callback11();
                                    }
                                }, function(err){
                                    if(err){
                                        response.status = 0;
                                        response.message = err;
                                        res.send(response);
                                    } else {
                                        data.brands = brands;
                                        callback1();
                                    }
                                });
                            } else {
                                data.brands = brands;
                                callback1();
                            }
                        }
                    });
                },
                function(callback2){
                    connection.query("SELECT MAX(price) AS max_price, MIN(price) AS min_price FROM `product` WHERE category_id = '"+post.category_id+"' AND status = '1'", function(err, rows3){
                        if(err){
                            response.status = 0;
                            response.message = err;
                            res.send(response);
                        } else {
                            data.max_price = (rows3.length > 0 && rows3[0].max_price != null) ? Math.ceil(rows3[0].max_price) : 0;
                            data.min_price = (rows3.length > 0 && rows3[0].min_price != null) ? Math.floor(rows3[0].min_price) : 0;
                        }
                        callback2();
                    });
                },
            ], function(err){
                if(err){
                    response.status = 0;
                    response.message = err;
                    res.send(response);
                }else{
                    response.status = 1;
                    response.message = "Success";
                    response.data = data;
                    res.send(response);
                }
            })
        });
    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
});

//Get flavours from brand
router.post('/api/master/getflavours', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    req.getConnection(function(err, connection) {
        if (err)
            throw err;
        connection.query("SELECT * FROM brands WHERE id='"+post.brand_id+"'" , function(err, data){
            if(err){
                throw err;
            }else{
                var dropdown = "<option value=''>Select Flavors</option>";
                if(data.length > 0){

                    var flavours = (data[0].flavours_id != null) ? data[0].flavours_id : "";
		    var whr = (flavours != "") ? " status = '1' OR id IN ("+flavours+")" : " status = '1'";
                    connection.query("SELECT id AS flavour_id, name AS flavour_name FROM flavours WHERE"+whr, function(err, rows2){
                        if(err){
                            response.status = 0;
                            response.message = err;
                            res.send(response);
                        } else {
                            if(rows2.length > 0){
				var flvr_arr = (flavours != "") ? flavours.split(",") : [];
                                async.forEachOf(rows2, function(value, key, callback11){
                                    var sel = (flvr_arr.includes((value.flavour_id).toString())) ? "selected" : "";
                                    dropdown += "<option value='"+value.flavour_id+"' "+sel+">"+value.flavour_name+"</option>";
                                    callback11();
                                }, function(err){
                                    if(err){
                                        response.status = 0;
                                        response.message = err;
                                        res.send(response);
                                    } else {
                                        response.status = 1;
                                        response.message = "Success";
                                        response.data = dropdown;
                                        res.send(response);
                                    }
                                });
                            }else{
                                response.status = 1;
                                response.message = "Success";
                                response.data = dropdown;
                                res.send(response);
                            }
                        }
                    })
                } else {
                    response.status = 1;
                    response.message = "Success";
                    response.data = dropdown;
                    res.send(response);
                }
            }
        });
    });
});

//Get flavours from brand
router.post('/api/master/getflavoursmaster', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    req.getConnection(function(err, connection) {
        if (err)
            throw err;
        var dropdown = "<option value=''>Select Flavors</option>";
        connection.query("SELECT id AS flavour_id, name AS flavour_name FROM flavours WHERE status = '1'", function(err, rows2){
            if(err){
                response.status = 0;
                response.message = err;
                res.send(response);
            } else {
                if(rows2.length > 0){
                    async.forEachOf(rows2, function(value, key, callback11){
                        dropdown += "<option value='"+value.flavour_id+"'>"+value.flavour_name+"</option>";
                        callback11();
                    }, function(err){
                        if(err){
                            response.status = 0;
                            response.message = err;
                            res.send(response);
                        } else {
                            response.status = 1;
                            response.message = "Success";
                            response.data = dropdown;
                            res.send(response);
                        }
                    });
                }else{
                    response.status = 1;
                    response.message = "Success";
                    response.data = dropdown;
                    res.send(response);
                }
            }
        })
    });
});


//Get flavours from brand
router.post('/api/master/getselectedflavours', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    req.getConnection(function(err, connection) {
        if (err)
            throw err;
        connection.query("SELECT flavours_id FROM product WHERE id = '"+post.product_id+"'" , function(err, data){
            if(err){
                throw err;
            }else{
                var dropdown = "<option value=''>Select Flavors</option>";
                if(data.length > 0){
                    var flavours = (data[0].flavours_id != null) ? data[0].flavours_id : "";
		    var whr = (flavours != "") ? " status = '1' OR id IN ("+flavours+")" : " status = '1'";
                    connection.query("SELECT id AS flavour_id, name AS flavour_name FROM flavours WHERE"+whr, function(err, rows2){
                        if(err){
                            response.status = 0;
                            response.message = err;
                            res.send(response);
                        } else {
                            if(rows2.length > 0){
                                var flvr_arr = (flavours != "") ? flavours.split(",") : [];
                                async.forEachOf(rows2, function(value, key, callback11){
                                    var sel = (flvr_arr.includes((value.flavour_id).toString())) ? "selected" : "";
                                    dropdown += "<option value='"+value.flavour_id+"' "+sel+">"+value.flavour_name+"</option>";
                                    callback11();
                                }, function(err){
                                    if(err){
                                        response.status = 0;
                                        response.message = err;
                                        res.send(response);
                                    } else {
                                        response.status = 1;
                                        response.message = "Success";
                                        response.data = dropdown;
                                        res.send(response);
                                    }
                                });
                            }else{
                                response.status = 1;
                                response.message = "Success";
                                response.data = dropdown;
                                res.send(response);
                            }
                        }
                    })
                } else {
                    response.status = 1;
                    response.message = "Success";
                    response.data = dropdown;
                    res.send(response);
                }
            }
        });
    });
});


//Get flavours from brand
router.post('/api/master/getbrandselectedflavours', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    req.getConnection(function(err, connection) {
        if (err)
            throw err;
        connection.query("SELECT * FROM brands WHERE id='"+post.brand_id+"'" , function(err, data){
            if(err){
                throw err;
            }else{
                var dropdown = "<option value=''>Select Flavors</option>";
                if(data.length > 0){

                    var flavours = (data[0].flavours_id != null) ? data[0].flavours_id : "";
		    var whr = (flavours != "") ? " status = '1' OR id IN ("+flavours+")" : " status = '1'";
                    connection.query("SELECT id AS flavour_id, name AS flavour_name FROM flavours WHERE"+whr, function(err, rows2){
                        if(err){
                            response.status = 0;
                            response.message = err;
                            res.send(response);
                        } else {
                            if(rows2.length > 0){
                                var flvr_arr = (flavours != "") ? flavours.split(",") : [];
                                async.forEachOf(rows2, function(value, key, callback11){
                                    var sel = (flvr_arr.includes((value.flavour_id).toString())) ? "selected" : "";
                                    dropdown += "<option value='"+value.flavour_id+"' "+sel+">"+value.flavour_name+"</option>";
                                    callback11();
                                }, function(err){
                                    if(err){
                                        response.status = 0;
                                        response.message = err;
                                        res.send(response);
                                    } else {
                                        response.status = 1;
                                        response.message = "Success";
                                        response.data = dropdown;
                                        res.send(response);
                                    }
                                });
                            }else{
                                response.status = 1;
                                response.message = "Success";
                                response.data = dropdown;
                                res.send(response);
                            }
                        }
                    })
                } else {
                    response.status = 1;
                    response.message = "Success";
                    response.data = dropdown;
                    res.send(response);
                }
            }
        });
    });
});


//Get sub categories from category
router.post('/api/master/getselectedsubcat', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    req.getConnection(function(err, connection) {
        if (err)
            throw err;
        var dropdown = "<option value=''>Select Sub Categories</option>";
        
        var category_id = post.category_id;
        var whr = " status = '1' AND category_id = '"+category_id+"'";
        connection.query("SELECT id AS subcategory_id, name AS subcategory_name FROM sub_categories WHERE"+whr, function(err, rows2){
            if(err){
                response.status = 0;
                response.message = err;
                res.send(response);
            } else {
                if(rows2.length > 0){
                    async.forEachOf(rows2, function(value, key, callback11){
                        var sel = (post.subcateogry_id == value.subcategory_id) ? "selected" : "";
                        dropdown += "<option value='"+value.subcategory_id+"' "+sel+">"+value.subcategory_name+"</option>";
                        callback11();
                    }, function(err){
                        if(err){
                            response.status = 0;
                            response.message = err;
                            res.send(response);
                        } else {
                            response.status = 1;
                            response.message = "Success";
                            response.data = dropdown;
                            res.send(response);
                        }
                    });
                }else{
                    response.status = 1;
                    response.message = "Success";
                    response.data = dropdown;
                    res.send(response);
                }
            }
        })
    });
});


//Get brands from category
router.post('/api/master/getselectedbrands', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    req.getConnection(function(err, connection) {
        if (err)
            throw err;
        var dropdown = "<option value=''>Select Brands</option>";
        
        var category_id = post.category_id;
        var whr = " status = '1' AND category_id = '"+category_id+"'";
        connection.query("SELECT id AS brand_id, name AS brand_name FROM brands WHERE"+whr, function(err, rows2){
            if(err){
                response.status = 0;
                response.message = err;
                res.send(response);
            } else {
                if(rows2.length > 0){
                    async.forEachOf(rows2, function(value, key, callback11){
                        var sel = (post.brand_id == value.brand_id) ? "selected" : "";
                        dropdown += "<option value='"+value.brand_id+"' "+sel+">"+value.brand_name+"</option>";
                        callback11();
                    }, function(err){
                        if(err){
                            response.status = 0;
                            response.message = err;
                            res.send(response);
                        } else {
                            response.status = 1;
                            response.message = "Success";
                            response.data = dropdown;
                            res.send(response);
                        }
                    });
                }else{
                    response.status = 1;
                    response.message = "Success";
                    response.data = dropdown;
                    res.send(response);
                }
            }
        })
    });
});




router.post('/vendorSignup', multerUpload.single('profile_photo'), (req, res, next) => {
    console.log("SANYONI:::::::::::::");
    var post = req.body;
    if (typeof(post.email) != 'undefined' && post.email && typeof(post.password) != 'undefined' && post.password) {
        bcrypt.genSalt(10, function(err, salt) {
            if (err) res.status(400).send({ message : err });
            else {
                bcrypt.hash(req.body.password, salt, function(err, hash) {
                    if (err) res.status(400).send({ message : err});
                    else {
                        var password_hash = hash;
                        req.getConnection(function(err, connection) {
                            if (err) res.status(500).send({ message : 'Database connection error'});
                            else {
                                var profile_photo = "", name = "";
                                if (typeof(req.file) != 'undefined' && req.file.filename) profile_photo = req.file.filename;
                                if (post.name != '' && typeof(post.name) != 'undefined') name = post.name;
                                
                                connection.query("SELECT  COUNT(*) as cnt FROM vendor WHERE email = '" + post.email + "'", function(err, checkemail) {
                                    if (checkemail[0].cnt == 0) {
                                        var phone_international = post.country_code ? (post.country_code + post.phone) : post.phone;
                                        connection.query("INSERT INTO vendor (vendor_name , email , phone, phone_international, profile_photo ,created_date , password_hash) VALUES ('" + name + "', '" + post.email + "', '" + post.phone + "', '" + phone_international + "', '" + profile_photo + "' ,' NOW() ', '" + password_hash + "')", function(err, signup) {
                                            if (err) res.status(500).send({ message : 'Oops! Something went wrong while inserting data'});
                                            //else res.status(200).send({ message : 'Signup successfully' });
                                            else res.redirect('/vendor/login');
                                        });
                                    }
                                    else res.status(400).send({ message : 'Email already exist'});
                                });
                            }
                        });
                    }
                });
            }
        });
    }
    else res.status(0).send({ message :'Wrong or missing parameters'});   
});

router.post('/vendorLogin', function(req, res, next) {
    var post = req.body;
    response = {};
    if (typeof(post.email) != 'undefined' && post.email && typeof(post.password) != 'undefined' && post.password) {
        req.getConnection(function(err, connection) {
            connection.query("SELECT * FROM vendor WHERE (email ='" + post.email + "')", function(err, rows) {
                if (err) res.status(500).send({ message :'Oops! Something went wrong while fetching data'});
                else {
                    if (rows.length > 0) {
                        bcrypt.compare(post.password, rows[0].password_hash, function(err, result) {
                            if (err) res.status(500).send({ message : 'Oops! Something went wrong'});
                            else if (result === true) {
                                const vendorId = rows[0].id;
                                console.log("SZXDCFVGBHNJ>>>>>>>>>>>>",vendorId);
                                res.render('vendor/dashboard', {vendorId :vendorId});
                            }
                            else res.status(400).send({ message :'Invalid phone or Password'});
                        });
                    } else res.status(400).send({ message :'User does not exist'});
                }
            });
        });
    } 
    else res.status(400).send({ message :'Wrong or missing parameters'});
});

// Step 1 : Create OAuth link :
//      https://connect.stripe.com/oauth/authorize?
//          response_type=code
//          &client_id=ca_DdfxBO8mO41KCC65RET0rLroImSnWMUf
//          &scope=read_write
//          &state={Vendor_id}
//          &redirect_uri=http://132.248.152.200:3030/stripeOnboarding/

// Step 2 : Create the button - Connect  with stripe
//      Attach the above OAuth link to this button
//
// Step 3 : User fills the form and clicks "Connect my stripe Account"
//      User is now redirected back to our site

// Step 4 : In our redirect API, we will fetch vendor account id from it and save to database

router.get('/stripeOnboarding', function (req, res, next) {
    console.log("  I am  here", req.body, req.query, req.headers);
    var options = {
        method: 'POST',
        url: 'https://connect.stripe.com/oauth/token',
        headers: { 'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' },
        formData:
        {
           // client_secret: 'sk_test_Gd7ZZnWWjQ4fL5iF3m4eEuXP',
            client_secret:'sk_live_hBxwAVs3chlsORWEv2nGqenn004P2lr3il',
            code: req.query.code,
            grant_type: 'authorization_code'
        }
    };

    request(options, function (err, response, body) {
        const body2 = JSON.parse(body);
        if(err) res.status(500).send({ message :'Oops! Something went wrong while connecting to Stripe'});
        else {
            req.getConnection(function(err, connection) {
                console.log("body.....",  (err) ? err : "no error", body2.stripe_user_id)
                if (err) res.status(500).send({ message :'Database connection error'});
                else {
                    connection.query("UPDATE vendor SET account_id = '" + body2.stripe_user_id +"' WHERE id = '" + req.query.state + "'", function(err, del){
                        console.log("body.....",  (err) ? err : "no error", body2.stripe_user_id)
                        if (err) res.status(500).send({ message :'Oops! Something went wrong while inserting data'});
                        else res.status(200).send({ message :'Vendor Onboarded Successfully'});
                    });
                }
            });
        }
    });
});


router.get('/vendor/login', function (req, res) {
    res.render('vendor/login', {
        message: req.flash('loginMessage')
    });
});

// login form
// router.post('/vendor/login', passport.authenticate('local-login', {
//     successRedirect: '/admin/dashboard', 
//     failureRedirect: '/admin/login', 
//     failureFlash: true 
// }));

// signup form
router.get('/vendor/signup', function (req, res) {
    res.render('vendor/signup', {
        message: req.flash('signupMessage')
    });
});

// signup form processing
// router.post('/vendor/signup', passport.authenticate('local-signup', {
//     successRedirect: '/admin/dashboard', 
//     failureRedirect: '/admin/signup', 
//     failureFlash: true 
// }));


module.exports = router;

// route middleware to ensure user is valid
function isUserActive(req, res, next) {
    var post = req.body;
    var userid = 0;
    var response = {};
    if (typeof (post.user_id) != "undefined" && post.user_id > 0) {
        userid = post.user_id;
    }
    if (userid > 0) {
        req.getConnection(function (err, connection) {
            if (err) {
                console.log(err);
                throw err;
            } else {
                var sql = "SELECT status FROM user WHERE id = '" + userid + "'";
                connection.query(sql, function (err, rows) {
                    if (err) {
                        console.log(err);
                        throw err;
                    } else {
                        if (rows.length > 0) {
                            if (rows[0].status == 0) {
                                response.status = -1;
                                response.message = "Sorry! Your account is in-active temporarily";
                                res.send(response);
                            } else if (rows[0].status == 2) {
                                response.status = -1;
                                response.message = "Sorry! Your account is deactive permanently";
                                res.send(response);
                            } else {
                                return next();
                            }
                        } else {
                            response.status = -1;
                            response.message = "Sorry! Invalid user";
                            res.send(response);
                        }
                    }
                });
            }
        });
    } else {
        return next();
    }
}
