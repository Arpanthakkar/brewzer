var express = require('express');
var router = express.Router();
var hash = require('../../pass').hash;
var path = require('path');
var multer = require('multer');
var mkdirp = require('mkdirp');
var bcrypt = require('bcryptjs');
var passport = require('passport');
var async = require("async");
var params = require('../../config/params.js');

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function(req, file, cb) {
        var getFileExt = function(fileName) {
            var fileExt = fileName.split(".");
            if (fileExt.length === 1 || (fileExt[0] === "" && fileExt.length === 2)) {
                return "";
            }
            return fileExt.pop();
        }
        var microsecond = Math.round(new Date().getTime() / 1000 * Math.floor(Math.random() * 1000000000)); //new Date().getTime();
        cb(null, microsecond + path.extname(file.originalname)); // Date.now() + '.' + getFileExt(file.originalname))
    }
});
var multerUpload = multer({storage: storage});

var response = {};
var upload = multer();

//add OR update product to cart
router.post('/api/cart/add-product', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    if(typeof(post.product_id) != "undefined" && post.product_id > 0 
        && typeof(post.user_id) != "undefined" && post.user_id > 0
        && typeof(post.quantity) != "undefined" && post.quantity
        && typeof(post.price) != "undefined" && post.price
        && typeof(post.zipcode) != "undefined" && post.zipcode){

        req.getConnection(function(err, connection) {
            if (err)
                throw err;

            var cur_zipcode = post.zipcode;
            var is_old_prod = 0;
            var remove_prod = 0;
            var prod_deleted = 0;
            var reached_max_qty = 0;
            async.series([
                function(cb){
                    if(typeof(post.delete_product)!="undefined" && post.delete_product > 0){
                        connection.query("UPDATE cart SET status = '2' WHERE user_id = '"+post.user_id+"' AND status = '1' AND product_id NOT IN (SELECT id FROM product WHERE FIND_IN_SET('"+cur_zipcode+"', zipcode))", function(err, del){
                            prod_deleted = 1;
                            cb();
                        });
                    }else{
                        cb();
                    }
                },
                function(cb1){
                    connection.query("SELECT * FROM cart WHERE user_id = '"+post.user_id+"' AND status = '1' AND product_id NOT IN (SELECT id FROM product WHERE FIND_IN_SET('"+cur_zipcode+"', zipcode))", function(err, oldProd){ 
			if(oldProd.length > 0){
                            remove_prod = 1;
                        }
                        cb1();
                    })
                },
                function(cb2){
                    if(remove_prod == 0){
                        connection.query("SELECT * FROM cart WHERE user_id = '"+post.user_id+"' AND product_id = '"+post.product_id+"' AND status = '1'", function(err, cartRow) {
                            if(cartRow.length > 0){
                                is_old_prod = 1;
                            }
                            cb2();
                        });
                    }else{
                        cb2();
                    }
                },
                function(cbx){
                    if(remove_prod == 0){
                        if(is_old_prod == 1){
                            var qty_sql = "SELECT SUM(quantity) AS total_quantities FROM cart WHERE user_id = '"+post.user_id+"' AND product_id != '"+post.product_id+"' AND status = '1'";
                        }else{
                            var qty_sql = "SELECT SUM(quantity) AS total_quantities FROM cart WHERE user_id = '"+post.user_id+"' AND status = '1'";
                        }
                        connection.query(qty_sql, function(err, qtyRow) {
                            if(qtyRow.length > 0){
                                var total_quantity = (typeof(qtyRow[0].total_quantities)!="undefined" && qtyRow[0].total_quantities!=null && qtyRow[0].total_quantities > 0) ? parseInt(qtyRow[0].total_quantities) : 0;
                                var updated_quantity = Number(total_quantity) + Number(post.quantity);
                                if(parseInt(updated_quantity) > params.MAX_QTY){
                                    reached_max_qty = 1;
                                }
                            }
                            cbx();
                        });
                    }else{
                        cbx();
                    }
                },
                function(cb3){
                    if(remove_prod == 0 && reached_max_qty == 0){
                        if(is_old_prod == 1){
                            var sql = "UPDATE cart SET quantity='"+post.quantity+"', price='"+post.price+"', modified_by='"+post.user_id+"', modified_date=NOW() WHERE user_id = '"+post.user_id+"' AND product_id = '"+post.product_id+"'";
                        }else{
                            var sql = "INSERT INTO cart (user_id, product_id, quantity, price, status, created_by, created_date) VALUES ('"+post.user_id+"', '"+post.product_id+"', '"+post.quantity+"', '"+post.price+"', '1', '"+post.user_id+"', NOW())";
                        }
                        //console.log(sql);
                        connection.query(sql, function(err, rows){
                            cb3();
                        });
                    }else{
                        cb3();
                    }
                }
            ], function(err){
                if (err) {
                    console.log(err);
                    response.status = 0;
                    response.message = 'Something went wrong';
                    res.send(response);
                } else {
                    if(remove_prod == 1){
                        response.status = 2;
                        response.message = 'Remove other products from cart which are added from different location';
                        res.send(response);
                    }else if(reached_max_qty == 1){
                        response.status = 0;
                        response.message = 'You can add maximum '+params.MAX_QTY+' quantity of all products in the cart';
                        res.send(response);
                    }else{
                        response.status = 1;
                        response.message = 'Successfully added';
                        res.send(response);
                    }
                }
            });
        });
    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
});

//remove product from cart
router.post('/api/cart/remove-product', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    if(typeof(post.product_id) != "undefined" && post.product_id > 0 
        && typeof(post.user_id) != "undefined" && post.user_id > 0){

        req.getConnection(function(err, connection) {
            if (err)
                throw err;
            var sql = "UPDATE cart SET status='2', modified_by='"+post.user_id+"', modified_date=NOW() WHERE user_id = '"+post.user_id+"' AND product_id = '"+post.product_id+"' AND status='1'";
            connection.query(sql, function(err, rows){
                if (err) {
                    console.log(err);
                    response.status = 0;
                    response.message = 'Something went wrong';
                    res.send(response);
                } else {
                    response.status = 1;
                    response.message = 'Successfully removed';
                    res.send(response);
                }
            });
        });
    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
});

//cart product list
router.post('/api/cart/product-list', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    if(typeof(post.user_id) != "undefined" && post.user_id > 0){
        req.getConnection(function(err, connection) {
            if (err)
                throw err;
            var sql = "SELECT cart.*, p.name, p.image, c.id AS category_id, c.name AS category_name, s.id AS subcategory_id, s.name AS subcategory_name FROM cart LEFT JOIN `product` as p ON p.id = cart.product_id LEFT JOIN sub_categories AS s ON s.id = p.subcategory_id LEFT JOIN categories AS c ON c.id = s.category_id WHERE p.status ='1' AND cart.user_id = '"+post.user_id+"' AND cart.status='1'";
            connection.query(sql, function(err, rows){
                if (err) {
                    console.log(err);
                    response.status = 0;
                    response.message = 'Something went wrong';
                    res.send(response);
                } else {
                    var data = [];
                    rows.forEach(function(cartData){
                        console.log("cartData:"+JSON.stringify(cartData));
                        var result = {};
                        result.cart_id = cartData.id;
                        result.product_id = cartData.product_id;
                        result.quantity = cartData.quantity;
                        result.price = cartData.price;
                        result.product_name = cartData.name;
                        result.category_name = cartData.category_name;
                        result.subcategory_name = cartData.subcategory_name;
                        result.image = (cartData.image != "") ? 'http://' + req.headers.host + '/product/' + cartData.image : ""
                        data.push(result);
                        console.log("data-inside:"+JSON.stringify(data));
                    });
                    console.log("data-outside:"+JSON.stringify(data));
                    response.status = 1;
                    response.message = 'Success';
                    response.data = data;
                    res.send(response);
                }
            });
        });
    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
});

module.exports = router;

// route middleware to ensure user is valid
function isUserActive(req, res, next) {
    var post = req.body;
    var userid = 0;
    var response = {};
    if (typeof (post.user_id) != "undefined" && post.user_id > 0) {
        userid = post.user_id;
    }
    if (userid > 0) {
        req.getConnection(function (err, connection) {
            if (err) {
                console.log(err);
                throw err;
            } else {
                var sql = "SELECT status FROM user WHERE id = '" + userid + "'";
                connection.query(sql, function (err, rows) {
                    if (err) {
                        console.log(err);
                        throw err;
                    } else {
                        if (rows.length > 0) {
                            if (rows[0].status == 0) {
                                response.status = -1;
                                response.message = "Sorry! Your account is in-active temporarily";
                                res.send(response);
                            } else if (rows[0].status == 2) {
                                response.status = -1;
                                response.message = "Sorry! Your account is deactive permanently";
                                res.send(response);
                            } else {
                                return next();
                            }
                        } else {
                            response.status = -1;
                            response.message = "Sorry! Invalid user";
                            res.send(response);
                        }
                    }
                });
            }
        });
    } else {
        return next();
    }
}
