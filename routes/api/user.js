var express = require('express');
var router = express.Router();
var hash = require('../../pass').hash;
var path = require('path');
var multer = require('multer');
var mkdirp = require('mkdirp');
var bcrypt = require('bcryptjs');
var passport = require('passport');
var nodemailer = require('nodemailer');
var rest = require('restler');
var parser = require('xml2json');
var async = require("async");
const client = require('twilio')(twilio.accountSid, twilio.authToken);
var otpGenerator = require('otp-generator')

var func  = require('../../config/functions');
var functions = func.func();

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads/user/')
    },
    filename: function(req, file, cb) {
        var getFileExt = function(fileName) {
            var fileExt = fileName.split(".");
            if (fileExt.length === 1 || (fileExt[0] === "" && fileExt.length === 2)) {
                return "";
            }
            return fileExt.pop();
        }
        var microsecond = Math.round(new Date().getTime() / 1000 * Math.floor(Math.random() * 1000000000)); //new Date().getTime();
        cb(null, microsecond + path.extname(file.originalname)); // Date.now() + '.' + getFileExt(file.originalname))
    }
});
var multerUpload = multer({storage: storage});

var response = {};
var upload = multer();

router.post('/api/user/user-info', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    console.log(JSON.stringify(post));
    if (typeof(post.user_id) != 'undefined' && post.user_id) {
        console.log(post.user_id);
        req.getConnection(function(err, connection) {
            if (err) {
                response.status = 0;
                response.message = 'Oops! Something went wrong';
                res.send(response);
            } else {
                connection.query("SELECT u.*, c.name AS country_name, s.name AS state_name, ci.name AS city_name FROM user AS u LEFT JOIN countries AS c ON c.id = u.country_id LEFT JOIN states AS s ON s.id = u.state_id LEFT JOIN cities AS ci ON ci.id = u.city_id WHERE u.id = " + post.user_id + " AND u.status ='1'", function(err, rows) {
                    console.log(JSON.stringify(rows));
                    if (err) {
                        response.status = 0;
                        response.message = 'Oops! Something went wrong';
                        res.send(response);
                    } else {
                        getUserData(req, rows, function(result) {
                            response.status = 1;
                            response.message = 'Success';
                            response.data = result;
                            res.send(response);
                        });
                    }
                })
            }
        })
    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
})

router.post('/api/user/login', upload.array(), function(req, res, next) {
    var post = req.body;
    response = {};
    if (typeof(post.phone) != 'undefined' && post.phone && typeof(post.password) != 'undefined' && post.password) {
        var phone = post.phone;
        var password = post.password;
        var device_type = "";
        var device_token = "";

        console.log(phone);
        console.log(password);

        req.getConnection(function(err, connection) {
            connection.query("SELECT u.*, c.name AS country_name, s.name AS state_name, ci.name AS city_name FROM user AS u LEFT JOIN countries AS c ON c.id = u.country_id LEFT JOIN states AS s ON s.id = u.state_id LEFT JOIN cities AS ci ON ci.id = u.city_id WHERE (u.phone ='" + phone + "' OR u.phone_international ='" + phone + "' OR u.phone_national ='" + phone + "' OR u.phone_e164 ='" + phone + "') AND u.status = 1", function(err, rows) {
                if (err) {
                    console.log("Error %s", err);
                    response.status = 0;
                    response.message = 'Oops! Something went wrong while fetching data';
                    res.send(response);
                } else {
                    console.log("ROW LENGTH IS ::: " + rows.length);
                    if (rows.length > 0) {
                        connection.query("SELECT * FROM user WHERE phone = '" + post.phone + "' ", function(err, rows) {
                            console.log("rows[0].otp_confirmation", rows[0])
                            if(rows[0].otp_confirmation == 'true'){

                                var password_hash = rows[0].password_hash;
                                var user_id = rows[0].id;

                                // Load hash from your password DB.
                                bcrypt.compare(password, password_hash, function(err, result) {
                                    if (err) {
                                        console.log(err);
                                        response.status = 0;
                                        response.message = 'Oops! Something went wrong';
                                        res.send(response);
                                    } else if (result === true) {
                                        console.log("password_hash match");
                                        if (typeof(req.body.device_type) != 'undefined' && typeof(req.body.device_token) != 'undefined') {
                                            connection.query("UPDATE user SET device_type = '" + req.body.device_type + "' , device_token = '" + req.body.device_token + "' WHERE id = '" + user_id + "'", function(err, data) {
                                                if (err) {
                                                    console.log(err);
                                                    response.status = 0;
                                                    response.message = 'Oops! Something went wrong';
                                                    res.send(response);
                                                }
                                            });
                                        }
                                        getUserData(req, rows, function(result) {
                                            result.device_type = req.body.device_type;
                                            result.device_token = req.body.device_token;

                                            response.status = 1;
                                            response.message = 'Success';
                                            response.data = result;
                                            res.send(response);
                                        });
                                    } else {
                                        console.log("Error %s", err);
                                        response.status = 0;
                                        response.message = 'Invalid phone or Password';
                                        res.send(response);
                                    }
                                    
                                })
                            }    
                            else{
                                response.status = 0;
                                response.message = 'Please confirm otp before logging in.';
                                res.send(response);
                            }
                        });
                    } else {
                        response.status = 0;
                        response.message = 'Invalid User';
                        res.send(response);
                    }
                }
            });
        // }
        // else{
        //     response.status = 0;
        //     response.message = 'Please confirm otp before logging in.';
        //     res.send(response);
        // }    
    // })        
        });
    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
});

router.post('/api/user/signup', multerUpload.single('profile_photo'), function(req, res, next) {
    console.log("In Sign Up ");
    response = {};
    var post = req.body;
    if (typeof(post.email) != 'undefined' && post.email && typeof(post.password) != 'undefined' && post.password && typeof(post.country_code) != 'undefined' && post.country_code && typeof(post.phone) != 'undefined' && post.phone && typeof(post.photo_id) != 'undefined' && post.photo_id) {
        bcrypt.genSalt(10, function(err, salt) {
            console.log("salt for hash generated");
            if (err) {
                //return next(err);
                console.log(err);
                response.status = 0;
                response.message = err;
                res.send(response);
            } else {
                bcrypt.hash(req.body.password, salt, function(err, hash) {
                    console.log("hash generated");
                    if (err) {
                        //return next(err);
                        console.log(err);
                        response.status = 0;
                        response.message = err;
                        res.send(response);
                    } else {
                        var password_hash = hash;
                        req.getConnection(function(err, connection) {
                            if (err) {
                                console.log(err);
                                response.status = 0;
                                response.message = 'Datbase connection error';
                                res.send(response);
                            } else {
                                var device_type = "";
                                var device_token = "";
                                var profile_photo = "";
                                var first_name = ""
                                var otp = otpGenerator.generate(4, { upperCase: false, specialChars: false, alphabets: false });
        
                                if (typeof(req.file) != 'undefined' && req.file.filename) {
                                    profile_photo = req.file.filename;
                                }
                                if (post.device_type != '' && typeof(post.device_type) != 'undefined') {
                                    device_type = post.device_type;
                                }
                                if (post.device_token != '' && typeof(post.device_token) != 'undefined') {
                                    device_token = post.device_token;
                                }
                                if (post.first_name != '' && typeof(post.first_name) != 'undefined') {
                                    first_name = post.first_name;
                                }

                                connection.query("SELECT  COUNT(*) as cnt FROM user WHERE email = '" + post.email + "' AND status!='2'", async function(err, checkemail) {
                                    connection.query("SELECT  COUNT(*) as cnt FROM user WHERE phone = '" + post.phone + "' AND status!='2'", async function(err, checkphone) {
                                        // connection.query("SELECT * FROM user WHERE id = '" + post.user_id + "' ", function(err, rows) {

                                        if(checkphone[0].cnt==0){
                                            console.log("count" + checkemail[0].cnt);
                                            if (checkemail[0].cnt == 0) {
                                                try{
                                                        await client.messages.create({
                                                        from: '+12183962521', 
                                                        to: post.country_code+post.phone, 
                                                        body: "Your brewzer otp is : "+ otp})

                                                        var phone_international = post.country_code + post.phone;
                                                        var phone_national = '0' + post.phone;
                                                        var phone_e164 = post.country_code + post.phone;
                                                        console.log("otp",otp)
                                                        connection.query("INSERT INTO user (first_name , email, country_code ,phone, phone_international, phone_national, phone_e164, photo_id, profile_photo , status, device_type , device_token , created_date , chat_notification, created_by, role, auth_key , password_hash, username, birthdate, otp) VALUES ('" + first_name + "', '" + post.email + "', '" + post.country_code + "', '" + post.phone + "', '" + phone_international + "', '" + phone_national + "', '" + phone_e164 + "', '" + post.photo_id + "', '" + profile_photo + "' , 1 , '" + device_type + "' , '" + device_token + "' , NOW() , 1 , 1 , 3, '" + password_hash + "' , '" + password_hash + "','" + post.username + "', '" + post.birthdate + "','" + otp + "')", function(err, signup) {                                            console.log("signup" + signup);
                                                            if (err) {
                                                                console.log(err);
                                                                response.status = 0;
                                                                response.message = 'Oops! Something went wrong while inserting data';
                                                                res.send(response);
                                                            } else {
                                                                var userData;
                                                                connection.query("SELECT u.*, c.name AS country_name, s.name AS state_name, ci.name AS city_name FROM user AS u LEFT JOIN countries AS c ON c.id = u.country_id LEFT JOIN states AS s ON s.id = u.state_id LEFT JOIN cities AS ci ON ci.id = u.city_id WHERE u.email = '" + post.email + "' AND u.status='1'", function(err, userinfo) {
                                                                    if (err) {
                                                                        console.log(err);
                                                                        response.status = 0;
                                                                        response.message = 'Unable to fetch user data';
                                                                        res.send(response);
                                                                    } else {
                                                                        getUserData(req, userinfo, function(result) {
                                                                            result.device_type = device_type;
                                                                            result.device_token = device_token;
                
                                                                            response.status = 1;
                                                                            response.message = 'Signup successfully';
                                                                            response.data = result;
                                                                            res.send(response);
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });

                                                }
                                                catch (error){
                                                    console.log("erorrrrr", error);
                                                    response.status = 0;
                                                    response.message = 'Unable to send message, pls check phone no.';
                                                    res.send(response);
                                                }
                                                
                                            } else {
                                                response.status = 0;
                                                response.message = 'Email already exist';
                                                res.send(response);
                                            }
                                        }   
                                        else {
                                            response.status = 0;
                                            response.message = 'Phone already exist';
                                            res.send(response);
                                        } 
                                    })    
                                });
                            }
                        });
                    }
                });
            }
        });
    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
});

router.post('/api/user/confirmotp', isUserActive, function(req, res, next) {
    console.log("inside confirm otp");
    console.log("req.body",req.body);
    var post = req.body;
    console.log("otp", post.otp);
    console.log("user_id",user_id);
    if (typeof(post.otp) != 'undefined' && post.otp && typeof(post.user_id) != 'undefined' && post.user_id ) {

        req.getConnection(function(err, connection) {

            connection.query("SELECT * FROM user WHERE id = '" + post.user_id + "' ", function(err, rows) {
                console.log("user data", rows[0].otp);
                if(rows[0].otp == post.otp){
                    connection.query("UPDATE user SET otp_confirmation = '" + 'true' + "' WHERE id = '" + post.user_id + "'", function(err, data) {
                        console.log("dataaaa",data)
                        response.status = 1;
                        response.message = 'Success, otp confirmed';
                        res.send(response);
                    })
                }
                else{
                    response.status = 0;
                    response.message = 'Wrong otp';
                    res.send(response);
                }
            })

        })
    }
    else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }

});
  


//change password
router.post('/api/user/changepassword', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    if (typeof(post.user_id) != 'undefined' && post.user_id && typeof(post.newpassword) != 'undefined' && post.newpassword && typeof(post.oldpassword) != 'undefined' && post.oldpassword) {

        req.getConnection(function(err, connection) {
            if (err) {
                console.log(err);
                response.status = 0;
                response.message = 'Datbase connection error';
                res.send(response);
            } else {
                connection.query("SELECT  COUNT(*) as cnt FROM user WHERE id = '" + post.user_id + "' AND status!='2'", function(err, checkuser) {
                    console.log("count" + checkuser[0].cnt);
                    if (checkuser[0].cnt > 0) {
                        connection.query("SELECT * FROM user WHERE id = '" + post.user_id + "' AND status!='2'", function(err, rows) {
                            if (err) {
                                console.log(err);
                                response.status = 0;
                                response.message = 'No user found';
                                res.send(response);
                            } else {
                                if (rows.length > 0) {
                                    var password_hash = rows[0].password_hash;
                                    var user_id = rows[0].id;

                                    // Load hash from your password DB.
                                    bcrypt.compare(post.oldpassword, password_hash, function(err, result) {
                                        if (err) {
                                            console.log(err);
                                            response.status = 0;
                                            response.message = 'Oops! Something went wrong';
                                            res.send(response);
                                        } else if (result === true) {
                                            console.log("Change Password");
                                            bcrypt.genSalt(10, function(err, salt) {
                                                console.log("salt for hash generated");
                                                if (err) {
                                                    //return next(err);
                                                    console.log(err);
                                                    response.status = 0;
                                                    response.message = err;
                                                    res.send(response);
                                                } else {
                                                    bcrypt.hash(post.newpassword, salt, function(err, hash) {
                                                        if (err) {
                                                            console.log(err);
                                                            response.status = 0;
                                                            response.message = 'Oops! Something went wrong';
                                                            res.send(response);
                                                        } else {
                                                            connection.query("UPDATE user SET password_hash = '" + hash + "' WHERE id = '" + user_id + "'", function(err, data) {
                                                                if (err) {
                                                                    console.log(err);
                                                                    response.status = 0;
                                                                    response.message = 'Oops! Something went wrong';
                                                                    res.send(response);
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                            response.status = 1;
                                            response.message = 'Success';
                                            //response.data = rows[0];
                                            res.send(response);
                                        } else {
                                            console.log("Error %s", err);
                                            response.status = 0;
                                            response.message = 'Invalid Password';
                                            res.send(response);
                                        }
                                        // res === true
                                    });
                                } else {
                                    response.status = 0;
                                    response.message = 'Invalid User';
                                    res.send(response);
                                }
                            }
                        });
                    } else {
                        response.status = 0;
                        response.message = 'Wrong or missing parameters';
                        res.send(response);
                    }
                });
            }
        });

    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
});

//Forgot password API
router.post('/api/user/forgotpassword', upload.array(), function(req, res, next) {
    console.log("In Forgot API with Mail Code");
    var post = req.body;
    response = {};

    if(typeof(post.email) != "undefined" && post.email){
        var useremail = post.email;

        console.log("User Email is : " + useremail);
        var transporter = nodemailer.createTransport({
            // debug: true,
            host: smtp.SMTP_HOST,
            // secureConnection: false, // true for 465, false for other ports
            port: smtp.SMTP_PORT,
            // tls: {cipher:'SSLv3'},
            auth: {
                user: smtp.SMTP_EMAIL,
                pass: smtp.SMTP_PASSWORD,
            }
        });

        var passwordResetToken = bcrypt.genSaltSync(25);
        req.getConnection(function(err, connection) {
            if (err)
                console.log(err);

            connection.query("SELECT * FROM user WHERE email = '" + useremail + "' AND status = '1'", function(err, data) {
                console.log("Data Length is : " + data.length);
                console.log("Data is :",data[0])
                console.log("Password Reset Token is : " + passwordResetToken);
                if (data.length > 0) {
                    console.log("UPDATE user SET password_reset_token = '" + passwordResetToken + "' WHERE id = '" + data[0].id + "' ");
                    connection.query("UPDATE user SET password_reset_token = '" + passwordResetToken + "' WHERE id = '" + data[0].id + "' ", function(err, updateResetToken) {
                        if (err)
                            console.log(err);

                        console.log("" + req.headers.host + "/admin/resetpassword?token=" + passwordResetToken + " ");
                        var mailOptions = {
                            from: 'no.reply.brewzer@gmail.com',
                            to: useremail,
                            subject: 'Reset Password',
                            html: '<p>Hello ' + data[0].first_name + '</p><p>Click on this link to reset your password : <a href="http://' + req.headers.host + '/admin/resetpassword?token=' + passwordResetToken + '">Reset Password</a></p><br /> Regards,<br />Team Brewzer',
                        };
                        transporter.sendMail(mailOptions, function(error, info) {
                            if (error) {
                                console.log(error);
                                response.status = 0;
                                response.message = error;
                                res.send(response);
                            } else {
                                console.log('Email sent: ' + info.response);
                                response.status = 1;
                                response.message = 'Mail has been sent to email-id';
                                res.send(response);
                            }
                        });
                    });
                } else {
                    response.status = 0;
                    response.message = 'Invalid User';
                    res.send(response);
                }
            });
        });
    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
});

//Check phone and email
router.post('/api/user/check-phone', upload.array(), function(req, res, next) {
    var post = req.body;
    response = {};
    if (typeof(post.phone) != 'undefined' && post.phone) {
        validateNumber(post.phone, function(phoneData) {
            req.getConnection(function(err, connection) {
                if (err)
                    throw err;

                connection.query("SELECT * FROM user WHERE (phone ='" + post.phone + "' OR phone_international ='" + phoneData.international_phone + "' OR phone_national ='" + phoneData.national_phone + "' OR phone_e164 ='" + phoneData.E164_phone + "') AND status = 1", function(err, rows) {
                    if (err) {
                        console.log("Error %s", err);
                        response.status = 0;
                        response.message = 'Oops! Something went wrong while fetching data';
                        res.send(response);
                    } else {
                        if (rows.length > 0) {
                            response.status = 0;
                            response.message = 'phone is already in use';
                            res.send(response);
                        } else {

                            response.status = 1;
                            response.message = 'you can use this phone';
                            res.send(response);
                        }
                    }
                });
            });
        });
    } else if (typeof(post.email) != 'undefined' && post.email) {
        req.getConnection(function(err, connection) {
            if (err)
                throw err;
            connection.query("SELECT * FROM user WHERE email ='" + post.email + "' AND status = 1", function(err, rows) {
                if (err) {
                    console.log("Error %s", err);
                    response.status = 0;
                    response.message = 'Oops! Something went wrong while fetching data';
                    res.send(response);
                } else {
                    if (rows.length > 0) {
                        response.status = 0;
                        response.message = 'Email is already exist';
                        res.send(response);
                    } else {
                        response.status = 1;
                        response.message = 'You can use this email';
                        res.send(response);
                    }
                }
            });
        });
    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
});

//Check phone
router.post('/api/user/get-master', upload.array(), function(req, res, next) {
    var post = req.body;
    response = {};
    if (typeof(post.type) != 'undefined' && post.type) {
        var where = "status = 1 ";
        if (typeof(post.id) != 'undefined' && post.id > 0) {
            var id_name = "id";
            if (post.type == "states") {
                var id_name = "country_id";
            } else if (post.type == "sub_categories") {
                var id_name = "category_id";
            } else if (post.type == "cities") {
                var id_name = "state_id";
            }
            where = where + "AND " + id_name + "='" + post.id + "' ";
        }else{
            if (post.type == "countries") {
                where += "ORDER BY id = '231' DESC,name ASC"; //for making US first..
            }
        }
        req.getConnection(function(err, connection) {
            if (err)
                throw err;
            connection.query("SELECT * FROM " + post.type + " WHERE " + where + " ", function(err, rows) {
                if (err) {
                    console.log("Error %s", err);
                    response.status = 0;
                    response.message = 'Oops! Something went wrong while fetching data';
                    res.send(response);
                } else {
                    var data = [];
                    if (rows.length > 0) {
                        rows.forEach(function(row) {
                            data.push(row);
                        });
                    }
                    response.status = 1;
                    response.message = 'Success';
                    response.data = data;
                    res.send(response);
                }
            });
        });
    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
});

//Update profile
router.post('/api/user/update-profile', multerUpload.single('profile_photo'), isUserActive, function(req, res, next) {

    var post = req.body;
    response = {};
    if (typeof(post.user_id) != "undefined" && post.user_id) {
        req.getConnection(function(err, connection) {
            if (err) {
                console.log("Error %s", err);
                response.status = 0;
                response.message = 'Connection error';
                res.send(response);
            } else {
                var t_date = new Date();
                var set_condition = "modified_by= '" + post.user_id + "', modified_date= '" + t_date + "'";

                if (typeof(post.first_name) != "undefined" && post.first_name) {
                    set_condition += ", first_name='" + post.first_name + "'";
                }
                if (typeof(post.birthdate) != "undefined" && post.birthdate) {
                    set_condition += ", birthdate='" + post.birthdate + "'";
                }
                if (typeof(post.address1) != "undefined" && post.address1) {
                    set_condition += ", address1='" + post.address1 + "'";
                }
                if (typeof(post.address2) != "undefined" && post.address2) {
                    set_condition += ", address2='" + post.address2 + "'";
                }
                if (typeof(post.country_id) != "undefined" && post.country_id) {
                    set_condition += ", country_id='" + post.country_id + "'";
                }
                if (typeof(post.state_id) != "undefined" && post.state_id) {
                    set_condition += ", state_id='" + post.state_id + "'";
                }
                if (typeof(post.city_id) != "undefined" && post.city_id) {
                    set_condition += ", city_id='" + post.city_id + "'";
                }
                if (typeof(post.photo_id) != "undefined" && post.photo_id) {
                    set_condition += ", photo_id='" + post.photo_id + "'";
                }
                if (typeof(post.device_type) != "undefined" && post.device_type) {
                    set_condition += ", device_type='" + post.device_type + "'";
                }
                if (typeof(post.device_token) != "undefined" && post.device_token) {
                    set_condition += ", device_token='" + post.device_token + "'";
                }
                if (typeof(post.first_name) != "undefined" && post.first_name) {
                    set_condition += ", first_name='" + post.first_name + "'";
                }
                if (typeof(req.file) != "undefined" && req.file.filename) {
                    set_condition += ", profile_photo='" + req.file.filename + "'";
                }
                if (typeof(post.phone) != "undefined" && post.phone && typeof(post.country_code) != "undefined" && post.country_code) {
                    connection.query("SELECT * from user WHERE phone = '" + post.phone + "' AND id != '" + post.user_id + "' AND status = 1", function(err, data) {
                        if (err) {
                            response.status = 0;
                            response.message = err;
                            res.send(response);
                        } else if (data.length == 0) {
                            var phone_international = post.country_code + post.phone;
                            var phone_national = '0' + post.phone;
                            var phone_e164 = post.country_code + post.phone;
                            set_condition += ", phone='" + post.phone + "', phone_international='" + phone_international + "', phone_national='" + phone_national + "', phone_e164='" + phone_e164 + "'";

                            connection.query("UPDATE user SET " + set_condition + " WHERE id = '" + post.user_id + "'", function(err, data) {
                                if (err) {
                                    response.status = 0;
                                    response.message = err;
                                    res.send(response);
                                } else {
                                    connection.query("SELECT u.*, c.name AS country_name, s.name AS state_name, ci.name AS city_name FROM user AS u LEFT JOIN countries AS c ON c.id = u.country_id LEFT JOIN states AS s ON s.id = u.state_id LEFT JOIN cities AS ci ON ci.id = u.city_id WHERE u.id='" + post.user_id + "'", function(err, rows) {
                                        getUserData(req, rows, function(result) {
                                            response.status = 1;
                                            response.message = 'Successfully updated';
                                            response.data = result;
                                            res.send(response);
                                        });
                                    });
                                }
                            });
                        } else {
                            response.status = 0;
                            response.message = 'Phone number already exist';;
                            res.send(response);
                        }
                    });
                } else {
                    connection.query("UPDATE user SET " + set_condition + " WHERE id = '" + post.user_id + "'", function(err, data) {
                        if (err) {
                            response.status = 0;
                            response.message = err;
                            res.send(response);
                        } else {
                            connection.query("SELECT u.*, c.name AS country_name, s.name AS state_name, ci.name AS city_name FROM user AS u LEFT JOIN countries AS c ON c.id = u.country_id LEFT JOIN states AS s ON s.id = u.state_id LEFT JOIN cities AS ci ON ci.id = u.city_id WHERE u.id='" + post.user_id + "'", function(err, rows) {
                                getUserData(req, rows, function(result) {
                                    //Save other data except mobile number
                                    response.status = 1;
                                    response.message = 'Successfully updated';
                                    response.data = result;
                                    res.send(response);
                                });
                            });
                        }
                    });
                }

            }
        });
    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
});

/**
 * API to check entered zipcode has products available or not
 * @param  zipcode, user_id
 * @param  boolean
 */
router.post('/api/user/check-zipcode', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    console.log(JSON.stringify(post));
    if (typeof(post.user_id) != 'undefined' && post.user_id && typeof(post.zipcode) != 'undefined' && post.zipcode) {
        console.log(post.user_id);
        var result = {};
        req.getConnection(function(err, connection) {
            if (err) {
                response.status = 0;
                response.message = 'Oops! Something went wrong';
                res.send(response);
            } else {
                connection.query("SELECT count(*) as pcount FROM product where FIND_IN_SET('" + post.zipcode + "', zipcode) AND status ='1'", function(err, rows) {
                    console.log(JSON.stringify(rows));
                    if (err) {
                        response.status = 0;
                        response.message = 'Oops! Something went wrong';
                        res.send(response);
                    } else {
                        var zip_exists = 0;
                        if (rows.length > 0 && rows[0].pcount > 0) {
                            zip_exists = 1;
                            connection.query("UPDATE user SET zipcode = '" + post.zipcode + "' WHERE id = '" + post.user_id + "'", function(err, data) {
                                if (err) {
                                    console.log(err);
                                    response.status = 0;
                                    response.message = 'Oops! Something went wrong';
                                    res.send(response);
                                } else {
                                    //result.zip_exists = zip_exists;
                                    response.status = 1;
                                    response.message = 'Success';
                                    //response.data = result;
                                    res.send(response);
                                }
                            });
                        } else {
                            //result.zip_exists = zip_exists;
                            response.status = 0;
                            response.message = 'There are no products available at your entered ZIPCODE, please try with another zipcode!';
                            //response.data = result;
                            res.send(response);
                        }
                    }
                })
            }
        })
    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
})

/**
 * API to get list of categories based on entered zipcode
 * @param  zipcode
 * @param  category name, image, id
 */
router.post('/api/user/getcategory', upload.array(), function(req, res, next) {
    var post = req.body;
    response = {};
    console.log(JSON.stringify(post));
    if (typeof(post.zipcode) != 'undefined' && post.zipcode) {
        var result = [];
        req.getConnection(function(err, connection) {
            if (err) {
                response.status = 0;
                response.message = 'Oops! Something went wrong';
                res.send(response);
            } else {
                connection.query("SELECT DISTINCT c.id,c.name,c.photo FROM `categories` as c INNER JOIN `product` as p on p.category_id= c.id WHERE FIND_IN_SET('" + post.zipcode + "', p.zipcode) AND p.status ='1' AND c.status ='1'", function(err, rows) {
                    console.log(JSON.stringify(rows));
                    if (err) {
                        response.status = 0;
                        response.message = 'Oops! Something went wrong';
                        res.send(response);
                    } else {
                        if (rows.length > 0) {
                            var url = 'http://' + req.headers.host + '/category/';
                            for (var i = rows.length - 1; i >= 0; i--) {
                                var resObj = {};
                                resObj.cat_id = rows[i].id;
                                resObj.name = rows[i].name;
                                resObj.image_url = (rows[i].photo != null)
                                    ? url + rows[i].photo
                                    : '';
                                result.push(resObj);
                            }
                            response.status = 1;
                            response.message = 'Success';
                            response.data = result;
                            res.send(response);
                        } else {
                            response.status = 1;
                            response.message = 'Success';
                            response.data = result;
                            res.send(response);
                        }
                    }
                })
            }
        })
    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
})

/**
 * API to get sub category list based on category
 * @param  cat_id
 * @param  sub category id, name
 */
router.post('/api/user/getsubcategory', upload.array(), function(req, res, next) {
    var post = req.body;
    response = {};
    console.log(JSON.stringify(post));
    if (typeof(post.cat_id) != 'undefined' && post.cat_id && typeof(post.zipcode) != 'undefined' && post.zipcode) {
        var result = [];
        req.getConnection(function(err, connection) {
            if (err) {
                response.status = 0;
                response.message = 'Oops! Something went wrong';
                res.send(response);
            } else {
                connection.query("SELECT DISTINCT s.id,s.name FROM `sub_categories` as s INNER JOIN `product` as p ON p.subcategory_id = s.id WHERE s.category_id = '" + post.cat_id + "' AND FIND_IN_SET('" + post.zipcode + "', p.zipcode) AND p.status ='1' AND s.status ='1'", function(err, rows) {
                    console.log(JSON.stringify(rows));
                    if (err) {
                        response.status = 0;
                        response.message = 'Oops! Something went wrong';
                        res.send(response);
                    } else {
                        if (rows.length > 0) {
                            for (var i = rows.length - 1; i >= 0; i--) {
                                var resObj = {};
                                resObj.sub_cat_id = rows[i].id;
                                resObj.name = rows[i].name;
                                result.push(resObj);
                            }
                            response.status = 1;
                            response.message = 'Success';
                            response.data = result;
                            res.send(response);
                        } else {
                            response.status = 1;
                            response.message = 'Success';
                            response.data = result;
                            res.send(response);
                        }
                    }
                })
            }
        })
    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
})

router.post('/api/user/productlist', upload.array(), function(req, res, next) {
    var post = req.body;
    response = {};
    console.log(JSON.stringify(post));
    if (typeof(post.sub_cat_id) != 'undefined' && post.sub_cat_id && typeof(post.zipcode) != 'undefined' && post.zipcode) {
        var result = [];
        req.getConnection(function(err, connection) {
            if (err) {
                response.status = 0;
                response.message = 'Oops! Something went wrong';
                res.send(response);
            } else {
                var filter = "";
                if(typeof(post.min_price) != "undefined" && post.min_price != "" && typeof(post.max_price) != "undefined" && post.max_price != ""){
                    filter += " AND (p.price BETWEEN '"+post.min_price+"' AND '"+post.max_price+"')";
                }
                if(typeof(post.brand_id) != "undefined" && post.brand_id > 0){
                    filter += " AND p.brand_id = '"+post.brand_id+"'";
                }
                if(typeof(post.flavours_id) != "undefined" && post.flavours_id != ""){
                    var post_flavours = post.flavours_id;
                    var flavours = post_flavours.split(",");
                    var inner_cond = "";
                    for(var i = 0; i < flavours.length; i++){
                        if (i == 0){
                            inner_cond += " FIND_IN_SET('"+flavours[i]+"', p.flavours_id)";
                        } else if (i == flavours.length) { //not in use as loop starts from i = 0
                            inner_cond += " OR FIND_IN_SET('"+flavours[i]+"', p.flavours_id) OR p.flavours_id IS NULL";
                        } else {
                            inner_cond += " OR FIND_IN_SET('"+flavours[i]+"', p.flavours_id)";
                        }
                    }
                    filter += " AND ("+inner_cond+")";
                }
                connection.query("SELECT DISTINCT p.id, p.name, p.description, p.price, p.zipcode, p.brand_id, p.flavours_id, p.image, c.id AS category_id, c.name AS category_name, s.id AS subcategory_id, s.name AS subcategory_name  FROM `product` as p LEFT JOIN sub_categories AS s ON s.id = p.subcategory_id LEFT JOIN categories AS c ON c.id = s.category_id WHERE p.subcategory_id = '" + post.sub_cat_id + "' AND FIND_IN_SET('" + post.zipcode + "', p.zipcode) AND p.status ='1'"+filter+" ", function(err, rows) {
                    console.log(JSON.stringify(rows));
                    if (err) {
                        response.status = 0;
                        response.message = 'Oops! Something went wrong';
                        res.send(response);
                    } else {
                        if (rows.length > 0) {
                            var url = 'http://' + req.headers.host + '/product/';
                            var thumburl = 'http://' + req.headers.host + '/product/';
                            for (var i = rows.length - 1; i >= 0; i--) {
                                var resObj = {};
                                resObj.prod_id = rows[i].id;
                                resObj.name = rows[i].name;
                                resObj.zipcode = rows[i].zipcode;
                                resObj.price = Number(rows[i].price);
                                resObj.description = rows[i].description;
                                resObj.image_url = (rows[i].image != '')
                                    ? url + rows[i].image
                                    : '';
                                resObj.thumb_image_url = (rows[i].image != '')
                                    ? thumburl + rows[i].image
                                    : '';
                                resObj.category_id = rows[i].category_id;
                                resObj.category_name = rows[i].category_name;
                                resObj.subcategory_id = rows[i].subcategory_id;
                                resObj.subcategory_name = rows[i].subcategory_name;
                                resObj.brand_id = rows[i].brand_id;
                                resObj.flavours_id = (rows[i].flavours_id != null) ? rows[i].flavours_id : "";
                                result.push(resObj);
                            }
                            response.status = 1;
                            response.message = 'Success';
                            response.data = result;
                            res.send(response);
                        } else {
                            response.status = 1;
                            response.message = 'Success';
                            response.data = result;
                            res.send(response);
                        }
                    }
                })
            }
        })
    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
})

router.post('/api/user/add-edit-address', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    console.log(JSON.stringify(post));
    
    if (typeof(post.user_id) != 'undefined' && post.user_id && typeof(post.address_title) != 'undefined' && post.address_title && typeof(post.address1) != "undefined" && post.address1 && typeof(post.country_id) != "undefined" && post.country_id && typeof(post.state_id) != "undefined" && post.state_id && typeof(post.city_id) != "undefined" && post.city_id && typeof(post.zipcode) != "undefined" && post.zipcode!="") {
        req.getConnection(function(err, connection) {
            if (err) {
                response.status = 0;
                response.message = 'Oops! Something went wrong';
                res.send(response);
            } else {
                var is_zipcode_err = 0;
                var zipcode = (typeof(post.zipcode)!="undefined" && post.zipcode!="") ? post.zipcode : '';
                var latitude = (typeof(post.latitude)!="undefined" && post.latitude!="") ? post.latitude : '0';
                var longitude = (typeof(post.longitude)!="undefined" && post.longitude!="") ? post.longitude : '0';
                async.series([
                    function(cb){
                        var returnstr = '?API=CityStateLookup&XML=<CityStateLookupRequest%20USERID="'+params.USPS_DATA["USERNAME"]+'"><ZipCode ID= "0">';
                        returnstr += '<Zip5>'+zipcode+'</Zip5>';
                        returnstr += '</ZipCode></CityStateLookupRequest>'

                        rest.get(params.USPS_URL + returnstr).on('complete', function(result) {
                            if (result instanceof Error) {
                                is_zipcode_err = 1;
                                response.status = 0;
                                response.message = 'Zipcode is not valid to ship your product';
                                cb();
                            } else {
                                var jsonobj = parser.toJson(result);
                                var data = JSON.parse(jsonobj);
                                if(typeof(data.CityStateLookupResponse.ZipCode.Error)!="undefined" && data.CityStateLookupResponse.ZipCode.Error){
                                    is_zipcode_err = 1;
                                    response.status = 0;
                                    response.message = 'Zipcode is not valid to ship your product';
                                }
                                cb();
                            }
                        });
                    },
                    function(cb1){
                        if(is_zipcode_err == 0){
                            if(typeof(post.address_id) != 'undefined' && post.address_id){
                                var sql = "UPDATE address SET address_title='"+post.address_title+"', address1='"+post.address1+"', address2='"+post.address2+"', country_id='"+post.country_id+"', state_id='"+post.state_id+"', city_id='"+post.city_id+"', zipcode='"+zipcode+"', latitude='"+latitude+"', longitude='"+longitude+"', modified_by='"+post.user_id+"',modified_date=NOW() WHERE id='"+post.address_id+"'";
                            }else{
                                var sql = "INSERT INTO address (address_title , address1, address2 ,country_id, state_id, city_id, zipcode, latitude, longitude, user_id, created_date, created_by) VALUES ('" + post.address_title + "', '" + post.address1 + "', '" + post.address2 + "', '" + post.country_id + "', '" + post.state_id + "', '" + post.city_id + "', '"+zipcode+"', '"+latitude+"', '"+longitude+"', '" + post.user_id + "',  NOW() , 1)";
                            }
                            connection.query(sql, function(err, rowData) {
                                if(typeof(post.address_id) != 'undefined' && post.address_id){
                                    var add_id = post.address_id;
                                }else{
                                    var add_id = rowData.insertId;
                                }
                                console.log();
                                if (err) {
                                    console.log(err);
                                    response.status = 0;
                                    response.message = 'Something went wrong';
                                    cb1();
                                } else {
                                    connection.query("SELECT a.*, c.name AS country_name, s.name AS state_name, ci.name AS city_name FROM address AS a LEFT JOIN countries AS c ON c.id = a.country_id LEFT JOIN states AS s ON s.id = a.state_id LEFT JOIN cities AS ci ON ci.id = a.city_id WHERE a.id = " + add_id + " AND a.status ='1'", function(err, rows) {
                                        console.log(JSON.stringify(rows));
                                        if (err) {
                                            response.status = 0;
                                            response.message = 'Oops! Something went wrong';
                                            cb1();
                                        } else {
                                            getAddressData(req, rows[0], function(result) {
                                                response.status = 1;
                                                response.message = 'Success';
                                                response.data = result;
                                                cb1();
                                            });
                                        }
                                    })                        
                                }
                            });
                        }else{
                            cb1();
                        }
                    }
                ], function(err){
                    if(err){
                        response.status = 0;
                        response.message = 'Something went wrong';
                        res.send(response);
                    }else{
                        res.send(response);
                    }
                })
            }
        });
    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
});

//Delete address, params : address_id (PK)
router.post('/api/user/delete-address', upload.array(), function(req, res, next){
    var post = req.body;
    var response = {};
    if (typeof(post.address_id) != 'undefined' && post.address_id) {
        req.getConnection(function(err, connection) {
            if (err) {
                response.status = 0;
                response.message = 'Oops! Something went wrong';
                res.send(response);
            } else {
                var sql = "UPDATE address SET status='2' WHERE id='"+post.address_id+"'";
                connection.query(sql, function(err, rowData) {
                    if(err){
                        response.status = 0;
                        response.message = 'Oops! something went wrong';
                        res.send(response);
                    }else{
                        response.status = 1;
                        response.message = 'Successfully deleted';
                        res.send(response);
                    }
                });
            }
        });
    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
});

//Verify address web api
router.post('/api/user/verify-address', upload.array(), function(req, res, next){
    var post = req.body;
    var response = {};
    var returnstr = '?API=CityStateLookup&XML=<CityStateLookupRequest%20USERID="'+params.USPS_DATA["USERNAME"]+'"><ZipCode ID= "0">';
    /*var add1 = (typeof(post.address1)!="undefined" && post.address1!="") ? post.address1 : '';
    var add2 = (typeof(post.address2)!="undefined" && post.address2!="") ? post.address2 : '';
    var city = (typeof(post.city)!="undefined" && post.city!="") ? post.city : '';
    var state = (typeof(post.state)!="undefined" && post.state!="") ? post.state : '';
    var zipcode = (typeof(post.zipcode)!="undefined" && post.zipcode!="") ? post.zipcode : '';
    
    returnstr += '<Address1>'+add1+'</Address1>';
    returnstr += '<Address2>'+add2+'</Address2>';
    returnstr += '<City>'+city+'</City>';
    returnstr += '<State>'+state+'</State>';
    returnstr += '<Zip5>'+zipcode+'</Zip5>';
    returnstr += '<Zip4></Zip4>';
    returnstr += '</Address></AddressValidateRequest>';*/

    var zipcode = (typeof(post.zipcode)!="undefined" && post.zipcode!="") ? post.zipcode : '';

    returnstr += '<Zip5>'+zipcode+'</Zip5>';
    returnstr += '</ZipCode></CityStateLookupRequest>'

    console.log("USPS URL==========>"+ params.USPS_URL + returnstr);
    rest.get(params.USPS_URL + returnstr).on('complete', function(result) {
        if (result instanceof Error) {
            res.send(result);
        } else {
            var jsonobj = parser.toJson(result);
            console.log("REPONSE JSON--->:"+JSON.stringify(jsonobj));
            var data = JSON.parse(jsonobj);

            if(typeof(data.CityStateLookupResponse.ZipCode.Error)!="undefined" && data.CityStateLookupResponse.ZipCode.Error){
                response.status = 0;
                response.message = data.CityStateLookupResponse.ZipCode.Error.Description;
                response.data = data.CityStateLookupResponse.ZipCode.Error;
            }else{
                response.status = 1;
                response.message = 'Success';
                response.data = data.CityStateLookupResponse.ZipCode;
            }
            res.send(response);
        }
    });
});


router.post('/api/user/get-address', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    console.log(JSON.stringify(post));
    if (typeof(post.user_id) != 'undefined' && post.user_id) {
        req.getConnection(function(err, connection) {
            connection.query("SELECT a.*, c.name AS country_name, s.name AS state_name, ci.name AS city_name FROM address AS a LEFT JOIN countries AS c ON c.id = a.country_id LEFT JOIN states AS s ON s.id = a.state_id LEFT JOIN cities AS ci ON ci.id = a.city_id WHERE a.user_id = " + post.user_id + " AND a.status ='1'", function(err, rows) {
                if (err) {
                    response.status = 0;
                    response.message = 'Oops! Something went wrong';
                    res.send(response);
                } else {
                    var arrAddress = [];
                    for (var i = 0; i < rows.length; i++){
                        getAddressData(req, rows[i], function(result) {
                            arrAddress.push(result);
                        });
                    }
                    response.status = 1;
                    response.message = 'Success';
                    response.data = arrAddress;
                    res.send(response);
                }
            })    
        })
    }
});
    
//update push notification
router.post('/api/user/update-setting', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    var data = [];
    var result = {};
    var required_params = ['user_id'];
    var elem = functions.validateReqParam(post, required_params);
    var valid = elem.missing.length==0 && elem.blank.length==0;
    if(valid) {
        var language = post.language;
        req.getConnection(function(err, connection) {
            if(err){
                response.status = 0;
                response.message = "Oops! Something went wrong.";
                res.send(response);
            } else {
                var conditions = "";
                var additional_query = "";
                if(typeof(post.push_notification) != "undefined" && post.push_notification){
                    conditions += ", push_notification = '"+post.push_notification+"'";
                }
                if(typeof(post.device_type) != "undefined" && post.device_type){
                    conditions += ", device_type = '"+post.device_type+"'";
                }
                if(typeof(post.device_token) != "undefined" && post.device_token){
                    conditions += ", device_token = '"+post.device_token+"'";
                    additional_query += "UPDATE user SET device_token='', device_type='' WHERE device_token='"+post.device_token+"';";
                }
                var sql = additional_query + "UPDATE user SET modified_by = '"+post.user_id+"', modified_date = NOW()"+conditions+" WHERE id='"+post.user_id+"'";
                connection.query(sql, function(err, rowdata){
                    if(err)
                        throw err;
                    response.status = 1;
                    response.message = "Successfully updated";
                    res.send(response);
                });
            }
        });
    } else {
        response.status = 0;
        response.message = "Wrong or missing parameters";
        res.send(response);
    }
});


//Get CMS content
router.post('/api/user/getcmscontent', upload.array(), function(req, res, next) {
    var post = req.body;
    response = {};
    var data = [];
    var result = {};
    var required_params = ['type'];
    var elem = functions.validateReqParam(post, required_params);
    var valid = elem.missing.length==0 && elem.blank.length==0;
    if(valid) {
        var language = post.language;
        req.getConnection(function(err, connection) {
            if(err){
                response.status = 0;
                response.message = "Oops! Something went wrong.";
                res.send(response);
            } else { 
                var sql = "SELECT * FROM cms_management WHERE type='"+post.type+"'";
                connection.query(sql, function(err, rows){
                    if(err){
                        throw err;
                    }
                    if(rows.length > 0){
                        result.id = parseInt(rows[0].id);
                        result.title = rows[0].title;
                        result.content = rows[0].content;
                        //data.push(result);
                    }
                    response.status = 1;
                    response.message = "Success";
                    //response.data = data;
                    response.data = result;
                    res.send(response);
                });
            }
        });
    } else {
        response.status = 0;
        response.message = "Wrong or missing parameters";
        res.send(response);
    }
});

//Fetch User function
function fetchUser(req, done) {
    req.getConnection(function(err, connection) {
        connection.query("SELECT * FROM user WHERE email ='" + req.body.email + "' AND status = 1", function(err, rows) {
            if (err) {
                console.log("Error %s", err);
                response.status = 0;
                response.message = 'Oops! Something went wrong while fetching data';
                res.send(response);
            } else {
                console.log("ROW LENGTH IS ::: " + rows.length);
                if (rows.length > 0) {
                    done(rows);
                } else {
                    done(false);
                }
            }
        });
    });
}

function getUserData(req, rows, done) {
    var userData = {};

    userData = {
        user_id: parseInt(rows[0].id),
        first_name: rows[0].first_name,
        country_code: (rows[0].country_code != null)
            ? rows[0].country_code
            : "",
        phone: (rows[0].phone != null)
            ? rows[0].phone
            : "",
        phone_international: (rows[0].phone_international != null)
            ? rows[0].phone_international
            : "",
        phone_national: (rows[0].phone_national != null)
            ? rows[0].phone_national
            : "",
        phone_e164: (rows[0].phone_e164 != null)
            ? rows[0].phone_e164
            : "",
        email: (rows[0].email != null)
            ? rows[0].email
            : "",
        profile_photo: (rows[0].profile_photo != null)
            ? "http://" + req.headers.host + "/user/" + rows[0].profile_photo
            : "",
        photo_id: (rows[0].photo_id != null)
            ? rows[0].photo_id
            : "",
        address1: (rows[0].address1 != null)
            ? rows[0].address1
            : "",
        address2: (rows[0].address2 != null)
            ? rows[0].address2
            : "",
        birthdate: (rows[0].birthdate != null)
            ? rows[0].birthdate
            : "",
        country_id: parseInt(rows[0].country_id),
        country_name: (rows[0].country_name != null)
            ? rows[0].country_name
            : "",
        state_id: parseInt(rows[0].state_id),
        state_name: (rows[0].state_name != null)
            ? rows[0].state_name
            : "",
        city_id: parseInt(rows[0].city_id),
        city_name: (rows[0].city_name != null)
            ? rows[0].city_name
            : "",
        zipcode: (rows[0].zipcode != null)
            ? rows[0].zipcode
            : "",
        device_type: (rows[0].device_type != null)
            ? rows[0].device_type
            : "",
        device_token: (rows[0].device_token != null)
            ? rows[0].device_token
            : "",
        push_notification: (rows[0].push_notification != null)
        ? rows[0].push_notification
        : 1,
    }
    return done(userData);
}

function getAddressData(req, rows, done) {
    var addressData = {};

    addressData = {
        id: parseInt(rows.id),
        address_title: rows.address_title,        
        address1: (rows.address1 != null)
            ? rows.address1
            : "",
        address2: (rows.address2 != null)
            ? rows.address2
            : "",       
        country_id: parseInt(rows.country_id),
        country_name: (rows.country_name != null)
            ? rows.country_name
            : "",
        state_id: parseInt(rows.state_id),
        state_name: (rows.state_name != null)
            ? rows.state_name
            : "",
        city_id: parseInt(rows.city_id),
        city_name: (rows.city_name != null)
            ? rows.city_name
            : "",       
        zipcode: (rows.zipcode != null)
            ? rows.zipcode
            : "",
    }
    return done(addressData);
}

function validateNumber(phone, done) {
    // Require `PhoneNumberFormat`.
    var PNF = require('google-libphonenumber').PhoneNumberFormat;

    // Get an instance of `PhoneNumberUtil`.
    var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

    // Parse number with country code.
    var phoneNumber = phoneUtil.parse(phone, 'US');

    var national_phone = phoneUtil.format(phoneNumber, PNF.NATIONAL);
    var international_phone = phoneUtil.format(phoneNumber, PNF.INTERNATIONAL);
    var E164_phone = phoneUtil.format(phoneNumber, PNF.E164);
    var phone_data = {};
    phone_data = {
        national_phone: national_phone,
        international_phone: international_phone,
        E164_phone: E164_phone
    }
    return done(phone_data);
}

function getMasterName(id, table, req, callback) {
    req.getConnection(function(err, connection) {
        connection.query("SELECT name FROM " + table + " WHERE id ='" + id + "'", function(err, rows) {
            if (err) {
                callback(err, null);
            } else {
                if (rows.length > 0) {
                    console.log(rows[0].name);
                    callback(null, rows[0].name);
                } else {
                    callback("No data found", null);
                }
            }
        });
    });
}




module.exports = router;

// route middleware to ensure user is valid
function isUserActive(req, res, next) {
    var post = req.body;
    var userid = 0;
    var response = {};
    if (typeof (post.user_id) != "undefined" && post.user_id > 0) {
        userid = post.user_id;
    }
    if (userid > 0) {
        req.getConnection(function (err, connection) {
            if (err) {
                console.log(err);
                throw err;
            } else {
                var sql = "SELECT status FROM user WHERE id = '" + userid + "'";
                connection.query(sql, function (err, rows) {
                    if (err) {
                        console.log(err);
                        throw err;
                    } else {
                        if (rows.length > 0) {
                            if (rows[0].status == 0) {
                                response.status = -1;
                                response.message = "Sorry! Your account is in-active temporarily";
                                res.send(response);
                            } else if (rows[0].status == 2) {
                                response.status = -1;
                                response.message = "Sorry! Your account is deactive permanently";
                                res.send(response);
                            } else {
                                return next();
                            }
                        } else {
                            response.status = -1;
                            response.message = "Sorry! Invalid user";
                            res.send(response);
                        }
                    }
                });
            }
        });
    } else {
        return next();
    }
}


