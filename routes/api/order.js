var express = require('express');
var router = express.Router();
var hash = require('../../pass').hash;
var path = require('path');
var multer = require('multer');
var mkdirp = require('mkdirp');
var bcrypt = require('bcryptjs');
var passport = require('passport');
var ApiContracts = require('authorizenet').APIContracts;
var ApiControllers = require('authorizenet').APIControllers;
var SDKConstants = require('authorizenet').Constants;
var async = require("async");
const TESTAPI_KEY = 'sk_live_hBxwAVs3chlsORWEv2nGqenn004P2lr3il';
const stripe = require('stripe')(TESTAPI_KEY);

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function(req, file, cb) {
        var getFileExt = function(fileName) {
            var fileExt = fileName.split(".");
            if (fileExt.length === 1 || (fileExt[0] === "" && fileExt.length === 2)) {
                return "";
            }
            return fileExt.pop();
        }
        var microsecond = Math.round(new Date().getTime() / 1000 * Math.floor(Math.random() * 1000000000)); //new Date().getTime();
        cb(null, microsecond + path.extname(file.originalname)); // Date.now() + '.' + getFileExt(file.originalname))
    }
});
var multerUpload = multer({storage: storage});

var response = {};
var upload = multer();

//Get Order details of user
router.post('/api/order/get-orders', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    if(typeof(post.user_id) != "undefined" && post.user_id > 0){
        req.getConnection(function(err, connection) {
            if (err)
                throw err;
            connection.query("SELECT o.*, c.code AS coupon_code, ba.id AS bill_address_id, ba.address_title AS bill_add_title, ba.address1 AS bill_address1, ba.address2 AS bill_address2, ba.country_id AS bill_country, ba.state_id AS bill_state, ba.city_id AS bill_city, ba.zipcode AS bill_zipcode, sa.id AS ship_address_id, sa.address_title AS ship_add_title, sa.address1 AS ship_address1, sa.address2 AS ship_address2, sa.country_id AS ship_country, sa.state_id AS ship_state, sa.city_id AS ship_city, sa.zipcode AS ship_zipcode FROM orders AS o LEFT JOIN address AS ba ON ba.id = o.billing_address_id LEFT JOIN address AS sa ON sa.id = o.shipping_address_id LEFT JOIN coupons AS c ON c.id = o.coupon_id WHERE o.user_id = '"+post.user_id+"' AND o.status != '2' ORDER BY o.id DESC", function(err, rows){
                console.log("Query Execute>>");
                if(err)
                    throw err;
                response.status = 1;
                response.message = 'Success';
                var data = [];
                if(rows.length > 0){
                    //rows.forEach(function(rowData){
                    async.forEachOf(rows, function(rowData, key, callback){
                        var orderData = {};
                        var billing = {};
                        var shipping = {};
                        var ost = rowData.order_status;
                        console.log("GENE_POINT>>>>>>>>>"+JSON.stringify(params.ORDER_STATUS[ost]));

                        orderData.order_id = rowData.id;
                        orderData.coupon_code = (rowData.coupon_code) ? rowData.coupon_code : "";
                        orderData.product_detail  = JSON.parse(rowData.product_detail);
                        orderData.sub_total = rowData.sub_total;
                        orderData.tax = rowData.tax;
                        orderData.delivery_fee = rowData.delivery_fees;
                        orderData.tip = rowData.tip;
                        orderData.discount = rowData.discount;
                        orderData.total = rowData.total;
                        orderData.order_status = rowData.order_status;
                        orderData.estimated_time = (typeof(rowData.estimated_time)!="undefined" && rowData.estimated_time) ? rowData.estimated_time : 0;
                        orderData.order_status_label = params.ORDER_STATUS[ost];
                        //orderData.order_date = new Date(rowData.created_date).getTime();


                        var order_place_date = new Date(rowData.created_date).getTime();
                        orderData.order_date =  order_place_date

                        var date_after_five_minutes = new Date(order_place_date + (5*60*1000));
                        var order_place_date_time_later = date_after_five_minutes.getTime();

                        if(order_place_date_time_later > new Date().getTime()){
                            orderData.is_display_cancel_order_option = 1
                        }else {
                            orderData.is_display_cancel_order_option = 0
                        }

                        orderData.delivery_date = new Date(rowData.delivery_date).getTime();

                        billing.id = (rowData.bill_address_id) ? rowData.bill_address_id : 0;
                        billing.address_title = (rowData.bill_add_title) ? rowData.bill_add_title : "";
                        billing.address1 = (rowData.bill_address1) ? rowData.bill_address1 : "";
                        billing.address2 = (rowData.bill_address2) ? rowData.bill_address2 : "";
                        billing.zipcode = (rowData.bill_zipcode) ? rowData.bill_zipcode : "";
                        billing.country_name = "";
                        billing.state_name = "";
                        billing.city_name = "";

                        shipping.id = (rowData.ship_address_id) ? rowData.ship_address_id : 0;
                        shipping.address_title = (rowData.ship_add_title) ? rowData.ship_add_title : "";
                        shipping.address1 = (rowData.ship_address1) ? rowData.ship_address1 : "";
                        shipping.address2 = (rowData.ship_address2) ? rowData.ship_address2 : "";
                        shipping.zipcode = (rowData.ship_zipcode) ? rowData.ship_zipcode : "";
                        shipping.country_name = "";
                        shipping.state_name = "";
                        shipping.city_name = "";

                        orderData.is_favorite = 0;
                        orderData.rating = 0;
                        orderData.comment = "";

                        async.parallel([
                            function(callback1){
                                var sql = "SELECT name FROM countries WHERE id='"+rowData.bill_country+"' AND status='1'";
                                connection.query(sql, function(err, rows){
                                    if(err)
                                        throw err;
                                    if(rows.length > 0){
                                        console.log(rows[0].name);
                                        billing.country_name = rows[0].name;
                                    }
                                    callback1();
                                });
                            },
                            function(callback2){
                                var sql = "SELECT name FROM states WHERE id='"+rowData.bill_state+"' AND status='1'";
                                connection.query(sql, function(err, rows){
                                    if(err)
                                        throw err;
                                    if(rows.length > 0){
                                        console.log(rows[0].name);
                                        billing.state_name = rows[0].name;
                                    }
                                    callback2();
                                });
                            },
                            function(callback3){
                                var sql = "SELECT name FROM cities WHERE id='"+rowData.bill_city+"' AND status='1'";
                                connection.query(sql, function(err, rows){
                                    if(err)
                                        throw err;
                                    if(rows.length > 0){
                                        console.log(rows[0].name);
                                        billing.city_name = rows[0].name;
                                    }
                                    callback3();
                                });
                            },
                            function(callback4){
                                var sql = "SELECT name FROM countries WHERE id='"+rowData.ship_country+"' AND status='1'";
                                connection.query(sql, function(err, rows){
                                    if(err)
                                        throw err;
                                    if(rows.length > 0){
                                        console.log(rows[0].name);
                                        shipping.country_name = rows[0].name;
                                    }
                                    callback4();
                                });
                            },
                            function(callback5){
                                var sql = "SELECT name FROM states WHERE id='"+rowData.ship_state+"' AND status='1'";
                                connection.query(sql, function(err, rows){
                                    if(err)
                                        throw err;
                                    if(rows.length > 0){
                                        console.log(rows[0].name);
                                        shipping.state_name = rows[0].name;
                                    }
                                    callback5();
                                });
                            },
                            function(callback6){
                                var sql = "SELECT name FROM cities WHERE id='"+rowData.ship_city+"' AND status='1'";
                                connection.query(sql, function(err, rows){
                                    if(err)
                                        throw err;
                                    if(rows.length > 0){
                                        console.log(rows[0].name);
                                        shipping.city_name = rows[0].name;
                                    }
                                    callback6();
                                });
                            },
                            function(callback7){
                                var sql = "SELECT * FROM order_survey WHERE order_id='"+rowData.id+"' AND user_id = '"+post.user_id+"' AND status='1'";
                                connection.query(sql, function(err, rows){
                                    if(err)
                                        throw err;
                                    if(rows.length > 0){
                                        console.log(rows[0].name);
                                        orderData.is_favorite = rows[0].is_favorite;
                                        orderData.rating = rows[0].rating;
                                        orderData.comment = rows[0].comment;
                                    }
                                    callback7();
                                });
                            },
                        ], function(err){
                            if(err){
                                throw err; 
                                console.log("Final ERR============>"+err);
                            }
                            orderData.billing = billing;
                            orderData.shipping = shipping;
                            data.push(orderData);
                            callback();
                        });
                    }, function(err){
                        if(err){
                            throw err;
                            console.log("Final ERROR==========>"+err)
                        }
                        response.data = data;
                        res.send(response);
                    });
                } else {
                    response.data = data;
                    res.send(response);
                }
            });
        });
    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
});

//cancel order
router.post('/api/order/cancel-orders', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    if(typeof(post.user_id) != "undefined" && post.user_id > 0 && typeof(post.order_id) != "undefined" && post.order_id > 0){
        req.getConnection(function(err, connection) {
            if (err)
                throw err;
            connection.query("SELECT o.* FROM orders AS o WHERE o.user_id = '"+post.user_id+"' AND o.id = '"+post.order_id+"' AND o.status != '2' ORDER BY o.id DESC", function(err, rows){
                console.log("Query Execute>>"+JSON.stringify(rows));
                if(err)
                    throw err;
                var today_date = new Date();
                var current_date_time = today_date.getTime();

                var order_place_date = new Date(rows[0].created_date);
                var date_after_five_minutes = new Date(order_place_date.getTime()+(5*60*1000));
                var order_place_date_time_later = date_after_five_minutes.getTime();
                if(order_place_date_time_later < current_date_time){
                    if(rows[0].transaction_id != null && rows[0].transaction_id != "" && rows[0].transaction_id != "0"){
                        refundTransaction(req, rows[0], function(err, transData){
                            if(err){
                                response.status = 0;
                                response.message = err;
                            } else {
                                console.log("On order cancellation"+JSON.stringify(transData));
                                connection.query("INSERT INTO cancelled_orders (order_id, transaction_id, payment_response, payment_status, payment_message, created_by, created_date) VALUES (?, ?, ?, ?, ?, ?, NOW())", 
                                [rows[0].id, transData.transaction_id, transData.payment_response, transData.payment_status, transData.payment_message, post.user_id], function(err, insertCancel){
                                    connection.query("UPDATE orders SET order_status = '5' WHERE id = '"+post.order_id+"'", function(err, updatedrows){
                                        if(err)
                                            throw err;
                                        response.status = 1;
                                        response.message = 'Your order has been cancelled successfully';
                                       
                                        var data = {};
                                        data.order_status_label = params.ORDER_STATUS[5];
                                        response.data = data;
                
                                        res.send(response);
                                    });
                                });
                            }
                        });
                    } else {
                        connection.query("UPDATE orders SET order_status = '5' WHERE id = '"+post.order_id+"'", function(err, updatedrows){
                            if(err)
                                throw err;

                            response.status = 1;
                            response.message = 'Your order has been cancelled successfully';
                            
                            var data = {};
                            data.order_status_label = params.ORDER_STATUS[5];
                            response.data = data;

                            res.send(response);
                        });
                    }
                    // response.status = 0;
                    // response.message = 'Sorry! You can not cancel this order now';
                    // res.send(response);
                }else{
                    connection.query("UPDATE orders SET order_status = '5' WHERE id = '"+post.order_id+"'", function(err, updatedrows){
                        if(err)
                            throw err;
                        response.status = 1;
                        response.message = 'Your order has been cancelled successfully';
                       
                        var data = {};
                        data.order_status_label = params.ORDER_STATUS[5];
                        
                        response.data = data;

                        res.send(response);
                    });
                }
            });
        });
    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
});

//place order coupon_id is optional, mandatoy_params : shipping_address_id / shipping_address AND billing_address_id / billing_address
router.post('/api/order/place-order', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    if(typeof(post.user_id) != "undefined" && post.user_id > 0
        && post.product_detail != ""
        && typeof(post.sub_total) != "undefined" && post.sub_total
        && typeof(post.discount) != "undefined" && post.discount
        && typeof(post.tax) != "undefined" && post.tax
        && typeof(post.tip) != "undefined" && post.tip
        && typeof(post.total) != "undefined" && post.total
        // && typeof(post.card_number) != "undefined" && post.card_number
        // && typeof(post.expiry_date) != "undefined" && post.expiry_date
        // && typeof(post.card_code) != "undefined" && post.card_code )
    ){
        req.getConnection(function(err, connection) {
            if (err)
                throw err;

            var coupon_id = 0;
            if(typeof(post.coupon_id) != "undefined" && post.coupon_id){
                coupon_id = post.coupon_id;
            }

            var shipping_address_id = 0;
            var billing_address_id = 0;
            async.parallel([
                function(callback1){
                    if(typeof(post.shipping_address_id) != "undefined" && post.shipping_address_id > 0){
                        shipping_address_id = post.shipping_address_id;
                        callback1();
                    } else if (typeof(post.shipping_address) != "undefined"){
                        var shipping_address = JSON.parse(post.shipping_address);

                        var zipcode = (typeof(shipping_address.zipcode)!="undefined" && shipping_address.zipcode!="") ? shipping_address.zipcode : '';
                        var latitude = (typeof(shipping_address.latitude)!="undefined" && shipping_address.latitude!="") ? shipping_address.latitude : '0';
                        var longitude = (typeof(shipping_address.longitude)!="undefined" && shipping_address.longitude!="") ? shipping_address.longitude : '0';
                        var sql = "INSERT INTO address (address_title, address1, address2 ,country_id, state_id, city_id, zipcode, latitude, longitude, user_id, created_date, created_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  NOW(), 1)";
                        console.log("SQL1",sql);
                        connection.query(sql, [shipping_address.address_title, shipping_address.address1, shipping_address.address2, shipping_address.country_id, shipping_address.state_id, shipping_address.city_id, zipcode, latitude, longitude, post.user_id], function(err, rowData) {
                            if(err)
                                    console.log("ROwdata err", err);
                            var ship_add_id = rowData.insertId;
                            shipping_address_id = ship_add_id;
                            callback1();
                        });
                    }
                },
                function(callback2){
                    if(typeof(post.same_as_shipping) && post.same_as_shipping == 1){
                        callback2();
                    } else {
                        if(typeof(post.billing_address_id) != "undefined" && post.billing_address_id > 0){
                            billing_address_id = post.billing_address_id;
                            callback2();
                        } else if (typeof(post.billing_address) != "undefined"){
                            var billing_address = JSON.parse(post.billing_address);
                            var sql = "INSERT INTO address (address_title , address1, address2 ,country_id, state_id, city_id, zipcode, latitude, longitude, user_id, created_date , created_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), 1)";
                            console.log("SQL1",sql);
                            connection.query(sql, [billing_address.address_title, billing_address.address1, billing_address.address2, billing_address.country_id, billing_address.state_id, billing_address.city_id, billing_address.zipcode, billing_address.latitude, billing_address.longitude, post.user_id], function(err, rowData) {
                                if(err)
                                    console.log("ROwdata err1", err);
                                var bill_add_id = rowData.insertId;
                                billing_address_id = bill_add_id;
                                callback2();
                            });
                        }
                    }
                },
            ], function(err){
                if(err){
                    response.status = 0;
                    response.message = err;
                    res.send(response);
                }
                if(typeof(post.same_as_shipping) && post.same_as_shipping == 1){
                    billing_address_id = shipping_address_id;
                }
                calltransaction2(req, res, function(err, Data){
                    console.log("CALL TRANSACTION------->"+JSON.stringify(Data));
                    if(err){
                        response.status = 0;
                        response.message = err;
                        res.send(response);
                    }else{
            //New changes as per payment response
                            if(Data.is_success == 1){
                            var order_sql = "INSERT INTO orders SET ?";
                                
                                //execute query
                                //var today_date = new Date();
                                var today_date = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
                                connection.query(order_sql, {transaction_id: Data.transaction_id, payment_response: JSON.stringify(Data.payment_response), payment_status: Data.payment_status, payment_message: Data.payment_message, user_id:post.user_id, product_detail:post.product_detail, coupon_id:coupon_id, sub_total:post.sub_total, discount:post.discount, tax:post.tax, tip:post.tip, delivery_fees:post.delivery_fee, shipping_address_id:shipping_address_id, billing_address_id:billing_address_id, same_as_shipping:post.same_as_shipping, total:post.total, order_status:0, status:1, created_by:post.user_id, created_date:today_date}, function(err, rows){
                                    if(err)
                                        throw err;
                                    connection.query("UPDATE cart SET status = '2' WHERE user_id = '"+post.user_id+"' AND status != '2'", function(err, updatedrows){
                                        if(err)
                                            throw err;
                                        response.status = 1;
                                        response.message = 'Order has been placed Successfully';
                                        res.send(response);
                                    });
                                });
                        }else{
                            response.status = 0;
                            response.message = Data.payment_message;
                            res.send(response);
                        }
                    }
                    /*var order_sql = "INSERT INTO orders SET ?";
                    
                    //execute query
                    //var today_date = new Date();
                    var today_date = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
                    connection.query(order_sql, {transaction_id: Data.transaction_id, payment_response: JSON.stringify(Data.payment_response), payment_status: Data.payment_status, payment_message: Data.payment_message, user_id:post.user_id, product_detail:post.product_detail, coupon_id:coupon_id, sub_total:post.sub_total, discount:post.discount, tax:post.tax, tip:post.tip, delivery_fees:post.delivery_fee, shipping_address_id:shipping_address_id, billing_address_id:billing_address_id, same_as_shipping:post.same_as_shipping, total:post.total, order_status:0, status:1, created_by:post.user_id, created_date:today_date}, function(err, rows){
                        if(err)
                            throw err;
                        connection.query("UPDATE cart SET status = '2' WHERE user_id = '"+post.user_id+"' AND status != '2'", function(err, updatedrows){
                            if(err)
                                throw err;
                            response.status = 1;
                            response.message = 'Order has been placed Successfully';
                            res.send(response);
                        });
                    });*/
                });
            });
        });
    }else{
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
});

// Apply coupon, Params : code, products(count) and amount 
router.post('/api/order/apply-coupon', upload.array(), function(req, res, next) {
    var post = req.body;
    response = {};
    if(typeof(post.code) != "undefined" && post.code
        && typeof(post.products) != "undefined" && post.products
        && typeof(post.amount) != "undefined" && post.amount){
        req.getConnection(function(err, connection) {
            if (err)
                throw err;
            var sql = "SELECT * FROM coupons WHERE code='"+post.code+"' AND status='1' ORDER BY id DESC LIMIT 0,1";
            connection.query(sql, function(err, rows){
                if(err)
                    throw err;
                if(rows.length > 0){
                    var date = new Date();
                    var today_date = date.formatDate();
                    console.log("DATE todays--->"+today_date);
                    if(rows[0].expiry_date <=  today_date){
                        response.status = 0;
                        response.message = 'Sorry! code is expired';
                        res.send(response);
                    }else if(rows[0].start_date >=  today_date){
                        response.status = 0;
                        response.message = 'Invalid code applied!';
                        res.send(response);
                    }else if(rows[0].min_product > post.products){
                        response.status = 0;
                        response.message = 'Sorry! Minimum '+rows[0].min_product+' products are required to apply this coupon';
                        res.send(response);
                    }else if(rows[0].min_amount > post.amount){
                        response.status = 0;
                        response.message = 'Sorry! Coupon required Minimum shopping of $'+rows[0].min_amount;
                        res.send(response);
                    }else{
                        var data = {};
                        data.id = rows[0].id;
                        data.code = rows[0].code;
                        data.description = rows[0].description;
                        data.type = rows[0].type;
                        data.percentage = (rows[0].percentage) ? rows[0].percentage : 0;
                        data.discount = (rows[0].discount) ? rows[0].discount : 0;
                        data.min_amount = rows[0].min_amount;
                        data.min_product = rows[0].min_product;

                        response.status = 1;
                        response.message = 'Successfully applied!';
                        response.data = data;
                        res.send(response);
                    }
                }else{
                    response.status = 0;
                    response.message = 'Invalid code applied!';
                    res.send(response);
                }
            });
        });
    }else{
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
});

//Order Survey : Mark order as favorite, Rate the order is_favorite OR rating, comment
router.post('/api/order/order-survey', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    if(typeof(post.user_id) != "undefined" && post.user_id
        && typeof(post.order_id) != "undefined" && post.order_id){
        req.getConnection(function(err, connection) {
            if (err)
                throw err;
            var sql = "SELECT * FROM order_survey WHERE order_id='"+post.order_id+"' AND status='1'";
            connection.query(sql, function(err, rows){
                if(err)
                    throw err;
                if (rows.length > 0) {
                    var set_conditions = "modified_by = '"+post.user_id+"', modified_date = NOW()";
                    if(typeof(post.is_favorite) != "undefined" && post.is_favorite){
                        set_conditions += ", is_favorite = '"+post.is_favorite+"'";
                    }
                    if(typeof(post.rating) != "undefined" && post.rating){
                        set_conditions += ", rating = '"+post.rating+"'";
                    }
                    if(typeof(post.comment) != "undefined" && post.comment){
                        set_conditions += ", comment = '"+post.comment+"'";
                    }
                    connection.query("UPDATE order_survey SET "+set_conditions+" WHERE order_id='"+post.order_id+"'", function(err, updateRow){
                        if(err)
                            throw err;
                        response.status = 1;
                        response.message = 'Successfully Updated';
                        res.send(response);
                    });
                } else {
                    var is_favorite = 0;
                    var rating = 0;
                    var comment = "";
                    if(typeof(post.is_favorite) != "undefined" && post.is_favorite){
                        is_favorite = post.is_favorite;
                    }
                    if(typeof(post.rating) != "undefined" && post.rating){
                        rating = post.rating;
                    }
                    if(typeof(post.comment) != "undefined" && post.comment){
                        comment = post.comment;
                    }
                    connection.query("INSERT INTO order_survey SET ?", {
                        user_id: post.user_id,
                        order_id: post.order_id,
                        is_favorite: is_favorite,
                        rating: rating,
                        comment: comment,
                        created_by: post.user_id,
                        status:1,
                    }, function(err, insertRow){
                        if(err)
                            throw err;
                        response.status = 1;
                        response.message = 'Successfully Updated';
                        res.send(response);
                    });
                }
            });
        });
    }else{
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
});

//get common data
router.post('/api/order/get-commondata', upload.array(), isUserActive, function(req, res, next) {
    var post = req.body;
    response = {};
    var data = [];
    if(typeof(post.user_id) != "undefined" && post.user_id > 0){
        req.getConnection(function(err, connection) {
            if (err)
                throw err;
            var result = {};
            result.product_in_cart = 0;
            result.tax_percentage = 0;
            var delivery_prices = [];
            async.parallel([
                function(callback1){
                    var sql = "SELECT SUM(quantity) AS total_quantity FROM cart WHERE user_id='"+post.user_id+"' AND status!='2'";
                    connection.query(sql, function(err, rows){
                        if(err)
                            throw err;
                        if(rows.length > 0){
                            result.product_in_cart = (typeof(rows[0].total_quantity)!="undefined" && rows[0].total_quantity!=null && rows[0].total_quantity > 0) ? rows[0].total_quantity : 0;
                        }
                        callback1();
                    });
                },
                function(callback2){
                    var sql = "SELECT * FROM delivery_prices WHERE status!='2'";
                    connection.query(sql, function(err, rows){
                        if(err)
                            throw err;
                        if(rows.length > 0){
                            rows.forEach(function(row){
                                var datares = {};
                                datares.id = row.id;
                                datares.no_of_bottles = row.no_of_bottles;
                                datares.min_amount = row.min_amount;
                                datares.miles = row.miles;
                                delivery_prices.push(datares);
                            })
                        }
                        callback2();
                    });
                },
                function(callback3){
                    var sql = "SELECT * FROM business_rules WHERE status!='2'";
                    connection.query(sql, function(err, rows){
                        if(err)
                            throw err;
                        if(rows.length > 0){
                            result.tax_percentage = rows[0].tax_percentage;
                        }
                        callback3();
                    });
                },
            ], function(err){
                if(err){
                    throw err;
                }
                result.delivery_prices = delivery_prices;
                data.push(result);
                response.status = 1;
                response.message = 'Success';
                response.data = data[0];
                res.send(response);
            })
        });
    } else {
        response.status = 0;
        response.message = 'Wrong or missing parameters';
        res.send(response);
    }
});

//Functions
function calltransaction(req, res, callback){
    var payment_res = {};
    var merchantAuthenticationType = new ApiContracts.MerchantAuthenticationType();
    merchantAuthenticationType.setName(payment.API_LOGIN_ID);
    merchantAuthenticationType.setTransactionKey(payment.TRANSACTION_KEY);

    var creditCard = new ApiContracts.CreditCardType();
    creditCard.setCardNumber(req.body.card_number); //card_number - 4242424242424242
    creditCard.setExpirationDate(req.body.expiry_date); //expiry_date mmyy - 0822
    creditCard.setCardCode(req.body.card_code); //card_code - 999

    var paymentType = new ApiContracts.PaymentType();
    paymentType.setCreditCard(creditCard);

    var transactionRequestType = new ApiContracts.TransactionRequestType();
    transactionRequestType.setTransactionType(ApiContracts.TransactionTypeEnum.AUTHCAPTURETRANSACTION);
    transactionRequestType.setPayment(paymentType);
    transactionRequestType.setAmount(Number(req.body.total)); //Amount - 2.99

    var createRequest = new ApiContracts.CreateTransactionRequest();
    createRequest.setMerchantAuthentication(merchantAuthenticationType);
    createRequest.setTransactionRequest(transactionRequestType);

    //pretty print request
    console.log(JSON.stringify(createRequest.getJSON(), null, 2));

    var ctrl = new ApiControllers.CreateTransactionController(createRequest.getJSON());

    // For PRODUCTION use
    if(payment.ENV == "PRODUCTION"){
        ctrl.setEnvironment(SDKConstants.endpoint.production);
    }

    ctrl.execute(function(){
        var apiResponse = ctrl.getResponse();
        var response = new ApiContracts.CreateTransactionResponse(apiResponse);
        //pretty print response
        console.log(JSON.stringify(response, null, 2));
        if(response != null){
            if(response.getMessages().getResultCode() == ApiContracts.MessageTypeEnum.OK && response.getTransactionResponse().getResponseCode() == '1'){
                console.log('Transaction ID: ' + response.getTransactionResponse().getTransId());
                payment_res.transaction_id = response.getTransactionResponse().getTransId();
                payment_res.payment_response = response.getTransactionResponse();
                payment_res.payment_status = response.getTransactionResponse().getResponseCode();
                payment_res.payment_message = response.getMessages().getResultCode();
		payment_res.is_success = 1;
            }else{
                console.log('Result Code: ' + response.getMessages().getResultCode());
                console.log('Error Code: ' + response.getMessages().getMessage()[0].getCode());
                console.log('Error message: ' + response.getMessages().getMessage()[0].getText());
                payment_res.transaction_id = (typeof(response.getTransactionResponse().getTransId()) != "undefined") ? response.getTransactionResponse().getTransId() : null;
                payment_res.payment_response = response.getTransactionResponse();
                payment_res.payment_status = response.getMessages().getResultCode();
                payment_res.payment_message = response.getTransactionResponse().getErrors().getError()[0].getErrorText();//response.getMessages().getMessage()[0].getText();
		payment_res.is_success = 0;
            }
            callback(null, payment_res);
        }else{
            payment_res.transaction_id = null;
            payment_res.payment_response = null;
            payment_res.payment_status = 0;
            payment_res.payment_message = "ERROR while payment! Please try again";
		payment_res.is_success = 0;
            callback(null, payment_res);
        }
    });
}

// let orderIndex = 0;

function calltransaction2(req, res, callback){
    let customerId;
    let fees = (2.9 / 100 * req.body.total).toFixed(2);
    fees = fees * 100;

    req.getConnection(function(err, connection) {
        if (err) throw err;
        else {
            connection.query("SELECT * FROM user WHERE (id = '" + req.body.user_id + "')", function(err, rows){
                if (err) throw err;
                else {
                    if(rows.length == 0 ) res.send({message : "No user found"});
                    else{
                        if(rows[0].stripe_customer_id && rows[0].stripe_customer_id != undefined){
                        customerId = rows[0].stripe_customer_id;
                        stripe.customers.createSource(customerId, {
                            source: req.body.stripeToken
                          })
                        .then(source => {
                            stripe.charges.create({
                                amount: req.body.total * 100,
                                currency: "usd",
                                customer: customerId,
                                source : source.id,
                                // transfer_group: "ORDER" + orderIndex,
                              }).then(function(charge) {
                                if(charge && charge != undefined){
                                    // Create a Transfer to the connected account (later):
                                    // let amountToTransfer = (req.body.sub_total * 100) - fees;
                                    // stripe.transfers.create({
                                    //     amount: amountToTransfer,
                                    //     currency: "usd",
                                    //     destination: "acct_1FKLumLiVx9f6PxO",
                                    //     transfer_group: "ORDER" + orderIndex,
                                    // }).then(function(transfer) {
                                    //     if(transfer  && transfer != undefined){
                                    //         console.log("I am here0000000000...Payment done");
                                            payment_res = {
                                                // transaction_id : orderIndex,
                                                transaction_id : charge.id,
                                                payment_response : "Payment successful",
                                                payment_status : 1,
                                                payment_message : "ok",
                                                is_success : 1
                                            }
                                            // orderIndex++;
                                            callback(null, payment_res);
                                        // }
                                        // else{
                                        //     console.log("I am here333333333333333333333333...Charge is undefined");
                                        //     console.log("********************************************************")
                                        //     payment_res = {
                                        //         transaction_id : null,
                                        //         payment_response : null,
                                        //         payment_status : 0,
                                        //         payment_message : "ERROR while payment! Please try again",
                                        //         is_success : 0
                                        //     }
                                        //     callback(null, payment_res);
                                        // }
                                    // });
                                }
                                else {
                                    console.log("I am here444444444444444444444...Charge is undefined");
                                    console.log("********************************************************")
                                    payment_res = {
                                        transaction_id : null,
                                        payment_response : null,
                                        payment_status : 0,
                                        payment_message : "ERROR while payment! Please try again",
                                        is_success : 0
                                    }
                                    callback(null, payment_res);
                                }
                              });
                              
                              
                    })
                        .catch((err) => {
                            console.log("I am here555555555555555555...", err);
                            console.log("********************************************************")
                            payment_res = {
                                transaction_id : null,
                                payment_response : null,
                                payment_status : 0,
                                payment_message : "ERROR while payment! Please try again",
                                is_success : 0
                            }
                            callback(null, payment_res);
                    
                        });

                        }
                        else {
                        stripe.customers.create({
                                email : rows[0].email 
                            })
                            .then(customer => {
                                customerId = customer.id;
                                connection.query("UPDATE user SET stripe_customer_id = '" + customer.id +"' WHERE id = '" + req.body.user_id + "'", function(err, del){
                                    if(err) throw err;
                                    else  {
                                        stripe.customers.createSource(customerId, {
                                            source: req.body.stripeToken
                                          })
                                        .then(source => {
                                        stripe.charges.create({
                                            amount: req.body.total * 100,
                                            currency: "usd",
                                            customer: customerId,
                                            source : source.id,
                                            // transfer_group: "ORDER" + orderIndex,
                                          }).then(function(charge) {
                                            if(charge && charge != undefined){
                                                // let amountToTransfer = (req.body.sub_total * 100) - fees;
                                                // stripe.transfers.create({
                                                //     amount: amountToTransfer,
                                                //     currency: "usd",
                                                //     destination: "acct_1FKLumLiVx9f6PxO",
                                                //     transfer_group: "ORDER" + orderIndex,
                                                // }).then(function(transfer) {
                                                //     if(transfer  && transfer != undefined){
                                                //         console.log("I am here0000000000...Payment done");
                                                        payment_res = {
                                                            transaction_id : charge.id,
                                                            payment_response : "Payment successful",
                                                            payment_status : 1,
                                                            payment_message : "ok",
                                                            is_success : 1
                                                        }
                                                        // orderIndex++;
                                                        callback(null, payment_res);
                                                //     }
                                                //     else{
                                                //         console.log("I am here333333333333333333333333...Charge is undefined");
                                                //         console.log("********************************************************")
                                                //         payment_res = {
                                                //             transaction_id : null,
                                                //             payment_response : null,
                                                //             payment_status : 0,
                                                //             payment_message : "ERROR while payment! Please try again",
                                                //             is_success : 0
                                                //         }
                                                //         callback(null, payment_res);
                                                //     }
                                                // });
                                            }
                                            else {
                                                console.log("I am here444444444444444444444...Charge is undefined");
                                                console.log("********************************************************")
                                                payment_res = {
                                                    transaction_id : null,
                                                    payment_response : null,
                                                    payment_status : 0,
                                                    payment_message : "ERROR while payment! Please try again",
                                                    is_success : 0
                                                }
                                                callback(null, payment_res);
                                            }
                                          })
                                          .catch((err) => {
                                            console.log("I am here555555555555555555...", err);
                                            console.log("********************************************************")
                                            payment_res = {
                                                transaction_id : null,
                                                payment_response : null,
                                                payment_status : 0,
                                                payment_message : "ERROR while payment! Please try again",
                                                is_success : 0
                                            }
                                            callback(null, payment_res);
                                    
                                        });  
                                    })
                                    .catch((err) => {
                                        console.log("I am here666666666666666666...", err);
                                        console.log("********************************************************")
                                        payment_res = {
                                            transaction_id : null,
                                            payment_response : null,
                                            payment_status : 0,
                                            payment_message : "ERROR while payment! Please try again",
                                            is_success : 0
                                        }
                                        callback(null, payment_res);
                                
                                    }); 
                                          
                                }
                            })
                        })
                        }               
                    }
                }
            });
        }
    });
}

function refundTransaction(req, resp, callback) {
    var payment_res = {};
	var merchantAuthenticationType = new ApiContracts.MerchantAuthenticationType();
	merchantAuthenticationType.setName(payment.API_LOGIN_ID);
	merchantAuthenticationType.setTransactionKey(payment.TRANSACTION_KEY);

    var creditCard = new ApiContracts.CreditCardType();

    var paymentRsp = JSON.parse(resp.payment_response);
    var accountNumber = paymentRsp.accountNumber;
    creditCard.setCardNumber(accountNumber);
	creditCard.setExpirationDate('XXXX');

	var paymentType = new ApiContracts.PaymentType();
	paymentType.setCreditCard(creditCard);

	var transactionRequestType = new ApiContracts.TransactionRequestType();
	transactionRequestType.setTransactionType(ApiContracts.TransactionTypeEnum.REFUNDTRANSACTION);
	transactionRequestType.setPayment(paymentType);
	transactionRequestType.setAmount(Number(resp.total));
	transactionRequestType.setRefTransId(resp.transaction_id);

	var createRequest = new ApiContracts.CreateTransactionRequest();
	createRequest.setMerchantAuthentication(merchantAuthenticationType);
	createRequest.setTransactionRequest(transactionRequestType);

	//pretty print request
	console.log(JSON.stringify(createRequest.getJSON(), null, 2));
		
	var ctrl = new ApiControllers.CreateTransactionController(createRequest.getJSON());

	ctrl.execute(function(){

		var apiResponse = ctrl.getResponse();
		var response = new ApiContracts.CreateTransactionResponse(apiResponse);

		//pretty print response
		console.log(JSON.stringify(response, null, 2));

		if(response != null){
			if(response.getMessages().getResultCode() == ApiContracts.MessageTypeEnum.OK){
				if(response.getTransactionResponse().getMessages() != null){
					console.log('Successfully created transaction with Transaction ID: ' + response.getTransactionResponse().getTransId());
					console.log('Response Code: ' + response.getTransactionResponse().getResponseCode());
					console.log('Message Code: ' + response.getTransactionResponse().getMessages().getMessage()[0].getCode());
                    console.log('Description: ' + response.getTransactionResponse().getMessages().getMessage()[0].getDescription());
                    payment_res.transaction_id = response.getTransactionResponse().getTransId();
                    payment_res.payment_response = response.getTransactionResponse();
                    payment_res.payment_status = response.getTransactionResponse().getResponseCode();
                    payment_res.payment_message = response.getTransactionResponse().getMessages().getMessage()[0].getDescription();
				}
				else {
					console.log('Failed Transaction.');
					if(response.getTransactionResponse().getErrors() != null){
						console.log('Error Code: ' + response.getTransactionResponse().getErrors().getError()[0].getErrorCode());
                        console.log('Error message: ' + response.getTransactionResponse().getErrors().getError()[0].getErrorText());
                        payment_res.transaction_id = response.getTransactionResponse().getTransId();
                        payment_res.payment_response = response.getTransactionResponse();
                        payment_res.payment_status = response.getTransactionResponse().getErrors().getError()[0].getErrorCode();
                        payment_res.payment_message = response.getTransactionResponse().getErrors().getError()[0].getErrorText();
					}
				}
			}
			else {
				console.log('Failed Transaction. ');
				if(response.getTransactionResponse() != null && response.getTransactionResponse().getErrors() != null){
				
					console.log('Error Code: ' + response.getTransactionResponse().getErrors().getError()[0].getErrorCode());
                    console.log('Error message: ' + response.getTransactionResponse().getErrors().getError()[0].getErrorText());
                    payment_res.transaction_id = response.getTransactionResponse().getTransId();
                    payment_res.payment_response = response.getTransactionResponse();
                    payment_res.payment_status = response.getTransactionResponse().getErrors().getError()[0].getErrorCode();
                    payment_res.payment_message = response.getTransactionResponse().getErrors().getError()[0].getErrorText();
				}
				else {
					console.log('Error Code: ' + response.getMessages().getMessage()[0].getCode());
                    console.log('Error message: ' + response.getMessages().getMessage()[0].getText());
                    payment_res.transaction_id = response.getTransactionResponse().getTransId();
                    payment_res.payment_response = response.getTransactionResponse();
                    payment_res.payment_status = response.getMessages().getMessage()[0].getCode();
                    payment_res.payment_message = response.getMessages().getMessage()[0].getText();
				}
			}
		}
		else {
            console.log('Null Response.');
            payment_res.transaction_id = null;
            payment_res.payment_response = null;
            payment_res.payment_status = null;
            payment_res.payment_message = "ERROR! NO Response";
		}
		callback(null, payment_res);
	});
}

async function getName(req, id, table){
    var name = "";
    req.getConnection(function(err, connection) {
        if (err)
            throw err;
        var sql = "SELECT name FROM "+table+" WHERE id='"+id+"' AND status='1'";
        connection.query(sql, function(err, rows){
            if(err)
                throw err;
            if(rows.length > 0){
                console.log(rows[0].name);
                name = rows[0].name;
            }
            return name;
        });
    });
}

//To format date in Y-m-d 
Date.prototype.formatDate = function() {
    var date = new Date(this.valueOf()),
        month = '' + (date.getMonth() + 1),
        day = '' + date.getDate(),
        year = date.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
//To format date in Y-m-d 
Date.prototype.formatDateTime = function() {
    var date = new Date(this.valueOf()),
        month = '' + (date.getMonth() + 1),
        day = '' + date.getDate(),
        year = date.getFullYear(),
        hours = date.getHours(),
        minutes = date.getMinutes(),
        seconds = date.getSeconds();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    var dateformat = [year, month, day].join('-');
    var timeformat = [hours, minutes, seconds].join(':');
    return dateformat+ ' ' +timeformat ;
}
module.exports = router;

// route middleware to ensure user is valid
function isUserActive(req, res, next) {
    var post = req.body;
    var userid = 0;
    var response = {};
    if (typeof (post.user_id) != "undefined" && post.user_id > 0) {
        userid = post.user_id;
    }
    if (userid > 0) {
        req.getConnection(function (err, connection) {
            if (err) {
                console.log(err);
                throw err;
            } else {
                var sql = "SELECT status FROM user WHERE id = '" + userid + "'";
                connection.query(sql, function (err, rows) {
                    if (err) {
                        console.log(err);
                        throw err;
                    } else {
                        if (rows.length > 0) {
                            if (rows[0].status == 0) {
                                response.status = -1;
                                response.message = "Sorry! Your account is in-active temporarily";
                                res.send(response);
                            } else if (rows[0].status == 2) {
                                response.status = -1;
                                response.message = "Sorry! Your account is deactive permanently";
                                res.send(response);
                            } else {
                                return next();
                            }
                        } else {
                            response.status = -1;
                            response.message = "Sorry! Invalid user";
                            res.send(response);
                        }
                    }
                });
            }
        });
    } else {
        return next();
    }
}