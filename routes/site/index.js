var express = require('express');
var router = express.Router();
var path = require('path');
var multer = require('multer');
var mkdirp = require('mkdirp');
var bcrypt = require('bcryptjs');
var http = require('http');

module.exports = function (app, passport) {
    app.get('/site/policy', function (req, res) {
        req.getConnection(function(err, connection) {
            if(err) throw err;
            connection.query("SELECT * FROM cms_management WHERE status != '2' AND type = 'privacy'", function(err, rows) {
                if (err) {
                    req.flash('cmsMessage', 'Oops! Something went wrong');
                    res.render('admin/dashboard', {
                        message: req.flash('cmsMessage')
                    });
                } else {
                    res.render('site/cms', {
                        CMS: rows[0],
                        queryParams: req.query,
                        message: req.flash('cmsMessage')
                    });
                }
            });
        });
    });

    app.get('/site/terms', function (req, res) {
        req.getConnection(function(err, connection) {
            if(err) throw err;
            connection.query("SELECT * FROM cms_management WHERE status != '2' AND type = 'term_condition'", function(err, rows) {
                if (err) {
                    req.flash('cmsMessage', 'Oops! Something went wrong');
                    res.render('admin/dashboard', {
                        message: req.flash('cmsMessage')
                    });
                } else {
                    res.render('site/cms', {
                        CMS: rows[0],
                        queryParams: req.query,
                        message: req.flash('cmsMessage')
                    });
                }
            });
        });
    });
}