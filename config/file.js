exports.func = function(){
    return {
        storage: function(req){
            var storage = multer.diskStorage({
                destination: function(req, file, cb) {
                    cb(null, 'uploads/')
                },
                filename: function(req, file, cb) {
                    var getFileExt = function(fileName) {
                        var fileExt = fileName.split(".");
                        if (fileExt.length === 1 || (fileExt[0] === "" && fileExt.length === 2)) {
                            return "";
                        }
                        return fileExt.pop();
                    }
                    var microsecond = Math.round(new Date().getTime() / 1000 * Math.floor(Math.random() * 1000000000)); //new Date().getTime();
                    cb(null, microsecond + path.extname(file.originalname)); // Date.now() + '.' + getFileExt(file.originalname))
                }
            });
            return storage;
        },
    }
}