var apn = require("apn");
var FCM = require("fcm-push");

exports.notification = function(){
    return{
        /*function to send notification*/
        sendNotification: function(device_token, msg, body, req){
            var fcm = new FCM(params.SERVER_KEY);
            var message = {
                to: device_token, // required fill with device token or topics
                //collapse_key: 'your_collapse_key', 
                data: {
                    sound: 'default',
                    alert: msg,
                    badge: 1,
                    payload: {'body': body},
                    topic: 'com.boozer',
                },
                notification: {
                    title: 'Brewzer',
                    body: msg,
                }
            };
            
            //callback style
            fcm.send(message, function(err, response){
                if (err) {
                    console.log("Something has gone wrong with notification!", err);
                } else {
                    console.log("Successfully sent with response: ", response);
                }
                return;
            });
        },
        /* function to store notification in our database
         * @data must be containing : user_id, type(integer), message 
         */
        saveNotification: function(data, msg, req){
            req.getConnection(function(err, connection) {
                var mdate = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
                var qr = 'INSERT INTO `notifications` SET ?';
                var qr_data = {
                    user_id: data.user_id,
                    type: data.type, //1=Global,2=Order
                    primary_id: (typeof(data.primary_id)!='undefined' && data.primary_id!=null) ? data.primary_id : 0,
                    message: data.message,
                    description: (typeof(data.description)!='undefined' && data.description!=null) ? data.description : "",
                    image: (typeof(data.image)!='undefined' && data.image!=null) ? data.image : "",
                    is_read:0,
                    status:1,
                    created_by:(req.user.user_id!=null) ? req.user.user_id : 0,
                    created_date:mdate
                };
                connection.query(qr, qr_data, function(err, result){
                    if(err){
                        throw err;
                        return;
                    }else{
                        console.log('done');
                        return;
                    }
                })
            });
        }
    };
}
