// var apn = require('apn');

// var options = {
//   token: {
//     key: "", //key.p8
//     keyId: "", //L8628QAXSW
//     teamId: "", //R479N6L2QR
//   },
//   production: true
// };
// var apnProvider = new apn.Provider(options);
// var note = new apn.Notification();
exports.func = function(){
    return {
        /* function to check whether required req param is exist in post or not*/
        validateReqParam : function(post, reqparam){
            var remain = [];
            var req = [];            
            for(var i=0;i<reqparam.length;i++){                
                if(typeof post[reqparam[i]]!='undefined'){                                                           
                    if(post[reqparam[i]]==''){
                        req.push(reqparam[i]);
                    }                    
                }else{                    
                    remain.push(reqparam[i]);
                }
            }              
            var respose = {'missing':remain,'blank':req};                  
            return respose;
        },
        /* function to load error message template for blank/missing req params for all APIs*/
        loadErrorTemplate: function(elem){
            var missing = elem.missing;    
            var blank_str = '', missing_str = '';    
            if(missing.length>0){
                missing_str = missing.join(',');        
                missing_str+=' missing';
            }
            var blank = elem.blank;    
            if(blank.length>0){        
                blank_str = blank.join(',');        
                blank_str+=' should not be blank';
            }
            var s = [missing_str, blank_str];
            var str = s.join(' \n ');
            return str;
        },
        /* function to send notification*/
        /*sendNotification: function(deviceToken, msg, body, req){
            note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
            note.badge = 3;
            note.sound = "default";
            note.alert = msg;
            note.payload = {'body': body};
            note.topic = "com.boozer";
            
            apnProvider.send(note, deviceToken).then( (result) => {
                console.log(result.failed);
                return;                
            // see documentation for an explanation of result
            }).catch(function(err){
                console.log(err);                    
                return;
            }); 
        },*/
        /* function to store notification*/
        saveNotification: function(type, parent_id, msg, req){  
            req.getConnection(function(err, connection) {
                var data = {};
                let user_id,pid;
                //data.team_id = parent_id;         
                if(type == 1){
                    user_id = parent_id;
                    pid = req.body.activity_id;
                }else if(type == 2){
                    user_id = parent_id;
                    pid = req.body.activity_id;                    
                }else if(type == 3){
                    user_id = req.body.friend_id;
                    pid = req.body.user_id;
                }else if(type == 4){
                    user_id = req.body.friend_id;
                    pid = req.body.user_id;
                }else{
                    user_id = req.body.user_id;
                    pid = 0;
                }
                var mdate = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
                var qr = 'INSERT INTO `notifications`(user_id, type, message,parent_id, created_date, modified_date) VALUES('+user_id+','+type+',?,'+pid+',"'+mdate+'","'+mdate+'")';
                connection.query(qr,msg, function(err, result){
                    if(err){
                        throw err;
                        return;
                    }else{
                        console.log('done');
                        return;
                    }
                })
            });
        },
    };
 }
Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};