// load all the things we need
var LocalStrategy    = require('passport-local').Strategy;
var bcrypt = require('bcryptjs');
var randomBytes = require('random-bytes');
var nodemailer = require('nodemailer');
var dbconfig = require('./db.js');
var params = require('./params.js');
var mysql = require('mysql');
// var connection = mysql.createConnection({
// 				    host     : dbconfig.host,
// 				    user     : dbconfig.user,
// 				    password : dbconfig.password,
//                 });

function handleDisconnect() {
    connection = mysql.createConnection({
        host     : dbconfig.host,
        user     : dbconfig.user,
        password : dbconfig.password,
    }); 
    // Recreate the connection, since
    // the old one cannot be reused.

    connection.connect(function(err) {              // The server is either down
        if(err) {                                     // or restarting (takes a while sometimes).
        console.log('error when connecting to db:', err);
        setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
        }                                     // to avoid a hot loop, and to allow our node script to
    });                                     // process asynchronous requests in the meantime.
                                            // If you're also serving http, display a 503 error.
    connection.on('error', function(err) {
        console.log('db error', err);
        if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
        handleDisconnect();                         // lost due to either server restart, or a
        } else {                                      // connnection idle timeout (the wait_timeout
        throw err;                                  // server variable configures this)
        }
    });
}

handleDisconnect();

connection.query('USE '+dbconfig.database);

// load up the user model
module.exports = function(passport) {
    // passport session setup ==================================================

    // serialize the user
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // deserialize the user
    passport.deserializeUser(function(id, done) {
        connection.query("SELECT * FROM user WHERE id='"+id+"' AND status = 1 AND role = 1", function(err, rows) {
            done(err, rows[0]);
        });
    });

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },
    function(req, email, password, done) {
        if (email)
            email = email.toLowerCase(); // Use lower-case e-mails to avoid case-sensitive e-mail matching

        // asynchronous
        process.nextTick(function() {
            req.getConnection(function(err, connection) {
                connection.query("SELECT * FROM user WHERE email='"+email+"' AND status = 1 AND role = 1", function(err, rows) {
                    if (err) {
                        return done(err);
                    } else {
                        console.log("ROW LENGTH IS ::: " + rows.length);
                        if (rows.length > 0) {

                            var password_hash = rows[0].password_hash;
                            var user_id = rows[0].id;

                            // Load hash from your password DB.
                            bcrypt.compare(password, password_hash, function(err, result) {
                                if (err) {
                                    console.log(err);
                                    return done(err);
                                } else if (result === true) {
                                    console.log("password_hash match");
                                    return done(null, rows[0]);
                                } else {
                                    return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));
                                }
                                // res === true
                            });
                        } else {
                            console.log("no user found");
                            return done(null, false, req.flash('loginMessage', 'Invalid User'));
                        }
                    }
                });
            });
        });
    }));

    // =========================================================================
    // LOCAL FORGOT PASSWORD =============================================================
    // =========================================================================
    passport.use('local-forgot-password', new LocalStrategy({
        usernameField : 'email',
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },
    function(req, email, done) {
        if (email)
            email = email.toLowerCase(); // Use lower-case e-mails to avoid case-sensitive e-mail matching

        // asynchronous
        process.nextTick(function() {
            req.getConnection(function(err, connection) {
                connection.query("SELECT * FROM user WHERE email='"+email+"' AND status = 1 AND role = 1", function(err, rows) {
                    if (err) {
                        return done(null, false, req.flash('forgotPasswordMessage', err));
                    } else {
                        console.log("ROW LENGTH IS ::: " + rows.length);
                        if (rows.length > 0) {
                            var transporter = nodemailer.createTransport({
                                debug: true,
                                host: params.SMTP_HOST,
                                secureConnection: false, // true for 465, false for other ports
                                port: params.SMTP_PORT,
                                tls: {cipher:'SSLv3'},
                                auth: {
                                    user: params.SMTP_EMAIL,
                                    pass: params.SMTP_PASSWORD,
                                }
                            });

                            randomBytes(48, function (err, buffer) {
                                var passwordResetToken = buffer.toString('hex');

                                connection.query("UPDATE user SET password_reset_token = '" + passwordResetToken + "' WHERE email = '" + email + "' ", function (err, updateResetToken) {
                                    if (err) console.log(err);
                                    var mailOptions = {
                                        from: 'no-reply@brewzeronline.com',
                                        to: email,
                                        subject: 'Reset Password',
                                        html: '<p>Hello ' + data[0].first_name + '</p><p>Click on this link to reset your password : <a href="' + req.headers.host + '/admin/resetpassword?token=' + passwordResetToken + '">Reset Password</a></p><br /> Regards,<br />Team Brewzer',
                                    };
                                    transporter.sendMail(mailOptions, function (error, info) {
                                        if (error) {
                                            console.log(error);
                                            return done(null, false, req.flash('forgotPasswordMessage', 'Oops! Something went wrong while sending a mail'));
                                        } else {
                                            console.log('Email sent: ' + info.response);
                                            return done(null, true, req.flash('forgotPasswordMessage', 'Mail has been sent to registered email-id'));
                                        }
                                    });
                                });
                            });
                        } else {
                            console.log("no user found");
                            return done(null, false, req.flash('loginMessage', 'No user found.'));
                        }
                    }
                });
            });
        });
    }));
    
};
