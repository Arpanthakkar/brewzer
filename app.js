var express = require('express');
var router = express.Router();
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var flash = require('connect-flash');
var session = require('express-session');
var dbConfig = require('./config/db.js');
var passport = require('passport');
var http = require('http');

//Constants
global.payment = {
    ENV: "PRODUCTION", //or PRODUCTION, SANDBOX
    //API_LOGIN_ID: "5t4tQfb42TU6",
    //TRANSACTION_KEY: "7A42KHv92wt69L72",
    API_LOGIN_ID: "5t4tQfb42TU6",
    TRANSACTION_KEY: "3dE8eA5U9Za7q7Cg", //4H55vWz8VA7Skf92, 7A42KHv92wt69L72
};
global.params = {
    ORDER_STATUS : {
        0 : "Pending",
        1 : "Accepted",
        2 : "Rejected",
        3 : "Out For Delivery",
        4 : "Delivered",
        5 : "Cancelled",
    },
    USPS_DATA : {
        USERNAME : "630JETSE0190",
        PASSWORD : "844DR28XX683",
    },
    USPS_URL : "http://production.shippingapis.com/ShippingAPI.dll",
    SERVER_KEY : "AIzaSyDpxvb09RXCU2SGFUrfxo3-IS3EbbMwMXQ",
};
global.smtp = {
    SMTP_EMAIL: "no-reply@brewzeronline.com",
    SMTP_PASSWORD: "Solulab@123",//"Ud0kWVu?!yK5dPxF",
    SMTP_HOST: "smtp.office365.com",
    SMTP_PORT: "587",
    // SMTP_EMAIL: "no.reply.brewzer@gmail.com",
    // SMTP_PASSWORD: "Pass@123",//"Ud0kWVu?!yK5dPxF",
    // SMTP_HOST: "smtp.gmail.com",
    // SMTP_PORT: "587",
};
global.twilio = {
    accountSid : "ACd9e3cf1a6d2779260e361b9fcef1fe13",
    authToken : "bf1be5396d028f268b8fa83d194150e6",
};

//app port
const port = process.env.PORT || 3030;

// Passport Config
require('./config/passport')(passport);
var passport = require('passport');

var app = express();

/*app.get('/.well-known/pki-validation/godaddy.html', function(request, response){
    response.sendFile('/home/sftpboozer/site/.well-known/pki-validation/godaddy.html');
});*/

var mysql = require('mysql');
var connection = require('express-myconnection');

// Express Messages Middleware
app.use(flash());

// view engine setup
app.set('views', path.join(__dirname, 'views'));

app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

// Set Public Folder
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'uploads')));

app.use(logger('dev'));

// Body Parser Middleware
app.use(cookieParser()); // read cookies (needed for auth)

// parse application/json
app.use(bodyParser.json());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true
}));

// Express Session Middleware
app.use(session({
    secret: 'mSNZjn,8e6#~*DD',
    resave: true,
    saveUninitialized: true
}));

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

//flash error and success mssages
app.use(function (req, res, next) {
    res.locals.messages = require('express-messages')(req, res);
    next();
});

// Express Validator Middleware
app.use(expressValidator({
    errorFormatter: function (param, msg, value) {
        var namespace = param.split('.'),
            root = namespace.shift(),
            formParam = root;

        while (namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param: formParam,
            msg: msg,
            value: value
        };
    }
}));

// Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

app.get('*', function (req, res, next) {
    res.locals.user = req.user || null;
    next();
});

//sql connection
app.use(connection(mysql, dbConfig));

var index = require('./routes/index');
var users = require('./routes/users');

//included files
app.use('/', index);
app.use('/users', users);

// Route Files
app.use('/', require('./routes/admin/role'));
app.use('/', require('./routes/admin/login'));
require('./routes/admin/site')(app, passport);
require('./routes/admin/user')(app, passport);
require('./routes/admin/category')(app, passport);
require('./routes/admin/subcategory')(app, passport);
require('./routes/admin/brand')(app, passport);
require('./routes/admin/flavour')(app, passport);
require('./routes/admin/product')(app, passport);
require('./routes/admin/coupon')(app, passport);
require('./routes/admin/order')(app, passport);
require('./routes/admin/delteam')(app, passport);
require('./routes/admin/cms')(app, passport);
require('./routes/admin/notification')(app, passport);
require('./routes/admin/report')(app, passport);
require('./routes/admin/deliveryprice')(app, passport);
require('./routes/site/index')(app, passport);

// Web API files
app.use('/', require('./routes/api/user'));
app.use('/', require('./routes/api/cart'));
app.use('/', require('./routes/api/order'));
app.use('/', require('./routes/api/master'));


//create server
var server = http.createServer(app);
// Start Server
server.listen(port, function () {
    console.log('HTTP server listening on port ' + port);
});

module.exports = app;
